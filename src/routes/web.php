<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'GeneralController@landing');
Route::get('get-my-book', 'GeneralController@buyMyBook');
Route::get('my-profile', 'GeneralController@myProfile');
Route::get('work-with-me', 'GeneralController@workWithMe');
Route::get('/blog', [App\Http\Controllers\GeneralController::class, 'blog']);
Route::get('/blog/{slug}', [App\Http\Controllers\GeneralController::class, 'blog_details']);
Route::get('buy/{ref}/{customer}', 'GeneralController@buy');

Route::get('buy/{customer}', 'GeneralController@buyhr');

Route::post('customer', 'GeneralController@customer');
Route::post('hrpay', 'GeneralController@hrpay');
Route::post('/pay', 'PaymentController@redirectToGateway')->name('pay');
Route::get('/payment/callback', 'PaymentController@handleGatewayCallback');
Route::get('/consult-dr-yemisi', 'GeneralController@consult_yemisi');
Route::post('/post/public', 'GeneralController@post_public');
Route::post('/post/one', 'GeneralController@post_one');
Route::post('/post/group', 'GeneralController@post_group');
Route::get('/pay/consult/{user}', 'GeneralController@pay_consult');
Route::get('/resources', 'GeneralController@resources');
Route::get('/events', 'GeneralController@events');
Route::get('/booklaunch', 'GeneralController@bookLaunch');
Route::get('/hrtemplate', 'GeneralController@hrtemplate');

Route::post('post/comment', 'GeneralController@post_comment');
Route::post('post/info', 'GeneralController@post_info');
Route::get('good', 'GeneralController@good');

Route::get('/success/{ref}', 'GeneralController@success');
Route::post('/subscribe/newsletter', 'GeneralController@newsletter');



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/settings', [App\Http\Controllers\HomeController::class, 'settings']);
Route::post('/upload/book', [App\Http\Controllers\HomeController::class, 'upload_book']);
Route::post('/create/coupon', [App\Http\Controllers\HomeController::class, 'create_coupon']);
Route::get('/delete/{code}', [App\Http\Controllers\HomeController::class, 'delete_coupon']);
Route::get('/add/post', [App\Http\Controllers\HomeController::class, 'add_post']);
Route::get('/view/post', [App\Http\Controllers\HomeController::class, 'view_post']);
Route::get('/edit/post/{ref}', [App\Http\Controllers\HomeController::class, 'edit_post']);
Route::get('/delete/post/{ref}', [App\Http\Controllers\HomeController::class, 'delete_post']);
Route::post('post/blog', [App\Http\Controllers\HomeController::class, 'post_blog']);
Route::post('edit/blog', [App\Http\Controllers\HomeController::class, 'post_blog_edit']);
Route::post('create/category', [App\Http\Controllers\HomeController::class, 'create_category']);
Route::get('/view-work', [App\Http\Controllers\HomeController::class, 'view_work']);
Route::get('/consult/list', [App\Http\Controllers\HomeController::class, 'consult_list']);
Route::post('/edit/consult', [App\Http\Controllers\HomeController::class, 'edit_consult']);
Route::get('/add/event/date', [App\Http\Controllers\HomeController::class, 'add_event_date']);
Route::post('/add/year', [App\Http\Controllers\HomeController::class, 'add_year']);
Route::get('/add/event', [App\Http\Controllers\HomeController::class, 'add_event']);
Route::post('/create/event', [App\Http\Controllers\HomeController::class, 'create_event']);
Route::post('/upload/event/image', [App\Http\Controllers\HomeController::class, 'upload_event_inage']);
Route::get('/info', [App\Http\Controllers\HomeController::class, 'info']);
Route::get('delete/info/{id}', [App\Http\Controllers\HomeController::class, 'delete_info']);

//Route::get('buy/{ref}', 'HomeController@buy');
//Paystack Payment Route


