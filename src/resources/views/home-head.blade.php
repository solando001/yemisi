<div class="widget widget-one">
    <div class="widget-heading">
        <h6 class="">Statistics</h6>
    </div>
    <div class="w-chart">
        <div class="w-chart-section">
            <div class="w-detail">
                <p class="w-title">Total Visits</p>
                <p class="w-stats">{{number_format($visit)}}</p>
            </div>
            <div class="w-chart-render-one">
                <div id="total-users"></div>
            </div>
        </div>

        <div class="w-chart-section">
            <div class="w-detail">
                <p class="w-title">Total Book Sales</p>
                <p class="w-stats">&#8358; 0.00</p>
            </div>
            <div class="w-chart-render-one">
                <div id="paid-visits"></div>
            </div>
        </div>
    </div>
</div>
