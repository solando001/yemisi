<footer class="p-half bg-black2">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 text-center">
                <ul class="footer-icons mb-4">
                    <li><a href="javascript:void(0)" class="wow fadeInUp"><i class="ti ti-facebook"></i> </a> </li>
                    <li><a href="javascript:void(0)" class="wow fadeInDown"><i class="ti ti-twitter"></i> </a> </li>
{{--                    <li><a href="javascript:void(0)" class="wow fadeInUp"><i class="ti ti-google"></i> </a> </li>--}}
{{--                    <li><a href="javascript:void(0)" class="wow fadeInDown"><i class="ti ti-linkedin"></i> </a> </li>--}}
{{--                    <li><a href="javascript:void(0)" class="wow fadeInUp"><i class="ti ti-instagram"></i> </a> </li>--}}
{{--                    <li><a href="javascript:void(0)" class="wow fadeInDown"><i class="ti ti-pinterest"></i> </a> </li>--}}
                </ul>
                <p class="copyrights mt-2 mb-2">© <?php echo date('Y') ?> VoiceOut. Made with love by <a href="https://www.dominahltechnologies.com" target="_blank">Dominahl Technologies LLC</a></p>
            </div>
        </div>
    </div>
</footer>
