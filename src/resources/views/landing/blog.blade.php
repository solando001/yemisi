<!doctype html>

<html class="no-js" lang="zxx">

<head>

    <!-- metas -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Yemisi Adeyeye">
    <meta name="keywords" content="portfolio, personal, corporate, business, parallax, creative, agency">

    <!-- title -->
    <title>My Blog | Yemisi Adeyeye</title>

    <!-- favicon -->
    <link href="img/favicon.ico" rel="icon" type="image/png">

    <!-- bootstrap css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet">

    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- magnific-popup CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">

    <!-- animate.min CSS -->
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- Font Icon Core CSS -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/et-line.css">

    <!-- Core Style Css -->
    <link rel="stylesheet" href="css/style.css">

    <!--[if lt IE 9]-->
    <script src="js/html5shiv.min.js"></script>
    <!--[endif]-->
    <style>
        .circle-icon {
            background: purple;
            color: white;
            padding:8px;
            border-radius: 50%;
        }
    </style>
</head>

<body>

<!-- ====== Preloader ======  -->
<div class="loading">
    <div class="load-circle">
    </div>
</div>
<!-- ======End Preloader ======  -->
@include('landing.nav')



<!--====== Blog ======-->
<section class="blog section-padding bg-gray" data-scroll-index="5">
    <div class="container">
        <div class="row">

            <!-- section heading -->
            <div class="section-head">
                <h3>My Blog.</h3>
            </div>

            <!-- owl carsouel -->
{{--            <div class="owl-carousel owl-theme">--}}

                <!-- pitems -->
            <div class="row">
                @if(isset($posts) && count($posts)>0)
                    @foreach($posts as $post)
                    <div class="pitem col-md-6">
                        <div class="post-img">
                            <img src="{{$post->image}}" alt="">
                        </div>
                        <div class="content">

                            <h6 class="tag">
                                <a href="#">{{$post->category->name}}</a>
                            </h6>

                            <h4>
                                <a href="{{url('blog/'.$post->slug)}}">{{$post->title}}</a>
                            </h4>
                            <span class="more">
                                        <a href="{{url('blog/'.$post->slug)}}">Read More</a>
                                    </span>
                        </div>
                    </div>
                    @endforeach
                @endif

{{--                <div class="pitem col-md-6">--}}
{{--                    <div class="post-img">--}}
{{--                        <img src="img/blog/2.jpg" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="content">--}}
{{--                        <h6 class="tag">--}}
{{--                            <a href="#0">Trends</a>--}}
{{--                        </h6>--}}
{{--                        <h4>--}}
{{--                            <a href="#0">Master These Awesome New Skills in March 2018</a>--}}
{{--                        </h4>--}}
{{--                        <span class="more">--}}
{{--									<a href="#0">Read More</a>--}}
{{--								</span>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="pitem col-md-6">--}}
{{--                    <div class="post-img">--}}
{{--                        <img src="img/blog/3.jpg" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="content">--}}
{{--                        <h6 class="tag">--}}
{{--                            <a href="#0">Themeforest</a>--}}
{{--                        </h6>--}}
{{--                        <h4>--}}
{{--                            <a href="#0">Master These Awesome New Skills in March 2018</a>--}}
{{--                        </h4>--}}
{{--                        <span class="more">--}}
{{--									<a href="#0">Read More</a>--}}
{{--								</span>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="pitem col-md-6">--}}
{{--                    <div class="post-img">--}}
{{--                        <img src="img/blog/4.jpg" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="content">--}}
{{--                        <h6 class="tag">--}}
{{--                            <a href="#0">Trends</a>--}}
{{--                        </h6>--}}
{{--                        <h4>--}}
{{--                            <a href="#0">Master These Awesome New Skills in March 2018</a>--}}
{{--                        </h4>--}}
{{--                        <span class="more">--}}
{{--									<a href="#0">Read More</a>--}}
{{--								</span>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>

        </div><!-- /row -->
    </div><!-- /container -->
</section>
<!--====== End Blog ======-->


@include('landing.footer');
