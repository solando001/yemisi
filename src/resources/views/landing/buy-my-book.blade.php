<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <!-- metas -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Yemisi Adeyeye">
    <meta name="keywords" content="portfolio, personal, corporate, business, parallax, creative, agency">
    <!-- title -->
    <title>Get My Book | Yemisi Adeyeye</title>

    <!-- favicon -->
    <link href="img/favicon.ico" rel="icon" type="image/png">

    <!-- bootstrap css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet">

    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- magnific-popup CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">

    <!-- animate.min CSS -->
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- Font Icon Core CSS -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/et-line.css">

    <!-- Core Style Css -->
    <link rel="stylesheet" href="css/style.css">

    <!--[if lt IE 9]-->
    <script src="js/html5shiv.min.js"></script>
    <!--[endif]-->

    <style>
        .circle-icon {
            background: purple;
            color: white;
            padding:8px;
            border-radius: 50%;
        }

        #hr-zone {
            /*background-color: #FD971F;*/
            background-color: pink;
            color: black;
            font-family: 'Poppins', sans-serif;
            border-bottom-left-radius: 30%;
            border-bottom-right-radius: 30%;
            border-top-left-radius: 30%;
            border-top-right-radius: 30%;
            font-size: x-large;
            padding: 35px;
        }

        #vr-zone{
            background-color: pink;
            color: black;
            font-family: 'Poppins', sans-serif;
            border-bottom-left-radius: 20%;
            border-bottom-right-radius: 20%;
            border-top-left-radius: 20%;
            border-top-right-radius: 20%;
            font-size: x-large;
            padding: 35px;
        }
        #test-zone{
            background-color: pink;
            color: black;
            font-family: 'Poppins', sans-serif;
            /*border-bottom-left-radius: 20%;*/
            /*border-bottom-right-radius: 20%;*/
            /*border-top-left-radius: 20%;*/
            /*border-top-right-radius: 20%;*/
            font-size: large;
            padding: 30px;
        }


    </style>

</head>

<body>

<!-- ====== Preloader ======  -->
<div class="loading">
    <div class="load-circle">
    </div>
</div>
<!-- ======End Preloader ======  -->
@include('landing.nav')


<section class="hero section-padding bg-gray pb-70" data-scroll-index="2">
    <div class="container">
        <div class="section-head">
            <h3>Get My Book.</h3>
        </div>

        <div class="row">

            <div class="col-md-5">
                <div class="hero-img mb-30">
                    <img src="img/book/1.jpg" alt="">
                </div>
            </div>
            <div class="col-md-7">
               <h2 style="color: red; font-size: 35px; font-weight: bolder">
                   HERE&#39;S HOW TO RUN A
                   BUSINESS THAT GIVES YOU
                   MORE PROFITS, PEACE OF
                   MIND, THE ABILITY TO FOCUS
                   ON STRATEGIC
                   PLANNING FOR GROWTH AND
                   QUALITY TIME FOR FAMILY AND
                   TRAVELS.
               </h2>
            </div>

        </div>

        <center><span style='font-size:100px;'>&#9472;</span></center>

        <div class="row">

            <div class="col-md-12">
                <h3 style="font-weight: bolder">
                    I have successfully cracked the code to helping you set up your business in a way that it allows you to
                    have freedom to do other things, travel when and where you want to, spend quality time with your families
                    and friends and keep earning even while on vacation.
                </h3>
            </div>

        </div>
        <br>
        <div class="row">

            <div class="col-md-7">
                <h4 style="text-align: justify">
                    <br><br>
{{--                    This is a picture of my family and I during a vacation to Dubai, UAE.<br><br><br><br>--}}
                    My business partner and I have our business running with staff handling
                        different aspects of the operations while we provide strategic leadership
                        and take top level decisions.
                    <br><br><br><br>
                        In spite of running a 24/7 (Every day all year round) business, we could travel, do other things
                        and still have that peace of mind that our business is running and growing exactly the way
                        it should.
                </h4>

                <center>
                    <button type="button" class="buton boton" data-toggle="modal" data-target="#exampleModal_1">
                        GET YOURS NOW (Bank Transfer)
                    </button>
    
                    <button type="button" class="buton boton" data-toggle="modal" data-target="#exampleModal">
                        GET HARD COPY NOW
                    </button>
                    <a href="https://selar.co/8ab1" target="_blank" class="buton boton">
                        Buy E-COPY NOW
                    </a>
                </center>
                
            </div>
            <div class="col-md-5">
                <div class="hero-img mb-30">
                    <img src="img/book/dubai.jpg" alt="">
                </div>
            </div>

        </div>

        <div  class="row">
            <div class="col-md-12">
                <h4 style="background: pink; color: black; font-size: large; padding: 30px; text-align: justify; border-radius: 20px">
                    You can't leave your business for a while and come back to meet increase in earnings and operations
                    done appropriately. Your business is totally tied to you. Once you stop working, your business stops too.
                    <br>
                    <b>This is not the way it should be.</b>
                </h4>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">

                <br><br><br><br><br><br><br><br>
                <h3 style="font-weight: bolder">A successful business should be able run with or without your physical involvement</h3>

                <br><br><br><br><br><br><br><br>
            </div>

            <div class="col-md-6">
                <br><br>
                <h4 style="background: pink; color: black; font-size: large; padding: 30px; text-align: justify; border-radius: 20px">
                   I will show you exactly what to do to start your business with a structure that gives you freedom and not imprison you.<br>

                   I will expose you to you how you can set up your business (regardless of the kind of business it is) with peace of mind.?<br>

                   I will analyse what to do and avoid when you are building a business. <br>

                   I will also teach you about basic accounting systems that allows you to monitor your finances anywhere you are in the world. <br>

                   I will break down for you how to hire the right persons and how to make sure your staff/employee do exactly the role you assign to them.

                </h4>
            </div>
        </div>
        <br><br>

        <div class="row">

            <div class="col-md-7">
                <h3 style="font-weight: bolder">Let me introduce myself to you.</h3>
                <br>
                <h4 style="text-align: justify">
                    I am Dr. Yemisi Adeyeye, the Cofounder and MD of Lifefount Hospital and Foundation, the founder of Lifefount Business Network.
                    <br>
                    I have trained and mentored over 5,000 business owners. I am an international Mentor for Tony Elumelu Foundation, an ambassador/Influencer for Her
                    Venture App of Cherie Blair Foundation and ExxonMobil Mobil, I am the Chairperson of Neca's Network of Entrepreneurial Women (Nnew kwara).
                    <br>
                    I am the relatable business teacher.
                    <br>
                    I teach business structure in the easiest and sweetest ways.
                    In my years of experience as a Successful Entrepreneur, I have learned, unlearned and relearned a whole lot of things.

                </h4>
            </div>

            <div class="col-md-5">
                <div class="hero-img mb-30">
                    <img src="img/book/1.jpg" alt="">
                </div>
            </div>

        </div>
        <br>
        <div class="row">
            <div class="col-md-5">
                <div class="hero-img mb-30">
                    <img src="img/book/Adeyeye.jpg" alt="">
                </div>
            </div>

            <div class="col-md-7">
                <h4 style="text-align: justify">
                    More so, I have interacted with several entrepreneurs; both start-ups and established business
                    owners and I know exactly what pain they are going through.
                    <br>
                    Having trained and consulted for over 5000 businesses on different platforms including but not
                    limited to First Bank Nigeria, Tony Elumelu Foundation, Fate foundation, Neca's Network of
                    Entrepreneurial Women (Nnew), Access bank ,Global Entrepreneurship Network (GEN), Dubai Leadership
                    Summit and I run a successful and profitable Hospital and foundation, I can confidently
                    say that I am an expert in Business Development.
                </h4>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
               <strong style="font-size: x-large">
                   With the stress I see Startups and already established business owners go through, I decided to create a solution rather than just complaining like others.
               </strong>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-5">
                <div class="hero-img mb-30">
                    <img src="img/book/book.jpg" alt="">
                </div>
            </div>
            <div class="col-md-7">
                <h3><strong>I PRESENT TO YOU  "Entrepreneurship is a Beautiful Thing"</strong></h3><br>
                <h4>This book actually provides answers with practical dimensions to your worries on</h4><br>
                <h4>
                    &bull;  Setting up systems and structures for your business<br>
                    &bull;  HR management<br>
                    &bull;  Accounting<br>
                    &bull;  Marketing<br>
                    &bull;  Finance<br>
                    And many more
                </h4>
                <center>
                    <button type="button" class="buton boton" data-toggle="modal" data-target="#exampleModal_1">
                        GET YOURS NOW (Bank Transfer)
                    </button>

                    <button type="button" class="buton boton" data-toggle="modal" data-target="#exampleModal">
                        GET HARD COPY NOW
                    </button>
                    <a href="https://selar.co/8ab1" target="_blank" class="buton boton">
                        Buy E-COPY NOW
                    </a>
                </center>
            </div>
        </div>
            <br><br>
        <div class="row">
            <div class="col-md-12">
                <center><h4><i>Read what these people has to say about the book</i></h4></center>
            </div>
            <br><br>

            <div class="col-md-6">
                <h4 style="background: pink; color: black; font-size: large; padding: 30px; text-align: justify">

                    The things I have learnt are numerous and it has wired me up for a more
                    structured & profitable business. I learned to be more structured in my book keeping & accounting,
                    pay myself salary based on the weight of my business & avoid spending my business income
                    for my personal needs. I have a better understanding of cash flow management now.
                    <br>
                    These learnings are awesome and very profitable to my business in giving it a shift.
                    Thanks to  Dr. Yemisi Adeyeye for this business school in  a book
                    <hr>
                    <b><i>Nelson Olusola</i></b>

                </h4>
            </div>

            <div class="col-md-6">
                <h4 style="background: pink; color: black; font-size: large; padding: 30px; text-align: justify">

                    I am grateful for the lessons learned from this book. This book has lots of lessons for entrepreneurs.
                    <br><br>
                    I have learned how to train my staff on the operations of my school and how to free up time so I can focus on strategic management and growth of my business.
                    My human resource management skill is sharper now. This is a most have business guide for all entrepreneurs. It's your passport to peace of mind and business growth.
                    <hr>
                    <b><i>Debbie Olanipekun</i></b>

                </h4>
            </div>
{{--            <div class="clearfix"></div>--}}

            <div class="col-md-12">
                <br><br>
               <h4> If you are desirous of running a business with peace of mind, more income and relevance, you need this book.
                   For $14(#5,000) only, you can get your copy sent to you.</h4>


                <center><span style='font-size:100px;'>&#9472;</span></center>


            <center>
                <button type="button" class="buton boton" data-toggle="modal" data-target="#exampleModal_1">
                    GET YOURS NOW (Bank Transfer)
                </button>

                <button type="button" class="buton boton" data-toggle="modal" data-target="#exampleModal">
                    GET HARD COPY NOW
                </button>
                <a href="https://selar.co/8ab1" target="_blank" class="buton boton">
                    Buy E-COPY NOW
                </a>
            </center>

                <div class="modal fade" id="exampleModal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Bank Transfer Payment</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <ul>
                                    <li>Account Name : <b>Adeyeye Rebecca Oluwayemisi</b></li>
                                    <li>Account Number : <b>1775027024</b></li>
                                    <li>Bank Name : <b>First City Monument Bank (FCMB)</b></li>
                                    <li>Amount : <b>&#8358;5,000</b></li>
                                </ul>
                                <hr>
                                <b style="color: red"><i>After successful payment, please contact this number 0909 440 7502 with proof of payment.</i></b>
                                <h3></h3>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                {{--                <button type="button" class="btn btn-primary">Save changes</button>--}}
                            </div>
                        </div>
                    </div>
                </div>

                <br><br>

                @if(session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                <div class="modal fade" data-keyboard="false" data-backdrop="static" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Enter Your Information</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <form method="POST" action="{{ url('customer') }}" enctype="multipart/form-data" class="form-inlineoo">
                                    @csrf

                                    <div class="contact-form">



                                    {{--                                            <!-- IF MAIL SENT SUCCESSFULLY -->--}}
                                    {{--                                            <div class="success">--}}
                                    {{--                                                Your message has been sent successfully.--}}
                                    {{--                                            </div>--}}
                                    {{--                                            <!-- IF MAIL SENDING UNSUCCESSFULL -->--}}
                                    {{--                                            <div class="error">--}}
                                    {{--                                                E-mail must be valid and message must be longer than 1 character.--}}
                                    {{--                                            </div>--}}
                                    <!-- Form Fields -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="sr-only" for="first_name">Full Name</label>
                                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="Full Name" autofocus>
                                                {{--                                                        @error('name')--}}
                                                {{--                                                        <span class="invalid-feedback" role="alert">--}}
                                                {{--                                                            <strong>{{ $message }}</strong>--}}
                                                {{--                                                        </span>--}}
                                                {{--                                                        @enderror--}}
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="sr-only" for="last_name">Email</label>
                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email Address" >
                                                {{--                                                    @error('email')--}}
                                                {{--                                                    <span class="invalid-feedback" role="alert">--}}
                                                {{--                                                        <strong>{{ $message }}</strong>--}}
                                                {{--                                                    </span>--}}
                                                {{--                                                    @enderror--}}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="sr-only" for="phone_number">Phone</label>
                                                <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" placeholder="Phone Number" >
                                                {{--                                                    @error('phone')--}}
                                                {{--                                                    <span class="invalid-feedback" role="alert">--}}
                                                {{--                                                        <strong>{{ $message }}</strong>--}}
                                                {{--                                                    </span>--}}
                                                {{--                                                    @enderror--}}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="sr-only" for="email_address">Email</label>
                                                <select id="gender" name="gender" class="form-control @error('gender') is-invalid @enderror" >
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>
                                                </select>
                                                {{--                                                    @error('gender')--}}
                                                {{--                                                    <span class="invalid-feedback" role="alert">--}}
                                                {{--                                                        <strong>{{ $message }}</strong>--}}
                                                {{--                                                    </span>--}}
                                                {{--                                                    @enderror--}}
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">

                                                <input id="location" type="text" class="form-control @error('location') is-invalid @enderror" name="location" value="{{ old('location') }}" placeholder="Location" >
                                                {{--                                                    @error('location')--}}
                                                {{--                                                    <span class="invalid-feedback" role="alert">--}}
                                                {{--                                                        <strong>{{ $message }}</strong>--}}
                                                {{--                                                    </span>--}}
                                                {{--                                                    @enderror--}}
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">

                                                <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" placeholder="Address" >
                                                {{--                                                    @error('address')--}}
                                                {{--                                                    <span class="invalid-feedback" role="alert">--}}
                                                {{--                                                        <strong>{{ $message }}</strong>--}}
                                                {{--                                                    </span>--}}
                                                {{--                                                    @enderror--}}
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">

                                                <input id="coupon" type="text" class="form-control @error('coupon') is-invalid @enderror" name="coupon" value="{{ old('coupon') }}" placeholder="Coupon Code" >
                                                {{--                                                    @error('address')--}}
                                                {{--                                                    <span class="invalid-feedback" role="alert">--}}
                                                {{--                                                        <strong>{{ $message }}</strong>--}}
                                                {{--                                                    </span>--}}
                                                {{--                                                    @enderror--}}
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">

                                                <select id="quantity" name="quantity" class="form-control @error('gender') is-invalid @enderror">

                                                    <?php
                                                    for($i=1; $i<101; $i++){
                                                    ?>
                                                    <option value="{{$i}}">{{$i}}</option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <?php $ref = Ramsey\Uuid\Uuid::uuid4();?>
                                        <input type="hidden" name="book_ref" value="{{$sale->ref}}">
                                        <input type="hidden" name="ref" value="{{$ref}}">

                                        <button type="submit" class="btn btn-primary">Continue</button>


                                        {{--                                            <div class="col-md-12 text-center">--}}
                                        {{--                                                <button type="submit" name="submit" id="submit">Contact Now</button>--}}
                                        {{--                                            </div>--}}

                                    </div>
                                </form>

                                {{--                                <form method="POST" action="{{ url('customer') }}" enctype="multipart/form-data"--}}
                                {{--                                    <div class="form-group row">--}}

                                {{--                                        <div class="col-md-12">--}}
                                {{--                                            <label for="email">Full Name</label>--}}
                                {{--                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autofocus>--}}
                                {{--                                            @error('name')--}}
                                {{--                                            <span class="invalid-feedback" role="alert">--}}
                                {{--                                                <strong>{{ $message }}</strong>--}}
                                {{--                                            </span>--}}
                                {{--                                            @enderror--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="col-md-12">--}}
                                {{--                                            <label for="email">Email Address</label>--}}
                                {{--                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" >--}}
                                {{--                                            @error('email')--}}
                                {{--                                            <span class="invalid-feedback" role="alert">--}}
                                {{--                                                <strong>{{ $message }}</strong>--}}
                                {{--                                            </span>--}}
                                {{--                                            @enderror--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="col-md-12">--}}
                                {{--                                            <label for="email">Phone Number</label>--}}
                                {{--                                            <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" >--}}
                                {{--                                            @error('phone')--}}
                                {{--                                            <span class="invalid-feedback" role="alert">--}}
                                {{--                                                <strong>{{ $message }}</strong>--}}
                                {{--                                            </span>--}}
                                {{--                                            @enderror--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="col-md-12">--}}
                                {{--                                            <label for="email">Gender</label>--}}
                                {{--                                            <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" autofocus>--}}
                                {{--                                           <select id="gender" name="gender" class="form-control @error('gender') is-invalid @enderror" >--}}
                                {{--                                               <option value="Male">Male</option>--}}
                                {{--                                               <option value="Female">Female</option>--}}
                                {{--                                           </select>--}}
                                {{--                                            @error('gender')--}}
                                {{--                                            <span class="invalid-feedback" role="alert">--}}
                                {{--                                                <strong>{{ $message }}</strong>--}}
                                {{--                                            </span>--}}
                                {{--                                            @enderror--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="col-md-12">--}}
                                {{--                                            <label for="email">City/Town</label>--}}
                                {{--                                            <input id="location" type="text" class="form-control @error('location') is-invalid @enderror" name="location" value="{{ old('location') }}" autofocus>--}}
                                {{--                                            @error('location')--}}
                                {{--                                            <span class="invalid-feedback" role="alert">--}}
                                {{--                                                <strong>{{ $message }}</strong>--}}
                                {{--                                            </span>--}}
                                {{--                                            @enderror--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="col-md-12">--}}
                                {{--                                            <label for="email">Address</label>--}}
                                {{--                                            <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" autofocus>--}}
                                {{--                                            @error('address')--}}
                                {{--                                            <span class="invalid-feedback" role="alert">--}}
                                {{--                                                <strong>{{ $message }}</strong>--}}
                                {{--                                            </span>--}}
                                {{--                                            @enderror--}}
                                {{--                                        </div>--}}
                                {{--                                        <input type="hidden" name="book_ref" value="{{$sale->ref}}">--}}

                                {{--                                        <button class="btn btn-success">Submit</button>--}}
                                {{--                                    </div>--}}
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                {{--                                <button type="button" class="btn btn-primary">Save changes</button>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                   <h5>
                       Dr. Yemisi Adeyeye<br>
                       Author,<br>
                       <b>Entrepreneurship is a Beautiful Thing</b><br>
                   </h5>
                    <br><br>
                    <center>
                        <h5>
                            <b><i>
                                    PS: If you want to run a business the right way, you can get this Business Guide book right away for a fee less than what can feed you well.
                                    Also note that it is a perfect gift for aspiring and existing entrepreneurs.  Buy it for them and watch them grow luxuriantly.
                            </i></b>
                        </h5>
                    </center>
                </div>
            </div>


        </div>
        </div>

    </div>
</section>



@include('landing.footer');
