<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <!-- metas -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Yemisi Adeyeye">
    <meta name="keywords" content="portfolio, personal, corporate, business, parallax, creative, agency">

    <!-- title -->
    <title>Get My Book | Yemisi Adeyeye</title>

    <!-- favicon -->
    <link href="img/favicon.ico" rel="icon" type="image/png">

    <!-- bootstrap css -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet">

    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">

    <!-- magnific-popup CSS -->
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">

    <!-- animate.min CSS -->
    <link rel="stylesheet" href="{{asset('css/animate.min.css')}}">

    <!-- Font Icon Core CSS -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/et-line.css')}}">

    <!-- Core Style Css -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <!--[if lt IE 9]-->
    <script src="{{asset('js/html5shiv.min.js')}}"></script>
    <!--[endif]-->

    <style>
        .circle-icon {
            background: purple;
            color: white;
            padding:8px;
            border-radius: 50%;
        }
    </style>

</head>

<body>

<!-- ====== Preloader ======  -->
<div class="loading">
    <div class="load-circle">
    </div>
</div>
<!-- ======End Preloader ======  -->
@include('landing.nav')

<!-- ====== Services ======  -->
{{--<section class="services section-padding bg-gray text-center pb-70" data-scroll-index="2">--}}
<section class="hero section-padding bg-gray pb-70" data-scroll-index="2">
    <div class="container">

        <!-- section heading -->
        <div class="section-head">
            <h3>Get My Book.</h3>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="hero-img mb-30">
                    <img src="{{asset('img/hrpolicy1.jpeg')}}" alt="">
                </div>
            </div>

            <!-- content -->
            <div class="col-md-12">
                <p class="content mb-30">

                <div class="clearfix"></div>

                <hr>

{{--                <table class="table">--}}
{{--                    <thead>--}}
{{--                    <tr>--}}
{{--                        <th scope="col">Fields</th>--}}
{{--                        <th scope="col">Name</th>--}}
{{--                    </tr>--}}
{{--                    </thead>--}}
{{--                    <tbody>--}}
{{--                    <tr>--}}
{{--                        <th>Quantity Requested</th>--}}
{{--                        <td style="text-align: justify">{{$cus->quantity}} copi(es)</td>--}}
{{--                    </tr>--}}
{{--                    <tr>--}}
{{--                        <th>Amount to Pay</th>--}}
{{--                        <td style="text-align: justify">{{number_format($price*$cus->quantity)}}</td>--}}
{{--                    </tr>--}}

{{--                    </tr>--}}
{{--                    <tr>--}}
{{--                        <th>Amount to Pay</th>--}}
{{--                        <td style="text-align: justify">--}}


<center>                      <p> Amount to pay &#8358;50,000 only. Please click the button below to make payment</p>
                               <form method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
                                <div class="row" style="margin-bottom:40px;">
                                    <div class="col-md-8 col-md-offset-2">
                                        <input type="hidden" name="email" value="{{$cus->email}}">
                                        <input type="hidden" name="orderID" value="345">
                                        <input type="hidden" name="amount" value="{{50000 * 100}}">
                                        <input type="hidden" name="quantity" value="1">
                                        <input type="hidden" name="currency" value="NGN">
                                        <input type="hidden" name="metadata" value="{{ json_encode($array = ['customer_id' => $cus->id, 'book_id' => 1, 'type' => 'book']) }}" >
                                        <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <center><button type="submit" class="buton buton-bg" >Buy Now!</button></center>
                                    </div>
                                </div>
                            </form>
</center>

                {{--                        </td>--}}
{{--                    </tr>--}}
{{--                    </tbody>--}}
{{--                </table>--}}
{{--                <br>--}}
                <br>
            </div>

        </div>
    </div><!-- /row -->
    </div><!-- /container -->
</section>
<!-- ====== End Services ======  -->

<!--====== Footer ======-->
<footer>
    <div class="container text-center">
        <div class="row">
            <p>Copy Right <?php echo date('Y') ?> &copy; Yemisi Adeyeye, All Rights Reserved</p>
        </div>
    </div>
</footer>
<!--====== End Footer ======-->

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>

<!-- bootstrap -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>

<!-- scrollIt -->
<script src="{{asset('js/scrollIt.min.js')}}"></script>

<!-- magnific-popup -->
<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>

<!-- owl carousel -->
<script src="{{asset('js/owl.carousel.min.js')}}"></script>

<!-- stellar js -->
<script src="{{asset('js/jquery.stellar.min.js')}}"></script>

<!-- animated.headline -->
<script src="{{asset('js/animated.headline.js')}}"></script>

<!-- jquery.waypoints.min js -->
<script src="{{asset('js/jquery.waypoints.min.js')}}"></script>

<!-- jquery.counterup.min js -->
<script src="{{asset('js/jquery.counterup.min.js')}}"></script>

<!-- isotope.pkgd.min js -->
<script src="{{asset('js/isotope.pkgd.min.js')}}"></script>

<!-- validator js -->
<script src="{{asset('js/validator.js')}}"></script>

<!-- custom script -->
<script src="{{asset('js/custom.js')}}"></script>

</body>
</html>
