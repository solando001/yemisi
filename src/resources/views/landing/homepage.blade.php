<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home | Yemisi Adeyeye</title>
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}" media="all" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}" media="all" />
    <!-- Lightbox CSS -->
{{--    <link rel="stylesheet" href="{{asset('css/lightbox.min.css')}}"/>--}}
    <!-- Flaticon CSS -->
    <link rel="stylesheet" href="{{asset('flaticon/flaticon.css')}}">
    <!-- Owl carousel CSS -->
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{asset('css/animate.min.css')}}">
    <!-- Reset CSS -->
    <link rel="stylesheet" href="{{asset('css/reset.css')}}">
    <!-- Main style CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('style.css')}}" media="all" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}" media="all" />
    <!-- Freelancer colors. You can choose any other color by changing color css file.
    -->
    <!-- <link rel="stylesheet" type="text/css" href="css/colors/default.css"> -->
    <!-- <link rel="stylesheet" type="text/css" href="css/colors/red.css"> -->
    <!-- <link rel="stylesheet" type="text/css" href="css/colors/blue.css"> -->
    <!-- <link rel="stylesheet" type="text/css" href="css/colors/green.css"> -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="js" data-spy="scroll" data-target=".navbar" data-offset="50">
<!-- Page loader -->
<div id="preloader"></div>
<!-- Header area start -->
<header id="menu" class="menubar">
    <nav class="navbar navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#menu">Freel<i class="flaticon-cursor"></i>ncer</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right menu">
                    <li><a href="#service" class="menu-color">Services</a></li>
                    {{--                    <li><a href="#process" class="menu-color">Process</a></li>--}}
                    <li><a href="#experience" class="menu-color">My Books</a></li>
                    {{--                    <li><a href="#portfolio" class="menu-color" >Portfolio</a></li>--}}
                    <li><a href="#review" class="menu-color">Testimonials</a></li>
                    <li><a href="#blog" class="menu-color">Blog</a></li>
{{--                    <li><a href="#contact" class="menu-color">HIRE ME</a></li>--}}
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>

<!-- Header area end -->
<!-- Slider area start -->
<section class="slider">
    <div class="slider-img">
        <div class="slider-overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="slider-content wow zoomIn">
                            <div class="slider-content-left col-md-8">
                                <img src="images/home3.jpg" alt="" />
                            </div>
                            <div class="slider-content-right col-md-4">
                                <div class="slider-icon text-right">
                                    <a href="https://web.facebook.com/yemisi.adeyeye.5" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="https://twitter.com/drroadeyeye/" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href="https://ng.linkedin.com/in/adeyeye-yemisi-0b916055" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                </div>
                                <div class="slider-text">
                                    <h2>I'm Yemisi Adeyeye</h2>
                                    <p style="text-align: justify">I'm glad you are here! I help business owners and organizations to understand and create structure in their businesses so they can enjoy all the benefits
                                        of entrepreneurship and growth. I am passionate about business and individual healthiness. Let me take you through the journey.</p>
                                    <ul>
                                        <li><i class="fa fa-check" aria-hidden="true"></i>Business Training & Consultancy</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i>Business Management</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i>Coaching and Mentorship</li>
{{--                                        <li><i class="fa fa-check" aria-hidden="true"></i>Business Training & Consultancy</li>--}}
                                    </ul>
                                    <a href="#experience">Buy My Book</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- Slider area end -->
<!-- Service top area start -->
<section class="service-top" id="service">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="service-top-head wow roolIn">
                    <h2 class="head-two">Services</h2>
                    <div class="top-head-before">
                        <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6 text-center">
                <div class="service-top-content wow fadeInUp">
                    <i class="fa fa-object-group" aria-hidden="true"></i>
                    <h5>Unique</h5>
                    <h5>Website Design</h5>
                    <div class="service-top-icon">
                        <a href="#"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 text-center">
                <div class="service-top-content wow fadeInUp">
                    <i class="fa fa-bullhorn" aria-hidden="true"></i>
                    <h5>Google</h5>
                    <h5>AdWords Management</h5>
                    <div class="service-top-icon">
                        <a href="#"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 text-center">
                <div class="service-top-content wow fadeInUp">
                    <i class="fa fa-bar-chart" aria-hidden="true"></i>
                    <h5>Digital</h5>
                    <h5>Asset Evaluations</h5>
                    <div class="service-top-icon">
                        <a href="#"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- Service top area end -->
<!-- Service bottom area start -->
<section class="service-bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="service-bottom-head">
                    <h3>I am dedicated to providing excellent service</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="service-bottom-content wow fadeInUp">
                    <p style="text-align: justify">Entrepreneurship is a beautiful thing  is a guide book on how to structure and grow your business.

                        This book focuses on the fundamentals of entrepreneurship and arm you with skills that will transform your business to an organisation that can run with or without your active supervision.

                        It is spiced with stories of the author's experience on her entrepreneurial journey and her learnings from different
                        institutions, organisations,networks and mentors.</p>
                    <p>Reading this book will add immense values to you and your business which includes:</p>
                    <ul>
                        <li><i class="fa fa-check" aria-hidden="true"></i>Staff management  skills using  policies,processes and standard operating procedures </li>
                        <li><i class="fa fa-check" aria-hidden="true"></i>Understanding of financial laws and acts that guides enterprise management.</li>
                        <li><i class="fa fa-check" aria-hidden="true"></i>Understanding of basics of bookkeeping ,accounting and marketing for Micro, Small and Medium Enterprises Sector (MSME)</li>
                        <li><i class="fa fa-check" aria-hidden="true"></i>Gain more confidence in delegating duties that relates to business operations and focus on the management of your business.</li>
                        <li><i class="fa fa-check" aria-hidden="true"></i>Boost your corporate governance skill and make your business ready for investment and numerous other opportunities available to MSME Sector.</li>
                        <li><i class="fa fa-check" aria-hidden="true"></i>Earn you quality time for family and other things you have passion for without worrying about your business whether it will crash in your absence.</li>
                    </ul>
                </div>
                <hr>
{{--                <a href="{{url('/buy/'.$sale->ref)}}" class="btn btn-success" >Buy Now</a>--}}

                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                  Buy Now
                </button>

                <div class="modal fade" data-keyboard="false" data-backdrop="static" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Enter Your Information</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <form method="POST" action="{{ url('customer') }}" enctype="multipart/form-data" class="form-inlineoo">
                                    @csrf

                                    <div class="contact-form">



{{--                                            <!-- IF MAIL SENT SUCCESSFULLY -->--}}
{{--                                            <div class="success">--}}
{{--                                                Your message has been sent successfully.--}}
{{--                                            </div>--}}
{{--                                            <!-- IF MAIL SENDING UNSUCCESSFULL -->--}}
{{--                                            <div class="error">--}}
{{--                                                E-mail must be valid and message must be longer than 1 character.--}}
{{--                                            </div>--}}
                                            <!-- Form Fields -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="sr-only" for="first_name">Full Name</label>
                                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="Full Name" autofocus>
{{--                                                        @error('name')--}}
{{--                                                        <span class="invalid-feedback" role="alert">--}}
{{--                                                            <strong>{{ $message }}</strong>--}}
{{--                                                        </span>--}}
{{--                                                        @enderror--}}
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="sr-only" for="last_name">Email</label>
                                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email Address" >
{{--                                                    @error('email')--}}
{{--                                                    <span class="invalid-feedback" role="alert">--}}
{{--                                                        <strong>{{ $message }}</strong>--}}
{{--                                                    </span>--}}
{{--                                                    @enderror--}}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="sr-only" for="phone_number">Phone</label>
                                                    <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" placeholder="Phone Number" >
{{--                                                    @error('phone')--}}
{{--                                                    <span class="invalid-feedback" role="alert">--}}
{{--                                                        <strong>{{ $message }}</strong>--}}
{{--                                                    </span>--}}
{{--                                                    @enderror--}}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="sr-only" for="email_address">Email</label>
                                                    <select id="gender" name="gender" class="form-control @error('gender') is-invalid @enderror" >
                                                       <option value="Male">Male</option>
                                                       <option value="Female">Female</option>
                                                   </select>
{{--                                                    @error('gender')--}}
{{--                                                    <span class="invalid-feedback" role="alert">--}}
{{--                                                        <strong>{{ $message }}</strong>--}}
{{--                                                    </span>--}}
{{--                                                    @enderror--}}
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">

                                                    <input id="location" type="text" class="form-control @error('location') is-invalid @enderror" name="location" value="{{ old('location') }}" placeholder="Location" >
{{--                                                    @error('location')--}}
{{--                                                    <span class="invalid-feedback" role="alert">--}}
{{--                                                        <strong>{{ $message }}</strong>--}}
{{--                                                    </span>--}}
{{--                                                    @enderror--}}
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">

                                                    <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" placeholder="Address" >
{{--                                                    @error('address')--}}
{{--                                                    <span class="invalid-feedback" role="alert">--}}
{{--                                                        <strong>{{ $message }}</strong>--}}
{{--                                                    </span>--}}
{{--                                                    @enderror--}}
                                                </div>
                                            </div>
                                                    <?php $ref = Ramsey\Uuid\Uuid::uuid4();?>
                                                <input type="hidden" name="book_ref" value="{{$sale->ref}}">
                                                <input type="hidden" name="ref" value="{{$ref}}">

                                            <button type="submit" class="btn btn-primary">Save changes</button>


{{--                                            <div class="col-md-12 text-center">--}}
{{--                                                <button type="submit" name="submit" id="submit">Contact Now</button>--}}
{{--                                            </div>--}}

                                    </div>
                                </form>

{{--                                <form method="POST" action="{{ url('customer') }}" enctype="multipart/form-data"--}}
{{--                                    <div class="form-group row">--}}

{{--                                        <div class="col-md-12">--}}
{{--                                            <label for="email">Full Name</label>--}}
{{--                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autofocus>--}}
{{--                                            @error('name')--}}
{{--                                            <span class="invalid-feedback" role="alert">--}}
{{--                                                <strong>{{ $message }}</strong>--}}
{{--                                            </span>--}}
{{--                                            @enderror--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-12">--}}
{{--                                            <label for="email">Email Address</label>--}}
{{--                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" >--}}
{{--                                            @error('email')--}}
{{--                                            <span class="invalid-feedback" role="alert">--}}
{{--                                                <strong>{{ $message }}</strong>--}}
{{--                                            </span>--}}
{{--                                            @enderror--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-12">--}}
{{--                                            <label for="email">Phone Number</label>--}}
{{--                                            <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" >--}}
{{--                                            @error('phone')--}}
{{--                                            <span class="invalid-feedback" role="alert">--}}
{{--                                                <strong>{{ $message }}</strong>--}}
{{--                                            </span>--}}
{{--                                            @enderror--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-12">--}}
{{--                                            <label for="email">Gender</label>--}}
{{--                                            <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" autofocus>--}}
{{--                                           <select id="gender" name="gender" class="form-control @error('gender') is-invalid @enderror" >--}}
{{--                                               <option value="Male">Male</option>--}}
{{--                                               <option value="Female">Female</option>--}}
{{--                                           </select>--}}
{{--                                            @error('gender')--}}
{{--                                            <span class="invalid-feedback" role="alert">--}}
{{--                                                <strong>{{ $message }}</strong>--}}
{{--                                            </span>--}}
{{--                                            @enderror--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-12">--}}
{{--                                            <label for="email">City/Town</label>--}}
{{--                                            <input id="location" type="text" class="form-control @error('location') is-invalid @enderror" name="location" value="{{ old('location') }}" autofocus>--}}
{{--                                            @error('location')--}}
{{--                                            <span class="invalid-feedback" role="alert">--}}
{{--                                                <strong>{{ $message }}</strong>--}}
{{--                                            </span>--}}
{{--                                            @enderror--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-12">--}}
{{--                                            <label for="email">Address</label>--}}
{{--                                            <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" autofocus>--}}
{{--                                            @error('address')--}}
{{--                                            <span class="invalid-feedback" role="alert">--}}
{{--                                                <strong>{{ $message }}</strong>--}}
{{--                                            </span>--}}
{{--                                            @enderror--}}
{{--                                        </div>--}}
{{--                                        <input type="hidden" name="book_ref" value="{{$sale->ref}}">--}}

{{--                                        <button class="btn btn-success">Submit</button>--}}
{{--                                    </div>--}}
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
{{--                                <button type="button" class="btn btn-primary">Save changes</button>--}}
                            </div>
                        </div>
                    </div>
                </div>

{{--                <form method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">--}}
{{--                    <div class="row" style="margin-bottom:40px;">--}}
{{--                        <div class="col-md-8 col-md-offset-2">--}}

{{--                            <input type="hidden" name="email" value="otemuyiwa@gmail.com"> --}}{{-- required --}}
{{--                            <input type="hidden" name="orderID" value="345">--}}
{{--                            <input type="hidden" name="amount" value="{{$sale->price * 100}}"> --}}{{-- required in kobo --}}
{{--                            <input type="hidden" name="quantity" value="1">--}}
{{--                            <input type="hidden" name="currency" value="NGN">--}}
{{--                            <input type="hidden" name="metadata" value="{{ json_encode($array = ['name' => 'Oluwatobi', 'book_id' => $sale->id]) }}" > --}}{{-- For other necessary things you want to add to your payload. it is optional though --}}
{{--                            <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> --}}{{-- required --}}
{{--                            {{ csrf_field() }} --}}{{-- works only when using laravel 5.1, 5.2 --}}

{{--                            <input type="hidden" name="_token" value="{{ csrf_token() }}"> --}}{{-- employ this in place of csrf_field only in laravel 5.0 --}}

{{--                            <button type="submit" class="btn btn-success btn-lg " >Buy Now!</button>--}}
{{--                            <p>--}}
{{--                                <button class="btn btn-success btn-lg btn-block" type="submit" value="Pay Now!">--}}
{{--                                    <i class="fa fa-plus-circle fa-lg"></i> Pay Now!--}}
{{--                                </button>--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </form>--}}

            </div>

            <div class="col-md-5">
                <div class="service-bottom-content wow fadeInUp">
                    <img src="images/service-right.png" alt="" />
{{--                    <div class="service-bottom-web">--}}
{{--                        <a href="">Buy Now</a>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</section><!-- Service bottom area end -->

<section class="counterup text-center" data-parallax="scroll">
    <div class="counterup-overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="count">
                        <span class="counter">234</span>
                        <h4>Happy Clients</h4>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="count">
                        <span class="counter">1089</span>
                        <h4>Coffee Cups</h4>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="count">
                        <span class="counter">107</span>
                        <h4>Projects Completed</h4>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="count">
                        <span class="counter">43</span>
                        <h4>Awards Won</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- Counter-up area end -->
<!-- Experience area start -->
<section class="experience" data-parallax="scroll" id="experience">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="experience-top-head">
                    <h2 class="head-two">My Books</h2>
                    <div class="top-head-before">
                        <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="carousel slide" data-ride="carousel" id="quote">
                    <!-- Bottom Carousel Indicators -->
{{--                    <ol class="carousel-indicators">--}}
{{--                        <li data-target="#quote" data-slide-to="0" class="active">--}}
{{--                            <h4>2014</h4>--}}
{{--                            <i class="fa fa-bullseye" aria-hidden="true"></i>--}}
{{--                        </li>--}}
{{--                        <li data-target="#quote" data-slide-to="1">--}}
{{--                            <h4>2015</h4>--}}
{{--                            <i class="fa fa-bullseye" aria-hidden="true"></i>--}}
{{--                        </li>--}}
{{--                        <li data-target="#quote" data-slide-to="2">--}}
{{--                            <h4>2016</h4>--}}
{{--                            <i class="fa fa-bullseye" aria-hidden="true"></i>--}}
{{--                        </li>--}}
{{--                        <li data-target="#quote" data-slide-to="3">--}}
{{--                            <h4>2017</h4>--}}
{{--                            <i class="fa fa-bullseye" aria-hidden="true"></i>--}}
{{--                        </li>--}}
{{--                    </ol>--}}
                    <!-- Carousel Slides / Quotes -->
                    <div class="carousel-inner">
                        <!-- Quote 1 -->
                        <?php $i = 1; ?>
                        @foreach($books as $book)
                            @if($i++ == 1)
                                <div class="item active">
                            @else
                                <div class="item">
                            @endif
                            <div class="experience-slide-one">
                                <div class="col-md-5">
                                    <div class="experience-slide-img">
                                        <img src="images/experience.jpg" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="experience-slide-text">
                                        <h3>{{$book->name}} | &#8358; {{number_format($book->price)}}</h3>
                                        <p>{{$book->description}}</p>
                                        <hr>
                                        <a href="#" class="btn btn-success btn-lg">Buy Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                    <!-- Carousel Buttons Next/Prev -->
                    <a data-slide="prev" href="#quote" class="left carousel-control"><i class="fa fa-angle-left"></i></a>
                    <a data-slide="next" href="#quote" class="right carousel-control"><i class="fa fa-angle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</section><!-- Experience area end -->

<section class="review" id="review">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="review-slider owl-carousel">
                    <div class="review-slide-one">
                        <div class="review-left text-center">
                            <div class="review-left-bg">
                                <div class="review-left-bg-color"></div>
                            </div>
                            <div class="review-left-over">
                                <img src="images/review-img.png" alt="" />
                                <div class="review-left-text">
                                    <h3>Mazuba Mwanachingwala (Mrs).</h3>
                                    <p>Director-MFOUR AGRO LIMITED</p>
                                </div>
                            </div>
                            <div class="review-left-before">
                                <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="review-right">
                            <h1>Testimonials</h1>
                            <i class="fa fa-quote-left" aria-hidden="true"></i><p>
                                Dr. Yemisi Adeyeye is a very hardworking, determined, innovative, goal oriented, compassionate
                                mentor I have ever come across. She is also very accommodative and easy to get along with always
                                determined to make others succeed as well. I have also been following her workshops on face book and
                                they are benefiting a lot of people.
                                I will forever remain grateful for the knowledge and skills that I continue acquiring from Dr. Yemisi
                                Adeyeye. I am wishing her all the best in her future undertaking.

                            </p><i class="fa fa-quote-right" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="review-slide-one">
                        <div class="review-left text-center">
                            <div class="review-left-bg">
                                <div class="review-left-bg-color"></div>
                            </div>
                            <div class="review-left-over">
                                <img src="images/review-img.png" alt="" />
                                <div class="review-left-text">
                                    <h3>Farohunbi Samuel</h3>
                                    <p>CEO, Goodnews Infotechnologies, Nigeria</p>
                                </div>
                            </div>
                            <div class="review-left-before">
                                <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="review-right">
                            <h1>Testimonials</h1>
                            <i class="fa fa-quote-left" aria-hidden="true"></i><p>
                                Dr. Yemisi Adeyeye is a  medicopreneur who teaches business and mentors business owners passionately.
                                If you've ever heard her speak in any business conference/training, you will always look forward to meeting her for personal business mentoring. She's a compendium of knowledge. She articulates her experience and expertise in
                                mentoring and transforming small, medium and large scale businesses.
                            </p><i class="fa fa-quote-right" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="review-slide-one">
                        <div class="review-left text-center">
                            <div class="review-left-bg">
                                <div class="review-left-bg-color"></div>
                            </div>
                            <div class="review-left-over">
                                <img src="images/review-img.png" alt="" />
                                <div class="review-left-text">
                                    <h3>Akolade Oluwatoba</h3>
                                    <p>Founder, The Book Surgeons International</p>
                                </div>
                            </div>
                            <div class="review-left-before">
                                <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="review-right">
                            <h1>Testimonials</h1>
                            <i class="fa fa-quote-left" aria-hidden="true"></i><p>
                                My business was just at the verge of wrapping up. I was confused especially in the issue of Structure in business. When I met Dr. Yemisi, I literally cried for not meeting her before that time. She breathed into my business and I am proud at what we are doing at The Book Surgeons International.
                                Via her mentoring and coaching, we have been able to secure clients locally and internationally and we have made a lot of them Published Authors.
                            </p><i class="fa fa-quote-right" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- Review area end -->
<!-- Testimonial area start -->
<section class="testimonial">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="testimonial-top">
                    <h2 class="head-two">FAQ's</h2>
                    <div class="top-head-before">
                        <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="testimonial-left wow fadeInUp">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Who is Yemisi Adeyeye?</a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse in">
                                <div class="panel-body" style="text-align: justify">
                                    Dr Yemisi Adeyeye is the cofounder and MD of Lifefount Hospital, a relatable business teacher and a certified management
                                    consultant. She is very passionate as promoting entrepreneurship, health, and education.
                                <br><br>
                                    Dr Yemisi is also the founder of Lifefount business Network which facilitates business courses, summits and
                                    bootcamps designed to empower youths and female entrepreneurs through mentorship, training and networking both online and offline classes.
                                <br><br>
                                    She is an international mentor and trainer who has won several awards and recognitions from leading global organisations.
                                <br><br>
                                    She is married to her darling husband Surgeon Ademola Adeyeye and blessed with two adorable children Adesewa and Adejola-Oluwa.
                                    <br><br>

                                    Follow her:<br>
                                    Instagram:@yemisiadeyeye<br>
                                    Facebook: Lifefount Business Network<br>
                                </div>
                            </div>
                        </div>
{{--                        <div class="panel panel-default">--}}
{{--                            <div class="panel-heading">--}}
{{--                                <h4 class="panel-title">--}}
{{--                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Who is behind Freelancer?</a>--}}
{{--                                </h4>--}}
{{--                            </div>--}}
{{--                            <div id="collapse2" class="panel-collapse collapse">--}}
{{--                                <div class="panel-body">Stay organized by adding agencies to lists, review agencies you're working with and keep an eye out for new project features or on mentions of agencies you list.</div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="panel panel-default">--}}
{{--                            <div class="panel-heading">--}}
{{--                                <h4 class="panel-title">--}}
{{--                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Beyond searching for an agency, what else can I do?</a>--}}
{{--                                </h4>--}}
{{--                            </div>--}}
{{--                            <div id="collapse3" class="panel-collapse collapse">--}}
{{--                                <div class="panel-body">Stay organized by adding agencies to lists, review agencies you're working with and keep an eye out for new project features or on mentions of agencies you list.</div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="panel panel-default">--}}
{{--                            <div class="panel-heading">--}}
{{--                                <h4 class="panel-title">--}}
{{--                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">How do I get my agency a profile on Freelancer?</a>--}}
{{--                                </h4>--}}
{{--                            </div>--}}
{{--                            <div id="collapse4" class="panel-collapse collapse">--}}
{{--                                <div class="panel-body">Stay organized by adding agencies to lists, review agencies you're working with and keep an eye out for new project features or on mentions of agencies you list.</div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="panel panel-default">--}}
{{--                            <div class="panel-heading">--}}
{{--                                <h4 class="panel-title">--}}
{{--                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">What benefit could Freelancer bring to my agency?</a>--}}
{{--                                </h4>--}}
{{--                            </div>--}}
{{--                            <div id="collapse5" class="panel-collapse collapse">--}}
{{--                                <div class="panel-body">Stay organized by adding agencies to lists, review agencies you're working with and keep an eye out for new project features or on mentions of agencies you list.</div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
{{--                    <div class="testimonial-left-para text-center">--}}
{{--                        <p>Can't find what you're looking for? No problem.</p>--}}
{{--                        <a href="#contact">Contact Me</a>--}}
{{--                    </div>--}}
                </div>
            </div>
            <div class="col-md-6">
                <div class="testimonial-right wow fadeInUp">
                    <img src="images/faq.jpg" alt="" />
                    <div class="testimonial-right-txt clearfix">
                        <h4 class="pull-left">Professionalism</h4>
                        <h4 class="pull-right">64%</h4>
                        <div id="progress1"></div>
                    </div>
                    <div class="testimonial-right-txt clearfix">
                        <h4 class="pull-left">Fastness</h4>
                        <h4 class="pull-right">74%</h4>
                        <div id="progress2"></div>
                    </div>
                    <div class="testimonial-right-txt clearfix">
                        <h4 class="pull-left">Creativness</h4>
                        <h4 class="pull-right">49%</h4>
                        <div id="progress3"></div>
                    </div>
                    <div class="testimonial-right-txt clearfix">
                        <h4 class="pull-left">We Care</h4>
                        <h4 class="pull-right">89%</h4>
                        <div id="progress4"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- Testimonial area end -->
<!-- Blog area start -->
<section class="blog" id="blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="blog-top">
                    <h2 class="head-two">Blog News</h2>
                    <div class="top-head-before">
                        <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="blog-content wow zoomIn">
                    <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                        <!-- Bottom Carousel Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#quote-carousel" data-slide-to="0" class="active"><img class="img-responsive " src="images/bolg-one.png" alt="">
                            </li>
                            <li data-target="#quote-carousel" data-slide-to="1"><img class="img-responsive" src="images/bolg-two.png" alt="">
                            </li>
                            <li data-target="#quote-carousel" data-slide-to="2"><img class="img-responsive" src="images/bolg-one.png" alt="">
                            </li>
                        </ol>
                        <!-- Carousel Slides / Quotes -->
                        <div class="carousel-inner text-center">
                            <!-- Quote 1 -->
                            <div class="item active">
                                <div class="row">
                                    <div class="col-md-5 col-sm-9">
                                        <div class="blog-left">
                                            <img src="images/bolg-feature.jpg" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-md-1 col-sm-3">
                                        <div class="blog-comment">
                                            <h1>4</h1>
                                            <h4>Feb-17</h4>
                                            <h1>21</h1>
                                            <h4>Comm</h4>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="blog-right">
                                            <h2>How to Have a Creative Desk in 2017</h2>
                                            <i class="fa fa-folder-open" aria-hidden="true"></i><span>business, creative, creative</span>
                                            <p>When your livelihood depends on your ability to have churn out creative designs everyday, it's imperative that you stay productive. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-5 col-sm-9">
                                        <div class="blog-left">
                                            <img src="images/bolg-feature.jpg" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-md-1 col-sm-3">
                                        <div class="blog-comment">
                                            <h1>4</h1>
                                            <h4>Feb-17</h4>
                                            <h1>21</h1>
                                            <h4>Comm</h4>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="blog-right">
                                            <h2>How to Have a Creative Desk in 2017</h2>
                                            <i class="fa fa-folder-open" aria-hidden="true"></i><span>business, creative, creative</span>
                                            <p>When your livelihood depends on your ability to have churn out creative designs everyday, it's imperative that you stay productive. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-5 col-sm-9">
                                        <div class="blog-left">
                                            <img src="images/bolg-feature.jpg" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-md-1 col-sm-3">
                                        <div class="blog-comment">
                                            <h1>4</h1>
                                            <h4>Feb-17</h4>
                                            <h1>21</h1>
                                            <h4>Comm</h4>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="blog-right">
                                            <h2>How to Have a Creative Desk in 2017</h2>
                                            <i class="fa fa-folder-open" aria-hidden="true"></i><span>business, creative, creative</span>
                                            <p>When your livelihood depends on your ability to have churn out creative designs everyday, it's imperative that you stay productive. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Carousel Buttons Next/Prev -->
                        <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-angle-left"></i></a>
                        <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- Blog area end -->
<!-- Contact area start -->
{{--<section class="contact" id="contact">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-12 text-center">--}}
{{--                <div class="contact-top">--}}
{{--                    <h2 class="head-two">Contact</h2>--}}
{{--                    <div class="top-head-before">--}}
{{--                        <i class="fa fa-dot-circle-o" aria-hidden="true"></i>--}}
{{--                    </div>--}}
{{--                    <p>Please call or complete the form below and I will be in touch with you shortly.</p>--}}
{{--                    <a href="#"><i class="fa fa-phone" aria-hidden="true"></i><span>Call me:</span> (012) 345-6789</a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-12">--}}
{{--                <div class="contact-form wow fadeInUp">--}}
{{--                    <form class="form-inline" id="contact-form">--}}
{{--                        <!-- IF MAIL SENT SUCCESSFULLY -->--}}
{{--                        <div class="success">--}}
{{--                            Your message has been sent successfully.--}}
{{--                        </div>--}}
{{--                        <!-- IF MAIL SENDING UNSUCCESSFULL -->--}}
{{--                        <div class="error">--}}
{{--                            E-mail must be valid and message must be longer than 1 character.--}}
{{--                        </div>--}}
{{--                        <!-- Form Fields -->--}}
{{--                        <div class="col-md-4">--}}
{{--                            <div class="form-group">--}}
{{--                                <label class="sr-only" for="first_name">First Name</label>--}}
{{--                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-4">--}}
{{--                            <div class="form-group">--}}
{{--                                <label class="sr-only" for="last_name">Last Name</label>--}}
{{--                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-4">--}}
{{--                            <div class="form-group">--}}
{{--                                <label class="sr-only" for="phone_number">Phone</label>--}}
{{--                                <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Phone">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-4">--}}
{{--                            <div class="form-group">--}}
{{--                                <label class="sr-only" for="email_address">Email</label>--}}
{{--                                <input type="email" class="form-control" id="email_address" name="email_address" placeholder="Email Address">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-8">--}}
{{--                            <div class="form-group">--}}
{{--                                <select id="contact_reason" name="contact_reason">--}}
{{--                                    <option value="Reason of Contact">Reason of Contact</option>--}}
{{--                                    <option value="Reason of Contact">Reason of Contact</option>--}}
{{--                                    <option value="Reason of Contact">Reason of Contact</option>--}}
{{--                                    <option value="Reason of Contact">Reason of Contact</option>--}}
{{--                                </select>--}}
{{--                                <i class="fa fa-angle-down" aria-hidden="true"></i>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-12">--}}
{{--                            <div class="form-group-textarea">--}}
{{--                                <textarea class="form-control" id="message" name="message" rows="4" placeholder="Message"></textarea>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-12 text-center">--}}
{{--                            <button type="submit" name="submit" id="submit">Contact Now</button>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section><!-- Contact area end -->--}}
<!-- Footer area start -->



<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer-content">
                    <p class="pull-left">&copy; <?php echo date('Y') ?> Yemisi Adeyeye <i class="fa fa-heart" aria-hidden="true"></i> by <a href="#">GoodNews Infotechnologies</a></p>
                    <div class="footer-icon pull-right">
                        <a href="https://web.facebook.com/yemisi.adeyeye.5" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="https://twitter.com/drroadeyeye/" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="https://ng.linkedin.com/in/adeyeye-yemisi-0b916055" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer><!-- Footer area end -->
<!-- scrolltotop start -->
<div>
    <a href="#" class="scrollToTop text-center" >
        <i class="scroll-fa fa fa-angle-up" aria-hidden="true"></i>
    </a>
</div><!-- scrolltotop end -->
<!-- jquery main JS -->
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<!-- Bootstrap JS -->
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- Owl carousel JS -->
<script type="text/javascript" src="{{asset('js/owl.carousel.min.js')}}"></script>
<!-- Counterup waypoints JS -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<!-- Counterup JS -->
<script type="text/javascript" src="{{asset('js/jquery.counterup.min.js')}}"></script>
<!-- Progressbar JS -->
<script type="text/javascript" src="{{asset('js/progressbar.min.js')}}"></script>
<!-- Parallax JS -->
<script type="text/javascript" src="{{asset('js/parallax.min.js')}}"></script>
<!-- Lightbox JS -->
<script type="text/javascript" src="{{asset('js/lightbox.min.js')}}"></script>
<!-- WOW JS -->
<script type="text/javascript" src="{{asset('js/wow-1.3.0.min.js')}}"></script>
<!-- main JS -->
<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
</body>
</html>
