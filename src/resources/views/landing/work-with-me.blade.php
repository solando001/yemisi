<!doctype html>

<html class="no-js" lang="zxx">

<head>

    <!-- metas -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Yemisi Adeyeye">
    <meta name="keywords" content="portfolio, personal, corporate, business, parallax, creative, agency">

    <!-- title -->
    <title>Work With Me | Yemisi Adeyeye</title>

    <!-- favicon -->
    <link href="img/favicon.ico" rel="icon" type="image/png">

    <!-- bootstrap css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet">

    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- magnific-popup CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">

    <!-- animate.min CSS -->
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- Font Icon Core CSS -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/et-line.css">

    <!-- Core Style Css -->
    <link rel="stylesheet" href="css/style.css">

    <!--[if lt IE 9]-->
    <script src="js/html5shiv.min.js"></script>
    <!--[endif]-->

    <style>
        .circle-icon {
            background: purple;
            color: white;
            padding:8px;
            border-radius: 50%;
        }
    </style>

</head>

<body>

<!-- ====== Preloader ======  -->
<div class="loading">
    <div class="load-circle">
    </div>
</div>
<!-- ======End Preloader ======  -->
@include('landing.nav')


<!-- ====== Services ======  -->
<section class="services section-padding bg-gray text-center pb-70" data-scroll-index="6">
    <div class="container">

        <!-- section heading -->
        <div class="section-head">
            <h3>Work With Me.</h3>
        </div>

        <div class="row">

            <!-- items -->
            <div class="col-md-4">
                <a href="{{url('/consult-dr-yemisi')}}" >
                    <div class="item">
                        <span class="icon"><i class="fa fa-laptop" aria-hidden="true"></i></span>
                        <h6>Consult Dr. Yemisi</h6>
                        <p>I will hold your hands and support your golden dreams as you navigate through the bends and curves in your business journey.</p>
                        <br>
                        <a href="{{url('/consult-dr-yemisi')}}" class="buton boton">Consult Me</a>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="{{url('/resources')}}">
                <div class="item">
                    <span class="icon"><i class="fa fa-bullhorn" aria-hidden="true"></i></span>
                    <h6>Resources</h6>
                    <p>You can learn from my experience and expertise in business, health and wellness. I will deplore my wealth of knowledge passionately and excellently.
                    </p>
                    <br>
                    <a href="{{url('/resources')}}" class="buton boton">Explore My Archive</a>
                </div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="{{url('/events')}}">
                <div class="item">
                    <span class="icon"><i class="fa fa-umbrella" aria-hidden="true"></i></span>
                    <h6>Events</h6>
                    <p>Catch up with  my Current, past and future events here</p>
                    <br>
                    <a href="{{url('/events')}}" class="buton boton">View All Events</a>
                </div>
                </a>
            </div>

        </div><!-- /row -->
    </div><!-- /container -->
</section>
<!-- ====== End Services ======  -->

<!--====== Contact ======-->
<section class="contact section-padding" data-scroll-index="6">
    <div class="container">
        <div class="row">

            <!-- section heading -->
            <div class="section-head">
                <h3>Contact Me.</h3>
            </div>

            <div class="col-md-offset-1 col-md-10">

                <!-- contact info -->
                <div class="info text-center mb-50">

                    <div class="col-md-4">
                        <div class="item">
                            <span class="icon"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>
                            <h6>Address</h6>
                            <p>3, Plantation Road, off Police Road, GRA,  Ilorin, Kwara State</p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="item">
                            <span class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                            <h6>Email</h6>
                            <p>adeyeye.yemisi@yahoo.com</p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="item">
                            <span class="icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                            <h6>Phone</h6>
                            <p>+2349094407502</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <!-- contact form -->
{{--                <form class="form" id="contact-form" method="post" action="contact.php">--}}
{{--                    <div class="messages"></div>--}}

{{--                    <div class="controls">--}}

{{--                        <div class="row">--}}
{{--                            <div class="col-md-6">--}}
{{--                                <div class="form-group">--}}
{{--                                    <input id="form_name" type="text" name="name" placeholder="Name" required="required">--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <div class="form-group">--}}
{{--                                    <input id="form_email" type="email" name="email" placeholder="Email" required="required">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-md-12">--}}
{{--                                <div class="form-group">--}}
{{--                                    <textarea id="form_message" name="message" placeholder="Message" rows="4" required="required"></textarea>--}}
{{--                                </div>--}}

{{--                                <input type="submit" value="Submit" class="buton buton-bg">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </form>--}}

            </div>
        </div><!-- /row -->
    </div><!-- /container -->
</section>
<!--====== End Contact ======-->

@include('landing.footer');
