<!doctype html>
<html class="no-js" lang="zxx">
<head>

    <!-- metas -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Daniels is a responsive creative template">
    <meta name="keywords" content="portfolio, personal, corporate, business, parallax, creative, agency">

    <!-- title -->
    <title>Resources | Yemisi Adeyeye</title>

    <!-- favicon -->
    <link href="img/favicon.ico" rel="icon" type="image/png">

    <!-- bootstrap css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet">

    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- magnific-popup CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">

    <!-- animate.min CSS -->
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- Font Icon Core CSS -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/et-line.css">

    <!-- Core Style Css -->
    <link rel="stylesheet" href="css/style.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
    <link rel="stylesheet" href="fluid-gallery.css">

    <!--[if lt IE 9]-->
    <script src="js/html5shiv.min.js"></script>
    <!--[endif]-->

    <style>
        .circle-icon {
            background: purple;
            color: white;
            padding:8px;
            border-radius: 50%;
        }
    </style>
</head>

<body>

<!-- ====== Preloader ======  -->
<div class="loading">
    <div class="load-circle">
    </div>
</div>
<!-- ======End Preloader ======  -->
@include('landing.nav')

<!-- ====== Header ======  -->
<section id="home" class="min-header" data-scroll-index="0">

    <div class="v-middle mt-30">
        <div class="container">
            <div class="row">

                <div class="text-center">
                    <h5>My Resources</h5>
{{--                    <a href="#0">Home</a>--}}
{{--                    <a href="#0">Blog</a>--}}
                </div>

            </div>
        </div><!-- /row -->
    </div><!-- /container -->
</section>
<!-- ====== End Header ======  -->

<!--====== Blog ======-->
<section class="blogs section-padding">
    <div class="container">
        <div class="row">

{{--            <div class="tz-gallery">--}}

{{--                <div class="row">--}}
{{--                    @foreach($books as $book)--}}
{{--                            <div class="col-sm-6 col-md-4">--}}
{{--                                <a class="lightbox" href="{{$book->image}}">--}}
{{--                                    <img src="{{$book->image}}" alt="Park">--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                    @endforeach--}}
{{--                </div>--}}

{{--            </div>--}}



            <div class="col-md-12">
                <div class="posts">
                    @foreach($books as $book)
                <div class="col-md-6">
                    <div class="post">
                        <div class="post-img">
                            <a href="#0">
                                <img src="{{$book->image}}" alt="">
                            </a>
                        </div>
                        <div class="content text-center">
                            <div class="post-meta">
                                <div class="post-title">
                                    <h5>
                                        <a href="#0">{{$book->name}}</a>
                                    </h5>
                                </div>
                                <ul class="meta">
                                    <li>
                                        <h4>
                                            <a href="#0">
                                                <i class="fa fa-money" aria-hidden="true"></i>
                                                {{number_format($book->price)}}
                                            </a>
                                        </h4>

                                    </li>
                                </ul>
                            </div>

                            <div class="post-cont">
                                <p> {{$book->description}} </p>
                            </div>

                            <a href="#0" class="butn" data-toggle="modal" data-target="#exampleModal_{{$book->id}}">Read More</a>

                        </div>
                    </div>
                </div>

                        @if(session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        <div class="modal fade" data-keyboard="false" data-backdrop="static" id="exampleModal_{{$book->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Enter Your Information</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">


                                        <form method="POST" action="{{ url('customer') }}" enctype="multipart/form-data" class="form-inlineoo">
                                            @csrf

                                            <div class="contact-form">



                                                                                        <!-- IF MAIL SENT SUCCESSFULLY -->
                                                                                        <div class="success">
                                                                                            Your message has been sent successfully.
                                                                                        </div>
                                                                                        <!-- IF MAIL SENDING UNSUCCESSFULL -->
                                                                                        <div class="error">
                                                                                            E-mail must be valid and message must be longer than 1 character.
                                                                                        </div>
                                            <!-- Form Fields -->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="sr-only" for="first_name">Full Name</label>
                                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="Full Name" autofocus>
                                                                                                                @error('name')
                                                                                                                <span class="invalid-feedback" role="alert">
                                                                                                                    <strong>{{ $message }}</strong>
                                                                                                                </span>
                                                                                                                @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="sr-only" for="last_name">Email</label>
                                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email Address" >
                                                                                                            @error('email')
                                                                                                            <span class="invalid-feedback" role="alert">
                                                                                                                <strong>{{ $message }}</strong>
                                                                                                            </span>
                                                                                                            @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="sr-only" for="phone_number">Phone</label>
                                                        <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" placeholder="Phone Number" >
                                                                                                            @error('phone')
                                                                                                            <span class="invalid-feedback" role="alert">
                                                                                                                <strong>{{ $message }}</strong>
                                                                                                            </span>
                                                                                                            @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="sr-only" for="email_address">Email</label>
                                                        <select id="gender" name="gender" class="form-control @error('gender') is-invalid @enderror" >
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                        </select>
                                                                                                            @error('gender')
                                                                                                            <span class="invalid-feedback" role="alert">
                                                                                                                <strong>{{ $message }}</strong>
                                                                                                            </span>
                                                                                                            @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">

                                                        <input id="location" type="text" class="form-control @error('location') is-invalid @enderror" name="location" value="{{ old('location') }}" placeholder="Location" >
                                                                                                            @error('location')
                                                                                                            <span class="invalid-feedback" role="alert">
                                                                                                                <strong>{{ $message }}</strong>
                                                                                                            </span>
                                                                                                            @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">

                                                        <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" placeholder="Address" >
                                                                                                            @error('address')
                                                                                                            <span class="invalid-feedback" role="alert">
                                                                                                                <strong>{{ $message }}</strong>
                                                                                                            </span>
                                                                                                            @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">

                                                        <input id="coupon" type="text" class="form-control @error('coupon') is-invalid @enderror" name="coupon" value="{{ old('coupon') }}" placeholder="Coupon Code" >
                                                                                                            @error('address')
                                                                                                            <span class="invalid-feedback" role="alert">
                                                                                                                <strong>{{ $message }}</strong>
                                                                                                            </span>
                                                                                                            @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">

                                                        <select id="quantity" name="quantity" class="form-control @error('gender') is-invalid @enderror">

                                                            <?php
                                                            for($i=1; $i<101; $i++){
                                                            ?>
                                                            <option value="{{$i}}">{{$i}}</option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <?php $ref = Ramsey\Uuid\Uuid::uuid4();?>
                                                <input type="hidden" name="book_ref" value="{{$book->ref}}">
                                                <input type="hidden" name="ref" value="{{$ref}}">

                                                <button type="submit" class="btn btn-primary">Continue</button>


{{--                                                                                            <div class="col-md-12 text-center">--}}
{{--                                                                                                <button type="submit" name="submit" id="submit">Contact Now</button>--}}
{{--                                                                                            </div>--}}

                                            </div>
                                        </form>


                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
{{--                                        <button type="button" class="btn btn-primary">Save changes</button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endforeach
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <center>
                        <div class="item">
                            <div class="hero-img mb-30">
                                {{--                        <img src="img/abt1.jpg" alt="" style="border-radius: 50%">--}}
                                <img src="img/hrpolicy.jpeg" alt="" width="200" height="200">
                            </div>

                            {{--                            <span class="icon"><i class="fa fa-book fa-4x" aria-hidden="true"></i></span>--}}
                            <br><br>
                            <h2> HR Template </h2>
                            <br><br>
                            <a href="{{url('/hrtemplate')}}" class="buton boton"> Get HR Template  </a>
                        </div>
                    </center>
                </div>
            </div>
            <br><br>

        </div>
    </div>
</section>
<!--====== End Blog ======-->

<!--====== Footer ======-->
<footer>
    <div class="container text-center">
        <div class="row">
            <p>Copy Right <?php echo date('Y') ?> &copy; Yemisi Adeyeye, All Rights Reserved</p>
        </div>
    </div>
</footer>
<!--====== End Footer ======-->



<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.tz-gallery');
</script>

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>

<!-- bootstrap -->
<script src="js/bootstrap.min.js"></script>

<!-- scrollIt -->
<script src="js/scrollIt.min.js"></script>

<!-- magnific-popup -->
<script src="js/jquery.magnific-popup.min.js"></script>

<!-- owl carousel -->
<script src="js/owl.carousel.min.js"></script>

<!-- stellar js -->
<script src="js/jquery.stellar.min.js"></script>

<!-- animated.headline -->
<script src="js/animated.headline.js"></script>

<!-- jquery.waypoints.min js -->
<script src="js/jquery.waypoints.min.js"></script>

<!-- jquery.counterup.min js -->
<script src="js/jquery.counterup.min.js"></script>

<!-- isotope.pkgd.min js -->
<script src="js/isotope.pkgd.min.js"></script>

<!-- validator js -->
<script src="js/validator.js"></script>

<!-- custom script -->
<script src="js/custom.js"></script>

</body>
</html>
