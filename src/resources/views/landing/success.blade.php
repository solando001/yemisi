<!doctype html>

<html class="no-js" lang="zxx">

<head>

    <!-- metas -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Yemisi Adeyeye">
    <meta name="keywords" content="portfolio, personal, corporate, business, parallax, creative, agency">

    <!-- title -->
    <title>Successful Payment | Yemisi Adeyeye</title>

    <!-- favicon -->
    <link href="img/favicon.ico" rel="icon" type="image/png">

    <!-- bootstrap css -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet">

    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">

    <!-- magnific-popup CSS -->
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">

    <!-- animate.min CSS -->
    <link rel="stylesheet" href="{{asset('css/animate.min.css')}}">

    <!-- Font Icon Core CSS -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/et-line.css')}}">

    <!-- Core Style Css -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <!--[if lt IE 9]-->
    <script src="{{asset('js/html5shiv.min.js')}}"></script>
    <!--[endif]-->

    <style>
        .circle-icon {
            background: purple;
            color: white;
            padding:8px;
            border-radius: 50%;
        }
    </style>

</head>

<body>

<!-- ====== Preloader ======  -->
<div class="loading">
    <div class="load-circle">
    </div>
</div>
<!-- ======End Preloader ======  -->
@include('landing.nav')

<!-- ====== Services ======  -->
<section class="services section-padding bg-gray text-center pb-70" data-scroll-index="6">
    <div class="container">

        <!-- section heading -->
        <div class="section-head">
            <h3>Payment Successful.</h3>
        </div>

        <div class="row">
            <!-- items -->
            <div class="col-md-12">
                <div class="item">
                    <span class="icon"><i class="fa fa-thumbs-up" aria-hidden="true"></i></span>
                    <h6>Payment was successful</h6>
                    <p>Your Payment is successful, please click the button below to continue</p>
                    <br>
                    <a href="https://chat.whatsapp.com/KZ5VPmW5yUgJhCwWZBmDD4" target="_blank" class="btn btn-success">Click here to Join My Group</a>
                </div>
            </div>
        </div><!-- /row -->
    </div><!-- /container -->
</section>
<!-- ====== End Services ======  -->


@include('landing.footer');
