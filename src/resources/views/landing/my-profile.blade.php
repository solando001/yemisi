<!doctype html>

<html class="no-js" lang="zxx">

<head>

    <!-- metas -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Yemisi Adeyeye">
    <meta name="keywords" content="portfolio, personal, corporate, business, parallax, creative, agency">

    <!-- title -->
    <title>My Profile | Yemisi Adeyeye</title>

    <!-- favicon -->
    <link href="img/favicon.ico" rel="icon" type="image/png">

    <!-- bootstrap css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet">

    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- magnific-popup CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">

    <!-- animate.min CSS -->
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- Font Icon Core CSS -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/et-line.css">

    <!-- Core Style Css -->
    <link rel="stylesheet" href="css/style.css">

    <!--[if lt IE 9]-->
    <script src="js/html5shiv.min.js"></script>
    <!--[endif]-->
    <style>
        .circle-icon {
            background: purple;
            color: white;
            padding:8px;
            border-radius: 50%;
        }
    </style>
</head>

<body>

<!-- ====== Preloader ======  -->
<div class="loading">
    <div class="load-circle">
    </div>
</div>
<!-- ======End Preloader ======  -->
@include('landing.nav')

<!--====== Portfolio ======-->
{{--<section class="portfolio section-padding pb-70" data-scroll-index="3">--}}
<section class="portfolio bg-gray section-padding pb-70" data-scroll-index="3">
    <div class="container">
        <div class="row">
            <!-- section heading -->
            <div class="section-head">
                <h3>My Profile.</h3>
            </div>

            <div class="row">

                <div class="col-md-7">
                    <p class="content mb-30">
                        Dr Rebecca  Oluwayemisi Adeyeye <a href="https://www.yemisiadeyeye.com">( www.yemisiadeyeye.com)</a>  is the cofounder and managing director of Lifefount Hospital a leading hospital in cancer care,Surgery and endoscopy. She is a fellow of institute of management consultants and 
                        the pioneer president  of Academy for women entrepreneurs (AWE) Alumnae which is a part of the white House led women's global development and prosperity initiative (W-GDP). She is recognised in 2021 as a global leader by the U.S  department of states under the International Visitors Leadership Program (IVLP).She is an executive of NECA's Network of Entrepreneurial Women ( NNEW). She recently authored her maiden book 'Entrepreneurship  is a beautiful thing' which is a business guide for aspiring and seasoned business owners who seeks to acquire business management skills.
                       <br> Dr Yemisi Adeyeye is an international mentor.and trainer who has won several awards and scholarships including the Kwame Nkurumah Exemplary Leadership Award 2021 by ECOWAS Youth Council , Entrepreneur of the year 2020 by kwaralive, Amazon Award by LeapAfrica and World bank women X scholarship award. She was hosted twice as a speaker at the Dubai Leadership Summit Dubai UAE. Her audiences referred to her style of presentation as relatable, memorable andactionable. Her speaking topics includes but not limited to entrepreneurship, health, women's economic advancement, relationship management and  faith based topics. 
                       <br>She is a brand ambassador for Tony Elumelu Foundation and Her Venture app of Cherie Blair foundation and ExxonMobil. She sits on the advisory board of AIESEC ILC.
                       <br> She has postgraduate certification in Entrepreneurial Management from EDC (Lagos Business School) of Pan-Atlantic University , Leadership and management in Global health from university of Washington USA and a Postgraduate diploma in Education from NTI/Open University. 
                    </p>
                </div>
                <div class="col-md-5">
                    <div class="hero-img mb-30">
{{--                        <img src="img/abt1.jpg" alt="" style="border-radius: 50%">--}}
                        <img src="img/home4.jpg" alt="" style="border-radius: 50%">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-5">
                    <div class="hero-img mb-30">
                        <img src="img/abt3.jpg" alt="" style="border-radius: 50%">
                    </div>
                </div>
                <div class="col-md-7">
                    <p class="content mb-30">
{{--                        She is the cofounder of Lifefount foundation that focuses on Health, Education and Empowerment. The Lifefount foundation has many Health and Education projects which includes--}}
{{--                        <br>--}}

{{--                        1. Community Development: Motorization of borehole water in a community in Ilorin West LGA.<br>--}}
{{--                        2. Medical outreaches to communities.<br>--}}
{{--                        3. Cancer awareness campaign in partnership with National orientation Agency.<br>--}}
{{--                        4. Health education to secondary schools in partnership with ministry of education on genotype , prevention of sexual and drug--}}
{{--                            abuse and emotional intelligence the hope of eradicating sickle cell  in the near future and ensuring youths make responsible--}}
{{--                            decisions as they grow up.</br>--}}
{{--                        She convened the positive impact Conference Which focused on drug and substance abuse prevention and rehabilitation support to the addicts.--}}
{{--<br>--}}
{{--                        Dr Yemisi Adeyeye is the coordinator of Lifefount leadership and business club  an online platform giving business information and support, empowering and mentoring members who are now beneficiaries of different awards, grants and scholarships  as they evolve in their entrepreneurial journeys with two of our members making top 100 SME in Nigeria by Businessday magazine.--}}

{{--                        She and her team mobilized more than 80 entrepreneurs in 2017 and about 100 in 2018 for the first and second  Global Entrepreneurship Network walk in Kwara The walk gave Indigenous businesses  brand visibility and network opportunities.--}}
{{--<br>--}}
{{--                        She hosted CEO Connect a gathering of Business owners which encouraged boarderless business, collaboration and exposure to opportunities with many success stories from these interactions.--}}

{{--                        In order to solve SME's biggest problem which is funding, she created the Lifefount Multipurpose cooperative society (LMCS) giving the young entrepreneurs the platform to save cooperatively and crowdfund members.--}}
{{--<br>--}}
{{--                        Dr Yemisi Adeyeye is a seasoned public speaker who was featured in many events organized nationally and internationally by government and non-governmental organizations.--}}

{{--                        She is married to her darling , Surgeon Ademola Adeyeye and she is blessed with two adorable children Adesewa and Adejola-Oluwa. Her hobbies includes visiting new and inspiring places and spending time with loved ones.--}}
{{--<br>--}}
{{--                        Her purpose in life is to use her expertise and experience in healthcare, entrepreneurship and education to better the lives of her contacts in order to achieve a better world.--}}
{{--<br>--}}


                        <br>Dr Yemisi Adeyeye is very passionate as promoting entrepreneurship and healthcare ,she cofounded Lifefount foundation which has many social impact projects.  Lifefount business Network project facilitates business trainings, Seminars and conferences which empowers youths and female entrepreneurs through mentorship, training and networking.
                        <br>Lifefount Cooperative society  project is a peer to peer funding of businesses from financial contributions of members. 
                        Lifefount school social  impact project trained 35  trainers on emotional intelligence ,genotype education and prevention of sexual and drugs addiction to three thousand and eighty-six students ( 3,086) in 13  secondary schools in different communities in kwara state. Lifefount cancer awareness and screening projects has given free breast clinical examination, mammography and ultrasound  to one thousand and five hundred (1500) women in two states in Nigeria in the past three years.
                        Dr Yemisi leveraged on technology to train one thousand,  one hundred and fifty-four(1,154) youths and female business owners on how to launch and structure their businesses profitably and sustainably during the pandemic.Lifefount Hospital under her leadership also leveraged on technology and provided training to about one thousand medical personnels on COVID-19 and cancer  care with facilitators from UK,Pakistan ,Spain and Nigeria. The use of  Online platforms for clinical consultations also helped her business to navigate the COVID-19 pandemic as Lifefount Hospital enjoyed patronage across the globe.
                        <br>She trained 225 entrepreneur in first quarter of 2021 on how to use graphics design to enhance their business growth. 
                        Her life goal is to  use her experience, expertise and education  to make her contacts better. 
                        <br>She is married to her darling husband Surgeon Ademola Adeyeye and blessed with two adorable children Adesewa and Adejola-Oluwa.
                        Her hobbies include spending time with loved ones and visiting inspiring places.
                        <br>

                        <b><i>Dr Yemisi Adeyeye</i></b>
<br>
                        <b><i>MBBS,PGDE,FIMC, CMC, Cert Entrepreneurial Management.</i></b>
                        <br>
                    </p>
                </div>
            </div>
        </div><!-- /row -->
    </div><!-- /container -->
</section>
<!--====== End Portfolio ======-->


<!--====== Numbers ======-->
<div class="numbers section-padding text-center pb-70">
    <div class="container">
        <div class="row">

            <!-- items -->
            <div class="col-md-4">
                <div class="item">
                    <span class="icon"><i class="fa fa-users" aria-hidden="true"></i></span>
                    <h3 class="counter">95</h3>
                    <p>Guest Speaking Engagement</p>
                </div>
            </div>

            <div class="col-md-4">
                <div class="item">
                    <span class="icon"><i class="fa fa-thumbs-up" aria-hidden="true"></i></span>
                    <h3 class="counter">250</h3>
                    <p>Business Consultations</p>
                </div>
            </div>

            <div class="col-md-4">
                <div class="item">
                    <span class="icon"><i class="fa fa-bullhorn" aria-hidden="true"></i></span>
                    <h3 class="counter">45</h3>
                    <p>International Award</p>
                </div>
            </div>

{{--            <div class="col-md-3">--}}
{{--                <div class="item">--}}
{{--                    <span class="icon"><i class="fa fa-cloud-download" aria-hidden="true"></i></span>--}}
{{--                    <h3 class="counter">780</h3>--}}
{{--                    <p>Files Downloaded</p>--}}
{{--                </div>--}}
{{--            </div>--}}

        </div><!-- /row -->
    </div><!-- /container -->
</div>
<!--====== End Numbers ======-->

@include('landing.footer');
