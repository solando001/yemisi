<!doctype html>

<html class="no-js" lang="zxx">

<head>

    <!-- metas -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Yemisi Adeyeye">
    <meta name="keywords" content="portfolio, personal, corporate, business, parallax, creative, agency">

    <!-- title -->
    <title>Make Payment | Yemisi Adeyeye</title>

    <!-- favicon -->
    <link href="img/favicon.ico" rel="icon" type="image/png">

    <!-- bootstrap css -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet">

    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">

    <!-- magnific-popup CSS -->
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">

    <!-- animate.min CSS -->
    <link rel="stylesheet" href="{{asset('css/animate.min.css')}}">

    <!-- Font Icon Core CSS -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/et-line.css')}}">

    <!-- Core Style Css -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <!--[if lt IE 9]-->
    <script src="{{asset('js/html5shiv.min.js')}}"></script>
    <!--[endif]-->

    <style>
        .circle-icon {
            background: purple;
            color: white;
            padding:8px;
            border-radius: 50%;
        }
    </style>

</head>

<body>

<!-- ====== Preloader ======  -->
<div class="loading">
    <div class="load-circle">
    </div>
</div>
<!-- ======End Preloader ======  -->
@include('landing.nav')

<section class="services section-padding bg-gray text-center pb-70" data-scroll-index="6">
    <div class="container">

        <!-- section heading -->
        <div class="section-head">

            @if($user->type == 'one')
                <h3> One-On-On Consultation  </h3>
                @elseif($user->type == 'group')
                <h3> Group Consultation  </h3>
                @elseif($user->type == 'public')
                <h3> Public Consultation  </h3>
                @endif
            <p>Payment Details</p>
        </div>

        <div class="row">

            <div class="col-md-12">

                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Fields</th>
                        <th scope="col">Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>Full Name</th>
                        <td style="text-align: justify">{{$user->name}}</td>
                    </tr>
                    <tr>
                        <th>Phone Number</th>
                        <td style="text-align: justify">{{$user->phone}}</td>
                    </tr>
                    <tr>
                        <th>Email Address</th>
                        <td style="text-align: justify">{{$user->email}}</td>
                    </tr>
                    <tr>
                        <th>About</th>
                        <td style="text-align: justify">{{$user->about}}</td>
                    </tr>

                    <?php
                    //echo $type;
                        if($type->location == 'int')
                            {
                                $amount = $type->amount * 480;
                            }else{
                                $amount = $type->amount;
                            }

                    ?>
                    <tr>
                        <th>Amount</th>
                        <td style="text-align: justify">{{number_format($amount)}}</td>
                    </tr>

                    <tr>
                        <th></th>
                        <td style="text-align: justify">
                            <form method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
                                <div class="row" style="margin-bottom:40px;">
                                    <div class="col-md-8 col-md-offset-2">
                                        <input type="hidden" name="email" value="{{$user->email}}">
                                        <input type="hidden" name="orderID" value="345">
                                        <input type="hidden" name="amount" value="{{$amount * 100}}">
                                        <input type="hidden" name="quantity" value="1">
                                        <input type="hidden" name="currency" value="NGN">
                                        <input type="hidden" name="metadata" value="{{ json_encode($array = ['consult_id' => $user->id, 'price_id' => $type->id, 'type' => 'consult']) }}" >
                                        <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="buton buton-bg" >Make Payment!</button>
                                    </div>
                                </div>
                            </form>
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>


        </div>
    </div>
</section>

@include('landing.footer');



