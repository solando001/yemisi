<!doctype html>

<html class="no-js" lang="zxx">

<head>

    <!-- metas -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Yemisi Adeyeye">
    <meta name="keywords" content="portfolio, personal, corporate, business, parallax, creative, agency">
    <meta property="og:url"           content="https://wwww.yemisiadeyeye.com" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="{{$details->title}}" />
    <meta property="og:description"   content="{{$details->content}}" />
    <meta property="og:image"         content="https://wwww.yemisiadeyeye.com/blog/{{$details->image}}" />

    <!-- title -->
    <title>My Blog | Yemisi Adeyeye</title>

    <!-- favicon -->
    <link href="img/favicon.ico" rel="icon" type="image/png">

    <!-- bootstrap css -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet">

    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">

    <!-- magnific-popup CSS -->
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">

    <!-- animate.min CSS -->
    <link rel="stylesheet" href="{{asset('css/animate.min.css')}}">

    <!-- Font Icon Core CSS -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/et-line.css')}}">

    <!-- Core Style Css -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <!--[if lt IE 9]-->
    <script src="{{asset('js/html5shiv.min.js')}}"></script>


    <!--[endif]-->
    <style>
        .circle-icon {
            background: purple;
            color: white;
            padding:8px;
            border-radius: 50%;
        }
    </style>
</head>

<body>

<!-- ====== Preloader ======  -->
<div class="loading">
    <div class="load-circle">
    </div>
</div>
<!-- ======End Preloader ======  -->
@include('landing.nav')

<!--====== Blog ======-->
<section class="blogs section-padding">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="posts">

                    <div class="post mb-80">
                        <div class="post-img">
                            <a href="#0">
                                <img src="{{$details->image}}" alt="">
                            </a>
                        </div>
                        <div class="content">
                            <div class="post-meta">
                                <div class="post-title">
                                    <h5>
                                        <a href="#">{{$details->title}}</a>
                                    </h5>
                                </div>
                                <ul class="meta">
                                    <li>
                                        <a href="#0">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                            Admin
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#0">
                                            <i class="fa fa-folder-open" aria-hidden="true"></i>
                                            {{$details->category->name}}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#0">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>

                                           {{\Carbon\Carbon::parse($details->created_at)->diffForhumans()}}
                                        </a>
                                    </li>
{{--                                    <li>--}}
{{--                                        <a href="#0">--}}
{{--                                            <i class="fa fa-tags" aria-hidden="true"></i>--}}
{{--                                            Blog,WordPress,ThemeForest--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
                                    <li>
                                        <a href="#0">
                                            <i class="fa fa-comments" aria-hidden="true"></i>
                                            {{count($details->comment)}} Comments
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="post-conts">
                                {!! $details->content !!}
                            </div>


                            <!-- Load Facebook SDK for JavaScript -->
                            <div id="fb-root"></div>
                            <script>
                                (function(d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) return;
                                    js = d.createElement(s); js.id = id;
                                    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));
                            </script>


                            <div class="share-post">
                                <span>Share Post</span>
                                <ul>
                                    <div class="fb-share-button"
                                         data-href="https://www.yemisiadeyeye.com/blog/{{$details->slug}}"
                                         data-layout="button_count">
                                    </div>

{{--                                    <li><i class="fa fa-facebook" aria-hidden="true"><a href="http://wwww.yemisiadeyeye.com/blog/{{$details->slug}}"></a> </i></li>--}}
{{--                                    <li><i class="fa fa-twitter" aria-hidden="true"></i></li>--}}
{{--                                    <li><i class="fa fa-youtube" aria-hidden="true"></i></li>--}}
{{--                                    <li><i class="fa fa-google-plus" aria-hidden="true"></i></li>--}}
                                </ul>
                            </div>

                        </div>
                    </div>

                    <div class="comments-area mb-80">
                        @if(count($details->comment) > 0)
                        <div class="title-g mb-50">
                            <h3>Comments</h3>
                        </div>

                        @foreach($details->comment as $comment)
                        <div class="comment-box">
                            <div class="author-thumb">
                                <img src="{{asset('img/blog/01.png')}}" alt="">
                            </div>
                            <div class="comment-info">
                                <h6><a href="#0">{{$comment->name}} - {{\Carbon\Carbon::parse($comment->created_at)->diffForhumans()}} </a></h6>
                                <p>{{$comment->text}}</p>

{{--                                <div class="reply">--}}
{{--                                    <a href="#0">--}}
{{--                                        <i class="fa fa-reply" aria-hidden="true"></i>--}}
{{--                                        Reply--}}
{{--                                    </a>--}}
{{--                                </div>--}}
                            </div>
                        </div>
                            @endforeach

                            @endif

                    </div>

                    <div class="comment-form">
                        <div class="title-g mb-50">
                            <h3>Post A Comment</h3>
                        </div>

                        @if(session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        @if(session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                        <form class="form" method="post" action="{{url('post/comment')}}">
                            @csrf
                            <div class="controls">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input id="form_name" type="text" name="name" placeholder="Name" required="required">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input id="form_email" type="email" name="email" placeholder="Email" required="required">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea id="form_message" name="text" placeholder="Message" rows="4" required="required"></textarea>
                                        </div>

                                        <input type="hidden" name="post_id" value="{{$details->id}}">

                                        <input type="submit" value="Submit" class="buton buton-bg">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

{{--            <div class="col-md-4">--}}
{{--                <div class="side-bar">--}}
{{--                    <div class="widget search">--}}
{{--                        <form>--}}
{{--                            <input type="search" name="" placeholder="Type here ...">--}}
{{--                            <button type="submite"><i class="fa fa-search" aria-hidden="true"></i></button>--}}
{{--                        </form>--}}
{{--                    </div>--}}

{{--                    <div class="widget">--}}
{{--                        <div class="widget-title">--}}
{{--                            <h6>Recent Posts</h6>--}}
{{--                        </div>--}}
{{--                        <ul>--}}
{{--                            <li><a href="#0">Top WordPress Themes and Plugins for Hotels.</a></li>--}}
{{--                            <li><a href="#0">Master These Awesome New Skills in March.</a></li>--}}
{{--                            <li><a href="#0">he 20 Best Lightroom Presets You Need.</a></li>--}}
{{--                            <li><a href="#0">Best Design Items to Appeal to the Millennial.</a></li>--}}
{{--                            <li><a href="#0">Stander Post With Image.</a></li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}

{{--                    <div class="widget">--}}
{{--                        <div class="widget-title">--}}
{{--                            <h6>Recent Comments</h6>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="widget">--}}
{{--                        <div class="widget-title">--}}
{{--                            <h6>Archives</h6>--}}
{{--                        </div>--}}
{{--                        <ul>--}}
{{--                            <li><a href="#0">January 2018</a></li>--}}
{{--                            <li><a href="#0">February 2018</a></li>--}}
{{--                            <li><a href="#0">March 2018</a></li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}

{{--                    <div class="widget">--}}
{{--                        <div class="widget-title">--}}
{{--                            <h6>Categories</h6>--}}
{{--                        </div>--}}
{{--                        <ul>--}}
{{--                            <li><a href="#0">WordPress</a></li>--}}
{{--                            <li><a href="#0">ThemeForest</a></li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}

{{--                    <div class="widget">--}}
{{--                        <div class="widget-title">--}}
{{--                            <h6>Meta</h6>--}}
{{--                        </div>--}}
{{--                        <ul>--}}
{{--                            <li><a href="#0">Log in</a></li>--}}
{{--                            <li><a href="#0">Entries RSS</a></li>--}}
{{--                            <li><a href="#0">Comments RSS</a></li>--}}
{{--                            <li><a href="#0">WordPress.org</a></li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--            </div>--}}

        </div>
    </div>
</section>
<!--====== End Blog ======-->


<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>

<!-- bootstrap -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>

<!-- scrollIt -->
<script src="{{asset('js/scrollIt.min.js')}}"></script>

<!-- magnific-popup -->
<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>

<!-- owl carousel -->
<script src="{{asset('js/owl.carousel.min.js')}}"></script>

<!-- stellar js -->
<script src="{{asset('js/jquery.stellar.min.js')}}"></script>

<!-- animated.headline -->
<script src="{{asset('js/animated.headline.js')}}"></script>

<!-- jquery.waypoints.min js -->
<script src="{{asset('js/jquery.waypoints.min.js')}}"></script>

<!-- jquery.counterup.min js -->
<script src="{{asset('js/jquery.counterup.min.js')}}"></script>

<!-- isotope.pkgd.min js -->
<script src="{{asset('js/isotope.pkgd.min.js')}}"></script>

<!-- validator js -->
<script src="{{asset('js/validator.js')}}"></script>

<!-- custom script -->
<script src="{{asset('js/custom.js')}}"></script>

