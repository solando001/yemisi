<!doctype html>

<html class="no-js" lang="zxx">

<head>

    <!-- metas -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Yemisi Adeyeye">
    <meta name="keywords" content="portfolio, personal, corporate, business, parallax, creative, agency">

    <!-- title -->
    <title>HR Template | Yemisi Adeyeye</title>

    <!-- favicon -->
    <link href="img/favicon.ico" rel="icon" type="image/png">

    <!-- bootstrap css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet">

    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- magnific-popup CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">

    <!-- animate.min CSS -->
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- Font Icon Core CSS -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/et-line.css">

    <!-- Core Style Css -->
    <link rel="stylesheet" href="css/style.css">

    <!--[if lt IE 9]-->
    <script src="js/html5shiv.min.js"></script>
    <!--[endif]-->
    <style>
        .circle-icon {
            background: purple;
            color: white;
            padding:8px;
            border-radius: 50%;
        }
    </style>
</head>

<body>

<!-- ====== Preloader ======  -->
<div class="loading">
    <div class="load-circle">
    </div>
</div>
<!-- ======End Preloader ======  -->
{{--@include('landing.nav')--}}

<!-- ====== Navgition ======  -->
<nav class="navbar navbar-default blog-nav navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-icon-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- logo -->
            <a class="logo" href="{{url('/')}}">
{{--                <img src="{{asset('img/logo.png')}}">--}}
            </a>

        </div>

        <!-- Collect the nav links, and other content for toggling -->
        <div class="collapse navbar-collapse" id="nav-icon-collapse">
            <!-- links -->
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{url('/')}}" style="font-size: 17px;">Home</a></li>
                <li><a href="{{url('/get-my-book')}}" style="font-size: 17px">Get My Book</a></li>
                <li><a href="{{url('my-profile')}}" style="font-size: 17px">My Profile</a></li>
                <li><a href="{{url('work-with-me')}}" style="font-size: 17px">Work with Me</a></li>
                <li><a href="{{url('blog')}}" style="font-size: 17px">Blog</a></li>

                <li><a href="https://web.facebook.com/yemisi.adeyeye.5" target="_blank">
                        <span><i class="fa fa-facebook circle-icon" aria-hidden="true"></i></span>
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com/drroadeyeye?lang=en" target="_blank">
                        <span><i class="fa fa-twitter circle-icon" aria-hidden="true"></i></span>
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/yemisiadeyeye/?hl=en" target="_blank">
                        <span><i class="fa fa-instagram circle-icon" aria-hidden="true"></i></span>
                    </a>
                </li>
                <li>
                    <a href="https://ng.linkedin.com/in/yemisi-adeyeye-5b0b96120" target="_blank">
                        <span><i class="fa fa-linkedin circle-icon" aria-hidden="true"></i></span>
                    </a>
                </li>
                <li>
                    <a href="https://wa.me/2349094407502" target="_blank">
                        <span><i class="fa fa-whatsapp circle-icon" aria-hidden="true"></i></span>
                    </a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container -->
</nav>


<section class="portfolio bg-gray section-padding pb-70" data-scroll-index="3">
    <div class="container">
        <div class="row">
            <!-- section heading -->
            <div class="section-head">
                <h3>HR Manual.</h3>
            </div>

            <div class="row">

                <div class="col-md-12">
                    <center><h3>Yemisi Adeyeye</h3></center>
                    <br><br><br>
                    <center><b style="color: red; text-transform: capitalize"><h4>Presents</h4></b></center>
                    <br><br><br>
                    <center><h1 style="color: blue; font-weight: bolder">Company HR Policy Manual</h1></center>
                    <br><br>
                    <center> <h4 style="color: red; text-transform: capitalize">THE ULTIMATE STAFF POLICY MANUAL TEMPLATE</h4> </center>
                    <br><br>

                    <h5 style="color: blue; font-weight: bolder">
                        Do you have headaches managing your business and your staff?<br><br>
                        You don't have a rule for them to follow?<br><br>
                        You can't even take a break off because things can muddle up the next minute you are away?<br><br>

                        It's probably because you don't have a Staff Policy to guide you and your team.<br><br>
                        I have carefully prepared this ultimate staff policy manual to guide you to develop your own policies <br> <br>and bring in the structure that will give you peace of mind in your business.
                    </h5>
                    <br><br>

                   <center>
{{--                       <div class="hero-img mb-30">--}}
                       <div class="hero-img mb-30">
                           {{--                        <img src="img/abt1.jpg" alt="" style="border-radius: 50%">--}}
                           <img src="img/hrpolicy.jpeg" alt="" width="200" height="200">
                       </div>

                       <br><br><br>
{{--                       <button type="submit" class="buton boton">Buy Online </button>--}}

                       <button type="button" class="buton boton" data-toggle="modal" data-target="#exampleModal_1">
                           Buy Online
                       </button>

                       <button type="button" class="buton boton" data-toggle="modal" data-target="#exampleModal">
                           Buy Offline (Bank Transfer)
                       </button>

{{--                       <a href="" class="buton boton">Buy Offline</a>--}}
                   </center>
                </div>



            </div>
        </div>
    </div>
</section>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Bank Transfer Payment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul>
                    <li>Account Name : <b>Adeyeye Rebecca Oluwayemisi</b></li>
                    <li>Account Number : <b>1775027024</b></li>
                    <li>Bank Name : <b>First City Monument Bank (FCMB)</b></li>
                    <li>Amount : <b>&#8358;50,000</b></li>
                </ul>
                <hr>
                <b style="color: red"><i>After successful payment, please contact this number 0909 440 7502 with proof of payment.</i></b>
                <h3></h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
{{--                <button type="button" class="btn btn-primary">Save changes</button>--}}
            </div>
        </div>
    </div>
</div>




<div class="modal fade" id="exampleModal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Enter Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ url('hrpay') }}" enctype="multipart/form-data" class="form-inlineoo">
                    @csrf

                    <div class="form-group">
                        <label for="exampleInputPassword1">Full Name</label>
                        <input type="text" name="name" class="form-control" id="exampleInputPassword1" placeholder="Enter Full Name" required>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Phone Number</label>
                        <input type="number" name="phone" class="form-control" id="exampleInputPassword1" placeholder="Enter Phone Number" required>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Email Address" required>
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Address</label>
                        <input type="text" name="address" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Address" required>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Location</label>
                        <input type="text" name="location" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Location" required>
                    </div>

                    <input type="hidden" name="coupon" value="00">
                    <input type="hidden" name="gender" value="male">

                    <?php $ref = Ramsey\Uuid\Uuid::uuid4();?>
{{--                    <input type="hidden" name="book_ref" value="{{$sale->ref}}">--}}
                    <input type="hidden" name="ref" value="{{$ref}}">


                    <button type="submit" class="btn btn-primary">Continue</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{--                <button type="button" class="btn btn-primary">Save changes</button>--}}
            </div>
        </div>
    </div>
</div>




@include('landing.footer');







