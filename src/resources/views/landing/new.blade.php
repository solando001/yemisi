<!doctype html>

<html class="no-js" lang="zxx">

<head>
    <!-- metas -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Welcome to Yemisi Adeyeye Official Website">
    <meta name="keywords" content="portfolio, personal, corporate, business, parallax, creative, agency">

    <meta property="og:image" content="https://yemisiadeyeye.com/img/abt3.jpg">
    <meta property="og:image:type" content="image/jpg">
    <meta property="og:image:width" content="1024">
    <meta property="og:image:height" content="1024">
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://yemisiadeyeye.com/"/>
    <meta property="og:title" content="Yemisi Adeyeye" />
    <meta property="og:description" content="Welcome to Yemisi Adeyeye." />

    <!-- title -->
    <title>Home | Yemisi Adeyeye</title>

    <!-- favicon -->
    <link href="{{asset('img/fv.png')}}" rel="icon" type="image/png">

    <!-- bootstrap css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet">

    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- magnific-popup CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">

    <!-- animate.min CSS -->
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- Font Icon Core CSS -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/et-line.css">

    <!-- Core Style Css -->
    <link rel="stylesheet" href="css/style.css">

    <!--[if lt IE 9]-->
    <script src="js/html5shiv.min.js"></script>
    <!--[endif]-->
    <style>
        .circle-icon {
            background: purple;
            color: white;
            padding:8px;
            border-radius: 50%;
        }
    </style>


{{--    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <script>
        // $(document).ready(function(){
        //     $("#myModal").modal('show');
        // });
    </script>

    <style>
        #MyPopup {
            display:none;
        }
    </style>


    {{--    <script src="https://apps.elfsight.com/p/platform.js" defer></script>--}}
{{--    <div class="elfsight-app-f5ae0a88-7624-4866-937c-99250a838870"></div>--}}
</head>

<body>

<!-- ====== Preloader ======  -->
<div class="loading">
    <div class="load-circle">
    </div>
</div>
<!-- ======End Preloader ======  -->

<!-- ====== Navgition ======  -->
<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-icon-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar" style="color: black"></span>
                <span class="icon-bar" style="color: black"></span>
                <span class="icon-bar" style="color: black"></span>
            </button>
            <!-- logo -->
            <a class="logo" href="{{url('/')}}" data-scroll-nav="0" style="font-size: 19px" >
                <img src="img/logo001.png">
            </a>

        </div>

        <!-- Collect the nav links, and other content for toggling -->
        <div class="collapse navbar-collapse" id="nav-icon-collapse">

            <!-- links -->
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{url('/')}}" style="font-size: 17px; color: black">Home</a></li>
{{--                <li><a href="{{url('/booklaunch')}}" style="font-size: 17px; color: black">BookLaunch</a></li>--}}
{{--                <li><a href="#" data-scroll-nav="0" class="active">Home</a></li>--}}
{{--                <li><a href="#" data-scroll-nav="1">About</a></li>--}}
{{--                <li><a href="#" data-scroll-nav="2">Get My Book</a></li>--}}
                <li><a href="{{url('/get-my-book')}}" style="font-size: 17px; color: black" >Get My Book</a></li>
                <li><a href="{{url('my-profile')}}" style="font-size: 17px; color: black">My Profile</a></li>
{{--                <li><a href="#" data-scroll-navl-nav="4">Testimonial</a></li>--}}
                <li><a href="{{url('work-with-me')}}" style="font-size: 17px; color: bla">Work with Me</a></li>
                <li><a href="{{url('blog')}}" style="font-size: 17px">Blog</a></li>

                <li><a href="https://web.facebook.com/yemisi.adeyeye.5" target="_blank">
                        <span><i class="fa fa-facebook circle-icon" aria-hidden="true"></i></span>
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com/drroadeyeye?lang=en" target="_blank">
                        <span><i class="fa fa-twitter circle-icon" aria-hidden="true"></i></span>
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/yemisiadeyeye/?hl=en" target="_blank">
                        <span><i class="fa fa-instagram circle-icon" aria-hidden="true"></i></span>
                    </a>
                </li>
                <li>
                    <a href="https://ng.linkedin.com/in/yemisi-adeyeye-5b0b96120" target="_blank">
                        <span><i class="fa fa-linkedin circle-icon" aria-hidden="true"></i></span>
                    </a>
                </li>

                <li>
                    <a href="https://wa.me/2349094407502" target="_blank">
                        <span><i class="fa fa-whatsapp circle-icon" aria-hidden="true"></i></span>
                    </a>
                </li>

            </ul>


        </div><!-- /.navbar-collapse -->
    </div><!-- /.container -->
</nav>
<!-- ====== End Navgition ======  -->

{{--@include('landing.nav')--}}
<!-- ====== Header ======  -->
{{--<section id="homes" class="header" data-scroll-index="0" style="background: url(img/new_home1.png) no-repeat center;" data-stellar-background-ratio="0.8">--}}
<section id="homes" class="header" data-scroll-index="0" style="background-image: url(img/homepg.jpg);" data-stellar-background-ratio="0.8">

    <div class="v-middle">
        <div class="container">
            <div class="row">

                <!-- caption -->
                <div class="caption">
{{--                    <h5>Hello</h5>--}}
{{--                    <h1 class="cd-headline clip">--}}
{{--                        <span class="blc">I Am </span>--}}
{{--                        <span class="cd-words-wrapper">--}}
{{--					              <b class="is-visible">Larry Daniels</b>--}}
{{--					              <b>Developer</b>--}}
{{--					              <b>Designer</b>--}}
{{--					            </span>--}}
{{--                    </h1>--}}

                    <!-- social icons -->
{{--                    <div class="social-icon">--}}
{{--                        <a href="#0">--}}
{{--                            <span><i class="fa fa-facebook" aria-hidden="true"></i></span>--}}
{{--                        </a>--}}
{{--                        <a href="#0">--}}
{{--                            <span><i class="fa fa-twitter" aria-hidden="true"></i></span>--}}
{{--                        </a>--}}
{{--                        <a href="#0">--}}
{{--                            <span><i class="fa fa-linkedin" aria-hidden="true"></i></span>--}}
{{--                        </a>--}}
{{--                        <a href="#0">--}}
{{--                            <span><i class="fa fa-behance" aria-hidden="true"></i></span>--}}
{{--                        </a>--}}
{{--                        <a href="#0">--}}
{{--                            <span><i class="fa fa-youtube" aria-hidden="true"></i></span>--}}
{{--                        </a>--}}
{{--                    </div>--}}
                </div>
                <!-- end caption -->
            </div>
        </div><!-- /row -->
    </div><!-- /container -->
</section>
<!-- ====== End Header ======  -->

{{--<div class="container">--}}
    <!-- Trigger the modal with a button -->
{{--    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>--}}

    <!-- Modal -->
{{--    <div class="modal show" id="myModal" role="dialog">--}}


{{--</div>--}}


<!-- ====== Hero ======  -->
<section class="hero section-padding pb-70" data-scroll-index="1">
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-sm-12">
                <div class="content mb-30">
                    <h3>I'm Yemisi Adeyeye.</h3>
                    <span class="sub-title">Business Consultant | Public Speaker | Life Coach </span>
                    <p>
                        I'm glad you are here! I help business owners and organizations to understand and create structure in their businesses so they can enjoy all the benefits
                        of entrepreneurship and growth. I am passionate about business and individual healthiness. Let me take you through the journey.
                    </p>
                </div>
            </div>


        </div><!-- /row -->
    </div><!-- /container -->
</section>
<!-- ====== End Hero ======  -->

<!--====== Clients ======-->
<section class="clients section-padding bg-gray" data-scroll-index="4">
    <div class="container">
        <div class="row">

            <!-- section heading -->
            <div class="section-head">
                <h3>Testimonials.</h3>
            </div>

            <!-- owl carousel -->
            <div class="col-md-offset-1 col-md-10">
                <div class="owl-carousel owl-theme text-center">

                    <!-- citems -->
                    <div class="citem">
                        <div class="author-img">
                            <img src="img/mazuba.jpeg" alt="">
                        </div>
                        <p>
                            Dr. Yemisi Adeyeye is a very hardworking, determined, innovative, goal oriented, compassionate
                            mentor I have ever come across. She is also very accommodative and easy to get along with always
                            determined to make others succeed as well. I have also been following her workshops on face book and
                            they are benefiting a lot of people.<br>
                            I will forever remain grateful for the knowledge and skills that I continue acquiring from Dr. Yemisi
                            Adeyeye. I am wishing her all the best in her future undertaking.
                        </p>
                        <h6>Mazuba Mwanachingwala (Mrs)</h6>
                        <span>Director-MFOUR AGRO LIMITED, Zambia</span>
                    </div>

                    <div class="citem">
                        <div class="author-img">
                            <img src="img/tobi.jpeg" alt="">
                        </div>
                        <p>Dr. Yemisi Adeyeye is a  medicopreneur who teaches business and mentors business owners passionately.
                            If you've ever heard her speak in any business conference/training, you will always look forward to meeting her for personal business mentoring. She's a compendium of knowledge. She articulates her experience and expertise in
                            mentoring and transforming small, medium and large scale businesses.</p>
                        <h6>Farohunbi Samuel</h6>
                        <span>CEO, Goodnews Infotechnologies, Nigeria</span>
                    </div>

                    <div class="citem">
                        <div class="author-img">
                            <img src="img/akolade.jpeg" alt="">
                        </div>
                        <p>
                            My business was just at the verge of wrapping up. I was confused especially in the issue of Structure in business. When I met Dr. Yemisi, I literally cried for not meeting her before that time. She breathed into my business and I am proud at what we are doing at The Book Surgeons International.
                            Via her mentoring and coaching, we have been able to secure clients locally and internationally and we have made a lot of them Published Authors.
                        </p>
                        <h6>Akolade Oluwatoba</h6>
                        <span>Founder, The Book Surgeons International, Nigeria</span>
                    </div>

                    <div class="citem">
                        <div class="author-img">
                            <img src="img/angelo.jpeg" alt="">
                        </div>
                        <p>
                            Dr Yemisi Adeyeye is a medical doctor but has shown a keen interest in entrepreneurship and the development of entrepreneurs.
                            I have worked with her on numerous occasions and have heard her present and she has amazing passion to help entrepreneurs.
                            She has also started and funded a programme to train entrepreneurs,
                            which takes a lot of energy and commitment. She has acted as a mentor to those who need her expertise, energy and guidance.
                        </p>
                        <h6>Angelo Kehayas</h6>
                        <span>CEO, Profweb, South Africa.</span>
                    </div>
                </div>
            </div>
        </div><!-- /row -->
    </div><!-- /container -->
</section>
<!--====== End clients ======-->


{{--<!--====== Blog ======-->--}}
<section class="blog section-padding" data-scroll-index="5">
    <div class="container">
        <div class="row">

            <!-- section heading -->
            <div class="section-head">
                <h3>My Blog.</h3>
            </div>

            <!-- owl carsouel -->
            <div class="owl-carousel owl-theme">

                <!-- pitems -->
                @if(isset($posts) && count($posts)>0)
                    @foreach($posts as $post)
                        <div class="pitem">
                    <div class="post-img">
                        <img src="{{$post->image}}" alt="">
                    </div>
                    <div class="content">
                        <h6 class="tag">
                            <a href="#">{{$post->category->name}}</a>
                        </h6>
                        <h4>
                            <a href="{{url('blog/'.$post->slug)}}">{{$post->title}}</a>
                        </h4>
                        <span class="more">
                            <a href="{{url('blog/'.$post->slug)}}">Read More</a>
                        </span>
                    </div>
                </div>
                    @endforeach
                @endif

            </div>

        </div><!-- /row -->
    </div><!-- /container -->
</section>
{{--<!--====== End Blog ======-->--}}
<!--====== Contact ======-->
<section class="contact section-padding" data-scroll-index="6">
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <center>
                    <div class="item">
                        <span class="icon"><i class="fa fa-book fa-4x" aria-hidden="true"></i></span>
                        <br><br>
                        <h6>Attend My Book Launch</h6>
                        <br><br>
                        <a href="{{url('/booklaunch')}}" class="buton boton">Book Launch </a>
                    </div>
                </center>
            </div>
        </div>
        <br><br>

        <div class="row">

            <!-- section heading -->
            <div class="section-head">
                <h3>Join My Business Network</h3>
            </div>

            <div class="col-md-offset-1 col-md-10">

                <!-- contact info -->
                <div class="info mb-50">

                    <div class="col-md-4">
                        <div class="item">
                            <img src="img/lifecount.png" alt="">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="item">
                            <p style="text-align: justify">
                                Lifefount Business Network is a Business Support group on Facebook for Aspiring and Season Business Owners to get information and trainings that will grow their
                                businesses. Business Owners also have the opportunity to market their goods and services on the platform.</p>
                        </div>
                        <a href="https://web.facebook.com/groups/lifefountbusinessnetwork" target="_blank" class="buton boton-bg">Join Now</a>
                    </div>


                    <div class="clearfix"></div>


                </div>
                <h5>Subscribe to My Newsletters</h5>
                <br>
                <!-- contact form -->

{{--                    <div class="messages"></div>--}}

                    @if(session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if(session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif

                        <div class="controls">
                            <div class="row">

                                <form class="form"  method="post" action="{{url('subscribe/newsletter')}}">
                                    @csrf
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="email" name="email" placeholder="Email Address" required="required">
                                    </div>
                                </div>
                                <button type="submit" class="buton buton-bg">Subscribe</button>
                                </form>

                            </div>
                        </div>
            </div>
        </div><!-- /row -->
    </div><!-- /container -->
</section>
<!--====== End Contact ======-->


{{--<div class="modal " id="myModal" role="dialog">--}}
<div class="modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Welcome</h5>
            </div>
            <div class="modal-body">
                <h5>
                    Attend my book launch and get access to one of my online courses (video) for free
                </h5>
                <br>
                <form action="{{url('post/info')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Full Name</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="name" aria-describedby="emailHelp" placeholder="Enter full name" required>
                        <small id="emailHelp" class="form-text text-muted">We'll never share your details with anyone else.</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" placeholder="Enter email" required>
                        <small id="emailHelp" class="form-text text-muted">We'll never share your details with anyone else.</small>
                    </div>
{{--                    <div class="form-group">--}}
{{--                        <label for="exampleInputEmail1">Phone Number</label>--}}
{{--                        <input type="text" class="form-control" id="exampleInputEmail1" name="phone" aria-describedby="emailHelp" placeholder="Enter phone number" required>--}}
{{--                        <small id="emailHelp" class="form-text text-muted">We'll never share your details with anyone else.</small>--}}
{{--                    </div>--}}
                    <input type="hidden" name="phone" value="08067336746">
                    <button type="submit" class="btn btn-success">Submit</button>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<script>
    // function setCookie(cname, cvalue, exdays) {
    //     var d = new Date();
    //     d.setTime(d.getTime() + (exdays*24*60*60*1000));
    //     var expires = "expires="+d.toUTCString();
    //     document.cookie = cname + "=" + cvalue + "; " + expires;
    // }
    //
    // function getCookie(cname) {
    //     var name = cname + "=";
    //     var ca = document.cookie.split(';');
    //     for(var i=0; i<ca.length; i++) {
    //         var c = ca[i];
    //         while (c.charAt(0)==' ') c = c.substring(1);
    //         if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    //     }
    //     return "";
    // }
    //
    // var cookie = getCookie('shown');
    // if (!cookie) {
    //     showPopup();
    // }
    //
    // function showPopup() {
    //     setCookie('shown', 'true', 7);
    //     document.querySelector('#myModal').style.display = 'block';
    // }
</script>
@include('landing.footer');


{{--    <div id="myModals" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">--}}
{{--        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">--}}
{{--            <div class="modal-content">--}}

{{--                <div class="modal-content">--}}
{{--                    <div class="modal-header">--}}
{{--                        <h2 class="modal-title" id="exampleModalLabel">Welcome to University Scholarships Portal</h2>--}}
{{--                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                            <span aria-hidden="true">&times;</span>--}}
{{--                        </button>--}}
{{--                    </div>--}}
{{--                    <div class="modal-body">--}}
{{--                        <h4>Dominahl Technologies wish to inform the general public that</h4>--}}
{{--                        <p>--}}
{{--                            1. <b>OYENIYA Oluwatobi</b> - <i>Obafemi Awolowo University,Ile-Ife</i> <br>--}}
{{--                            2. <b>AYODELE Oluwasogo Emmanuel</b> - <i>Ladoke Akintola University of Technology, Ogbomoso</i><br>--}}
{{--                            3. <b>ADEWALE Jolaoso</b> - <i>Pan-Atlantic University, Lagos</i><br>--}}
{{--                            have been selected as awardees of our 2019 scholarship having performed excellently in our scholarship <br> examination held online between 1-5 July, 2019. Details of the award ceremony will be communicated shortly.</p>--}}
{{--                    </div>--}}
{{--                    <div class="modal-footer">--}}
{{--                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
{{--                        <a  href="https://e-portal.com.ng/"  target="_blank" class="btn btn-primary">Continue to Practice Questions</a>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--@endsection--}}


