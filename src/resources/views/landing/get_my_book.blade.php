<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <!-- metas -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Yemisi Adeyeye">
    <meta name="keywords" content="portfolio, personal, corporate, business, parallax, creative, agency">
    <!-- title -->
    <title>Get My Book | Yemisi Adeyeye</title>

    <!-- favicon -->
    <link href="img/favicon.ico" rel="icon" type="image/png">

    <!-- bootstrap css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet">

    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- magnific-popup CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">

    <!-- animate.min CSS -->
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- Font Icon Core CSS -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/et-line.css">

    <!-- Core Style Css -->
    <link rel="stylesheet" href="css/style.css">

    <!--[if lt IE 9]-->
    <script src="js/html5shiv.min.js"></script>
    <!--[endif]-->

    <style>
        .circle-icon {
            background: purple;
            color: white;
            padding:8px;
            border-radius: 50%;
        }
    </style>

</head>

<body>

<!-- ====== Preloader ======  -->
<div class="loading">
    <div class="load-circle">
    </div>
</div>
<!-- ======End Preloader ======  -->
@include('landing.nav')

<!-- ====== Services ======  -->
{{--<section class="services section-padding bg-gray text-center pb-70" data-scroll-index="2">--}}
<section class="hero section-padding bg-gray pb-70" data-scroll-index="2">
    <div class="container">

        <!-- section heading -->
        <div class="section-head">
            <h3>Get My Book.</h3>
        </div>

        <div class="row">

            <div class="col-md-5">
                <div class="hero-img mb-30">
                    <img src="img/book1.jpeg" alt="">
                </div>
            </div>

            <!-- content -->
            <div class="col-md-7">
                <p class="content mb-30">
                <h3>Entrepreneurship is a Beautiful Thing</h3>
                {{--                    <span class="sub-title">UI / UX Designer & Web Developer</span>--}}
                <p style="text-align: justify">Entrepreneurship is a beautiful thing  is a guide book on how to structure and grow your business.

                    This book focuses on the fundamentals of entrepreneurship and arms you with skills that will transform your business to an organisation that can run with or without your active supervision.

                    It is spiced with stories of the author's experience on her entrepreneurial journey and her learnings from different
                    institutions, organisations,networks and mentors.</p>

                <p>Reading this book will add immense values to you and your business which includes:</p>
                <br>

                <p style="text-align: justify">
                    1. Staff management  skills using  policies,processes and standard operating procedures </br></br>
                    2. Understanding of financial laws and acts that guides enterprise management.</br></br>
                    3. Understanding of basics of bookkeeping ,accounting and marketing for Micro, Small and Medium Enterprises Sector (MSME)</br></br>
                    3. Gain more confidence in delegating duties that relates to business operations and focus on the management of your business.</br></br>
                    4. Boost your corporate governance skill and make your business ready for investment and numerous other opportunities available to MSME Sector.</br></br>
                    5. Earn you quality time for family and other things you have passion for without worrying about your business whether it will crash in your absence.</br></br>
                </p>

                <div class="clearfix"></div>

                @if(session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif





                <button type="button" class="button boton" data-toggle="modal" data-target="#exampleModal">
                   Buy Online
                </button>

                <div class="modal fade" data-keyboard="false" data-backdrop="static" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Enter Your Information</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <form method="POST" action="{{ url('customer') }}" enctype="multipart/form-data" class="form-inlineoo">
                                    @csrf

                                    <div class="contact-form">



                                    {{--                                            <!-- IF MAIL SENT SUCCESSFULLY -->--}}
                                    {{--                                            <div class="success">--}}
                                    {{--                                                Your message has been sent successfully.--}}
                                    {{--                                            </div>--}}
                                    {{--                                            <!-- IF MAIL SENDING UNSUCCESSFULL -->--}}
                                    {{--                                            <div class="error">--}}
                                    {{--                                                E-mail must be valid and message must be longer than 1 character.--}}
                                    {{--                                            </div>--}}
                                    <!-- Form Fields -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="sr-only" for="first_name">Full Name</label>
                                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="Full Name" autofocus>
                                                {{--                                                        @error('name')--}}
                                                {{--                                                        <span class="invalid-feedback" role="alert">--}}
                                                {{--                                                            <strong>{{ $message }}</strong>--}}
                                                {{--                                                        </span>--}}
                                                {{--                                                        @enderror--}}
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="sr-only" for="last_name">Email</label>
                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email Address" >
                                                {{--                                                    @error('email')--}}
                                                {{--                                                    <span class="invalid-feedback" role="alert">--}}
                                                {{--                                                        <strong>{{ $message }}</strong>--}}
                                                {{--                                                    </span>--}}
                                                {{--                                                    @enderror--}}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="sr-only" for="phone_number">Phone</label>
                                                <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" placeholder="Phone Number" >
                                                {{--                                                    @error('phone')--}}
                                                {{--                                                    <span class="invalid-feedback" role="alert">--}}
                                                {{--                                                        <strong>{{ $message }}</strong>--}}
                                                {{--                                                    </span>--}}
                                                {{--                                                    @enderror--}}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="sr-only" for="email_address">Email</label>
                                                <select id="gender" name="gender" class="form-control @error('gender') is-invalid @enderror" >
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>
                                                </select>
                                                {{--                                                    @error('gender')--}}
                                                {{--                                                    <span class="invalid-feedback" role="alert">--}}
                                                {{--                                                        <strong>{{ $message }}</strong>--}}
                                                {{--                                                    </span>--}}
                                                {{--                                                    @enderror--}}
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">

                                                <input id="location" type="text" class="form-control @error('location') is-invalid @enderror" name="location" value="{{ old('location') }}" placeholder="Location" >
                                                {{--                                                    @error('location')--}}
                                                {{--                                                    <span class="invalid-feedback" role="alert">--}}
                                                {{--                                                        <strong>{{ $message }}</strong>--}}
                                                {{--                                                    </span>--}}
                                                {{--                                                    @enderror--}}
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">

                                                <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" placeholder="Address" >
                                                {{--                                                    @error('address')--}}
                                                {{--                                                    <span class="invalid-feedback" role="alert">--}}
                                                {{--                                                        <strong>{{ $message }}</strong>--}}
                                                {{--                                                    </span>--}}
                                                {{--                                                    @enderror--}}
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">

                                                <input id="coupon" type="text" class="form-control @error('coupon') is-invalid @enderror" name="coupon" value="{{ old('coupon') }}" placeholder="Coupon Code" >
                                                {{--                                                    @error('address')--}}
                                                {{--                                                    <span class="invalid-feedback" role="alert">--}}
                                                {{--                                                        <strong>{{ $message }}</strong>--}}
                                                {{--                                                    </span>--}}
                                                {{--                                                    @enderror--}}
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">

                                                <select id="quantity" name="quantity" class="form-control @error('gender') is-invalid @enderror">

                                                    <?php
                                                    for($i=1; $i<101; $i++){
                                                    ?>
                                                    <option value="{{$i}}">{{$i}}</option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <?php $ref = Ramsey\Uuid\Uuid::uuid4();?>
                                        <input type="hidden" name="book_ref" value="{{$sale->ref}}">
                                        <input type="hidden" name="ref" value="{{$ref}}">

                                        <button type="submit" class="btn btn-primary">Continue</button>


                                        {{--                                            <div class="col-md-12 text-center">--}}
                                        {{--                                                <button type="submit" name="submit" id="submit">Contact Now</button>--}}
                                        {{--                                            </div>--}}

                                    </div>
                                </form>

                                {{--                                <form method="POST" action="{{ url('customer') }}" enctype="multipart/form-data"--}}
                                {{--                                    <div class="form-group row">--}}

                                {{--                                        <div class="col-md-12">--}}
                                {{--                                            <label for="email">Full Name</label>--}}
                                {{--                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autofocus>--}}
                                {{--                                            @error('name')--}}
                                {{--                                            <span class="invalid-feedback" role="alert">--}}
                                {{--                                                <strong>{{ $message }}</strong>--}}
                                {{--                                            </span>--}}
                                {{--                                            @enderror--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="col-md-12">--}}
                                {{--                                            <label for="email">Email Address</label>--}}
                                {{--                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" >--}}
                                {{--                                            @error('email')--}}
                                {{--                                            <span class="invalid-feedback" role="alert">--}}
                                {{--                                                <strong>{{ $message }}</strong>--}}
                                {{--                                            </span>--}}
                                {{--                                            @enderror--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="col-md-12">--}}
                                {{--                                            <label for="email">Phone Number</label>--}}
                                {{--                                            <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" >--}}
                                {{--                                            @error('phone')--}}
                                {{--                                            <span class="invalid-feedback" role="alert">--}}
                                {{--                                                <strong>{{ $message }}</strong>--}}
                                {{--                                            </span>--}}
                                {{--                                            @enderror--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="col-md-12">--}}
                                {{--                                            <label for="email">Gender</label>--}}
                                {{--                                            <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" autofocus>--}}
                                {{--                                           <select id="gender" name="gender" class="form-control @error('gender') is-invalid @enderror" >--}}
                                {{--                                               <option value="Male">Male</option>--}}
                                {{--                                               <option value="Female">Female</option>--}}
                                {{--                                           </select>--}}
                                {{--                                            @error('gender')--}}
                                {{--                                            <span class="invalid-feedback" role="alert">--}}
                                {{--                                                <strong>{{ $message }}</strong>--}}
                                {{--                                            </span>--}}
                                {{--                                            @enderror--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="col-md-12">--}}
                                {{--                                            <label for="email">City/Town</label>--}}
                                {{--                                            <input id="location" type="text" class="form-control @error('location') is-invalid @enderror" name="location" value="{{ old('location') }}" autofocus>--}}
                                {{--                                            @error('location')--}}
                                {{--                                            <span class="invalid-feedback" role="alert">--}}
                                {{--                                                <strong>{{ $message }}</strong>--}}
                                {{--                                            </span>--}}
                                {{--                                            @enderror--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="col-md-12">--}}
                                {{--                                            <label for="email">Address</label>--}}
                                {{--                                            <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" autofocus>--}}
                                {{--                                            @error('address')--}}
                                {{--                                            <span class="invalid-feedback" role="alert">--}}
                                {{--                                                <strong>{{ $message }}</strong>--}}
                                {{--                                            </span>--}}
                                {{--                                            @enderror--}}
                                {{--                                        </div>--}}
                                {{--                                        <input type="hidden" name="book_ref" value="{{$sale->ref}}">--}}

                                {{--                                        <button class="btn btn-success">Submit</button>--}}
                                {{--                                    </div>--}}
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                {{--                                <button type="button" class="btn btn-primary">Save changes</button>--}}
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>

{{--            <div class="col-md-12">--}}
{{--                <div class="hero-img mb-30">--}}
{{--                    <img src="img/book2.jpeg" alt="">--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </div><!-- /row -->
    </div><!-- /container -->
</section>
<!-- ====== End Services ======  -->

@include('landing.footer');
