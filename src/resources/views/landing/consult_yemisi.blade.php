<!doctype html>

<html class="no-js" lang="zxx">

<head>

    <!-- metas -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Yemisi Adeyeye">
    <meta name="keywords" content="portfolio, personal, corporate, business, parallax, creative, agency">

    <!-- title -->
    <title>Work With Me | Yemisi Adeyeye</title>

    <!-- favicon -->
    <link href="img/favicon.ico" rel="icon" type="image/png">

    <!-- bootstrap css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet">

    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- magnific-popup CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">

    <!-- animate.min CSS -->
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- Font Icon Core CSS -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/et-line.css">

    <!-- Core Style Css -->
    <link rel="stylesheet" href="css/style.css">

    <!--[if lt IE 9]-->
    <script src="js/html5shiv.min.js"></script>
    <!--[endif]-->

    <style>
        .circle-icon {
            background: purple;
            color: white;
            padding:8px;
            border-radius: 50%;
        }
    </style>

</head>

<body>

<!-- ====== Preloader ======  -->
<div class="loading">
    <div class="load-circle">
    </div>
</div>
<!-- ======End Preloader ======  -->
@include('landing.nav')


<!-- ====== Services ======  -->
<section class="services section-padding bg-gray text-center pb-70" data-scroll-index="6">
    <div class="container">

        <!-- section heading -->
        <div class="section-head">
            <h3>Work With Me.</h3>
            <p>
                I will hold your hands and support your golden dreams as you navigate through the bends and curves in your business journey.
            </p>
        </div>

        <div class="row">

            <!-- items -->
            <div class="col-md-6">

                    <div class="item">
                        <span class="icon"><i class="fa fa-users" aria-hidden="true"></i></span>
                        <h6>One-On-One Consultation</h6>
                        <p>
                            My physical and online one-on-one sessions are always superlative, engaging and impactful.
                            You can book an appointment with me for business management consultancy, Human Resources management,  counselling on Life and Well-being and a host of other issues.
                        </p>
                        <br>
                        <a href="" class="buton boton" data-toggle="modal" data-target="#exampleModal">Book Appointment Now </a>
                    </div>

            </div>
            <div class="col-md-6">
                <div class="item">
                    <span class="icon"><i class="fa fa-bullhorn" aria-hidden="true"></i></span>
                    <h6>Group Coaching</h6>
                    <p>You can learn from my experience and expertise in business, health and wellness. I will deplore my wealth of knowledge passionately and excellently.
                    </p>
                    <br>
                    <a href="" class="buton boton" data-toggle="modal" data-target="#exampleModal1">Book Appointment Now</a>
                </div>
            </div>
            <div class="col-md-12">
                <div class="item">
                    <span class="icon"><i class="fa fa-umbrella" aria-hidden="true"></i></span>
                    <h6>Public Speaking</h6>
                    <p>
                        MY PUBLIC SPEAKING AREAS ARE <br>

                        1. Entrepreneurship–HowTo Start And Run A Successful Business, Human
                        Resources Management, Book keeping, Marketing and Sales, Branding <br>

                        2. Health –Cancer, Women’s Health, Genotype, etc <br>

                        3. Lifestyle– How To Break Addiction (Sexual and drug abuse), WeightLoss <br>

                        4. Marriage/Relationship–Sex Education, Love And Romance
                        Women And Girl issues<br>

                        5. Personal Development, Vision Board, Wheel of Life, Effective
                        Communication, Setting and Smashing your goals.<br>

                        6. Christian Topics
                    </p>
                    <br>
                    <a href="" data-toggle="modal" data-target="#exampleModal2" class="buton boton">Book Appointment Now </a>
                </div>
            </div>

        </div><!-- /row -->
    </div><!-- /container -->
</section>
<!-- ====== End Services ======  -->

@include('landing.footer');


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">One-On-One Consultation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
               <p>
                   Price: {{number_format($one->amount)}}
               </p>
                <hr>
                <form action="{{url('post/one')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Full Name</label>
                        <input type="text" class="form-control" name="name" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Full Name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Email Address</label>
                        <input type="email" name="email" class="form-control" id="exampleInputPassword1" placeholder="Email Address">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Phone Number</label>
                        <input type="text" name="phone" class="form-control" id="exampleInputPassword1" placeholder="Phone Number">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">How many hours</label>
                        <input type="number" name="hours" class="form-control" id="exampleInputPassword1" placeholder="Number of hours">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">About You</label>
                       <textarea name="about" class="form-control" placeholder="Tell me about yourself"></textarea>
                    </div>

                    <input type="hidden" name="type" value="one">

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
{{--                <button type="button" class="btn btn-primary">Save changes</button>--}}
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Group Consultation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    Price - {{number_format($group->amount)}}
                </p>
                <hr>
                <form action="{{url('post/group')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Group Name</label>
                        <input type="text" class="form-control" name="name" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Full Name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Email Address</label>
                        <input type="email" name="email" class="form-control" id="exampleInputPassword1" placeholder="Email Address">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Phone Number</label>
                        <input type="text" name="phone" class="form-control" id="exampleInputPassword1" placeholder="Phone Number">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">How many people are in your group</label>
                        <input type="mumber" name="count" class="form-control" id="exampleInputPassword1" placeholder="Number of People in your group">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">About You</label>
                        <textarea name="about" class="form-control" placeholder="Tell me about your group"></textarea>
                    </div>

                    <input type="hidden" name="type" value="group">

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{--                <button type="button" class="btn btn-primary">Save changes</button>--}}
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Public Speaking</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
{{--                    Price (within Ilorin) - {{number_format($public_ilorin->amount)}}<br>--}}
                    Price (National) - NGN {{number_format($public_outside->amount)}}<br>
                    Price (International) - $ {{number_format($public_intl->amount)}}<br>
                    <small>Note: Logistic for physical events  not included</small>
                </p>
                <hr>
                <form action="{{url('post/public')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Group Name</label>
                        <input type="text" class="form-control" name="name" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Full Name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Email Address</label>
                        <input type="email" name="email" class="form-control" id="exampleInputPassword1" placeholder="Email Address">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Phone Number</label>
                        <input type="text" name="phone" class="form-control" id="exampleInputPassword1" placeholder="Phone Number">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Location</label>
                       <select class="form-control" name="location">
{{--                           <option value="ilorin">Within Ilorin</option>--}}
                           <option value="outside">National</option>
                           <option value="int">International</option>
                       </select>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">About You</label>
                        <textarea name="about" class="form-control" placeholder="Tell me about your group"></textarea>
                    </div>

                    <input type="hidden" name="type" value="public">

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{--                <button type="button" class="btn btn-primary">Save changes</button>--}}
            </div>
        </div>
    </div>
</div>
