<!doctype html>

<html class="no-js" lang="zxx">

<head>

    <!-- metas -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Yemisi Adeyeye">
    <meta name="keywords" content="portfolio, personal, corporate, business, parallax, creative, agency">

    <!-- title -->
    <title>Work With Me | Yemisi Adeyeye</title>

    <!-- favicon -->
    <link href="img/favicon.ico" rel="icon" type="image/png">

    <!-- bootstrap css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet">

    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- magnific-popup CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">

    <!-- animate.min CSS -->
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- Font Icon Core CSS -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/et-line.css">

    <!-- Core Style Css -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
    <link rel="stylesheet" href="fluid-gallery.css">


    <!--[if lt IE 9]-->
    <script src="js/html5shiv.min.js"></script>
    <!--[endif]-->

    <style>
        .circle-icon {
            background: purple;
            color: white;
            padding:8px;
            border-radius: 50%;
        }
    </style>

</head>

<body>

<!-- ====== Preloader ======  -->
<div class="loading">
    <div class="load-circle">
    </div>
</div>
<!-- ======End Preloader ======  -->
@include('landing.nav')

<!--====== Portfolio ======-->
<section class="portfolio bg-gray section-padding pb-70" data-scroll-index="3">
    <div class="container">
        <center><h2>Gallery</h2></center>


        <div class="tz-gallery">
            <h4>Year 2016</h4>
            <div class="row">
                @foreach($gallery as $g)
                    @if($g->year->date == '2016' && $g->year->type == 'gallery')
                        <div class="col-sm-6 col-md-4">
                            <a class="lightbox" href="{{$g->image}}">
                                <img src="{{$g->image}}" alt="Park">
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>

            <hr>
            <h4>Year 2017</h4>
            <div class="row">
                @foreach($gallery as $g)
                    @if($g->year->date == '2017' && $g->year->type == 'gallery')
                        <div class="col-sm-6 col-md-4">
                            <a class="lightbox" href="{{$g->image}}">
                                <img src="{{$g->image}}" alt="Park">
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>

            <hr>
            <h4>Year 2018</h4>
            <div class="row">
                @foreach($gallery as $g)
                    @if($g->year->date == '2018' && $g->year->type == 'gallery')
                        <div class="col-sm-6 col-md-4">
                            <a class="lightbox" href="{{$g->image}}">
                                <img src="{{$g->image}}" alt="Park">
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>

            <hr>
            <h4>Year 2019</h4>
            <div class="row">
                @foreach($gallery as $g)
                    @if($g->year->date == '2019' && $g->year->type == 'gallery')
                        <div class="col-sm-6 col-md-4">
                            <a class="lightbox" href="{{$g->image}}">
                                <img src="{{$g->image}}" alt="Park">
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>

            <hr>
            <h4>Year 2020</h4>
            <div class="row">
                @foreach($gallery as $g)
                    @if($g->year->date == '2020' && $g->year->type == 'gallery')
                        <div class="col-sm-6 col-md-4">
                            <a class="lightbox" href="{{$g->image}}">
                                <img src="{{$g->image}}" alt="Park">
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>

        </div>


    </div><!-- /container -->
</section>


<!--====== Portfolio ======-->
<section class="portfolio section-padding pb-70" data-scroll-index="3">
    <div class="container">
        <center><h2>Collaboration</h2></center>
        <div class="tz-gallery">

            <div class="row">
                    @foreach($gallery as $g)
                        @if($g->year->type == 'collaboration')
                            <div class="col-sm-6 col-md-4">
                                <a class="lightbox" href="{{$g->image}}">
                                    <img src="{{$g->image}}" alt="Park">
                                </a>
                            </div>
                        @endif
                    @endforeach
            </div>

        </div>

    </div><!-- /container -->
</section>


<section class="portfolio bg-gray section-padding pb-70" data-scroll-index="3">
    <div class="container">
        <center><h2>Award and Certification Organisations</h2></center>
        <div class="tz-gallery">

            <div class="row">
                @foreach($gallery as $g)
                    @if($g->year->type == 'award')
                        <div class="col-sm-6 col-md-4">
                            <a class="lightbox" href="{{$g->image}}">
                                <img src="{{$g->image}}" alt="Park">
                            </a>
                        </div>
                    @endif
                @endforeach

            </div>

        </div>

    </div><!-- /container -->
</section>



{{--            <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">--}}

{{--                <div class="tz-gallery">--}}

{{--                    <div class="row">--}}
{{--                        <div class="col-sm-6 col-md-4">--}}
{{--                            <a class="lightbox" href="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(132).jpg">--}}
{{--                                <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(132).jpg" alt="Park">--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        <div class="col-sm-6 col-md-4">--}}
{{--                            <a class="lightbox" href="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(115).jpg">--}}
{{--                                <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(115).jpg" alt="Bridge">--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        <div class="col-sm-12 col-md-4">--}}
{{--                            <a class="lightbox" href="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(123).jpg">--}}
{{--                                <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(123).jpg" alt="Tunnel">--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        <div class="col-sm-6 col-md-4">--}}
{{--                            <a class="lightbox" href="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(123).jpg">--}}
{{--                                <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(123).jpg" alt="Coast">--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        <div class="col-sm-6 col-md-4">--}}
{{--                            <a class="lightbox" href="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(123).jpg">--}}
{{--                                <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(123).jpg" alt="Rails">--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        <div class="col-sm-6 col-md-4">--}}
{{--                            <a class="lightbox" href="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(123).jpg">--}}
{{--                                <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(123).jpg" alt="Traffic">--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        --}}{{--                        <div class="col-sm-6 col-md-4">--}}
{{--                        --}}{{--                            <a class="lightbox" href="../images/rocks.jpg">--}}
{{--                        --}}{{--                                <img src="../images/rocks.jpg" alt="Rocks">--}}
{{--                        --}}{{--                            </a>--}}
{{--                        --}}{{--                        </div>--}}
{{--                        --}}{{--                        <div class="col-sm-6 col-md-4">--}}
{{--                        --}}{{--                            <a class="lightbox" href="../images/benches.jpg">--}}
{{--                        --}}{{--                                <img src="../images/benches.jpg" alt="Benches">--}}
{{--                        --}}{{--                            </a>--}}
{{--                        --}}{{--                        </div>--}}
{{--                        --}}{{--                        <div class="col-sm-6 col-md-4">--}}
{{--                        --}}{{--                            <a class="lightbox" href="../images/sky.jpg">--}}
{{--                        --}}{{--                                <img src="../images/sky.jpg" alt="Sky">--}}
{{--                        --}}{{--                            </a>--}}
{{--                        --}}{{--                        </div>--}}
{{--                    </div>--}}

{{--                </div>--}}


{{--            </div>--}}
{{--        </div>--}}







{{--        --}}
{{--            <div class="row">--}}
{{--                <!-- section heading -->--}}
{{--                <div class="section-head">--}}
{{--                    <h3>Events</h3>--}}
{{--                </div>--}}
{{--                <h5><i>Gallery</i></h5>--}}
{{--                <ul class="nav nav-tabs" id="myTab" role="tablist">--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">2016</a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">2017</a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" id="messages-tab" data-toggle="tab" href="#messages" role="tab" aria-controls="messages" aria-selected="false">2018</a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" id="settings-tab" data-toggle="tab" href="#settings" role="tab" aria-controls="settings" aria-selected="false">2019</a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" id="settings-tab-1" data-toggle="tab" href="#settings-1" role="tab" aria-controls="settings" aria-selected="false">2020</a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--                <div class="tab-content">--}}
{{--                    <div class="tab-pane active" id="home" role="tabpanel" aria-labelledby="home-tab">--}}
{{--                    <br>--}}
{{--                        <div class="gallery text-center">--}}

{{--                            <!-- gallery item -->--}}
{{--                            @foreach($gallery as $g)--}}
{{--                                @if($g->year->date == '2016' && $g->year->type == 'gallery')--}}
{{--                                    <div class="col-md-4 col-sm-6">--}}
{{--                                        <div class="item-img">--}}
{{--                                            <img src="{{$g->image}}" alt="image">--}}
{{--                                            <div class="item-img-overlay">--}}
{{--                                                <div class="overlay-info v-middle text-center">--}}
{{--                                                    <h6 class="sm-titl">{{$g->year->name}} | {{$g->year->date}}</h6>--}}
{{--                                                    <div class="icons">--}}
{{--                                            <span class="icon">--}}
{{--                                                <a href="#0">--}}
{{--                                                    <i class="fa fa-chain-broken" aria-hidden="true"></i>--}}
{{--                                                </a>--}}
{{--                                            </span>--}}
{{--                                                        <span class="icon link">--}}
{{--                                                <a href="{{$g->image}}">--}}
{{--                                                    <i class="fa fa-search-plus" aria-hidden="true"></i>--}}
{{--                                                </a>--}}
{{--                                            </span>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                @endif--}}

{{--                            @endforeach--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                    <div class="tab-pane" id="profile" role="tabpanel" aria-labelledby="profile-tab">--}}
{{--                        <br>--}}
{{--                        <div class="gallery text-center">--}}

{{--                            <!-- gallery item -->--}}
{{--                            @foreach($gallery as $g)--}}
{{--                                @if($g->year->date == '2017' && $g->year->type == 'gallery')--}}
{{--                                    <div class="col-md-4 col-sm-6 {{$g->name}}">--}}
{{--                                        <div class="item-img">--}}
{{--                                            <img src="{{$g->image}}" alt="image">--}}
{{--                                            <div class="item-img-overlay">--}}
{{--                                                <div class="overlay-info v-middle text-center">--}}
{{--                                                    <h6 class="sm-titl">{{$g->name}} | {{$g->name}}</h6>--}}
{{--                                                    <div class="icons">--}}
{{--                                            <span class="icon">--}}
{{--                                                <a href="#0">--}}
{{--                                                    <i class="fa fa-chain-broken" aria-hidden="true"></i>--}}
{{--                                                </a>--}}
{{--                                            </span>--}}
{{--                                                        <span class="icon link">--}}
{{--                                                <a href="{{$g->image}}">--}}
{{--                                                    <i class="fa fa-search-plus" aria-hidden="true"></i>--}}
{{--                                                </a>--}}
{{--                                            </span>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                @endif--}}

{{--                            @endforeach--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="tab-pane" id="messages" role="tabpanel" aria-labelledby="messages-tab">--}}
{{--                        <br>--}}
{{--                        <div class="gallery text-center">--}}
{{--                            <!-- gallery item -->--}}
{{--                            @foreach($gallery as $g)--}}
{{--                                @if($g->year->date == '2018' && $g->year->type == 'gallery')--}}
{{--                                    <div class="col-md-4 col-sm-6 {{$g->name}}">--}}
{{--                                        <div class="item-img">--}}
{{--                                            <img src="{{$g->image}}" alt="image">--}}
{{--                                            <div class="item-img-overlay">--}}
{{--                                                <div class="overlay-info v-middle text-center">--}}
{{--                                                    <h6 class="sm-titl">{{$g->name}} | {{$g->name}}</h6>--}}
{{--                                                    <div class="icons">--}}
{{--                                            <span class="icon">--}}
{{--                                                <a href="#0">--}}
{{--                                                    <i class="fa fa-chain-broken" aria-hidden="true"></i>--}}
{{--                                                </a>--}}
{{--                                            </span>--}}
{{--                                                        <span class="icon link">--}}
{{--                                                <a href="{{$g->image}}">--}}
{{--                                                    <i class="fa fa-search-plus" aria-hidden="true"></i>--}}
{{--                                                </a>--}}
{{--                                            </span>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                @endif--}}

{{--                            @endforeach--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="tab-pane" id="settings" role="tabpanel" aria-labelledby="settings-tab">--}}
{{--                        <br>--}}
{{--                        <div class="gallery text-center">--}}

{{--                            <!-- gallery item -->--}}
{{--                            @foreach($gallery as $g)--}}
{{--                                @if($g->year->date == '2019' && $g->year->type == 'gallery')--}}
{{--                                    <div class="col-md-4 col-sm-6 {{$g->name}}">--}}
{{--                                        <div class="item-img">--}}
{{--                                            <img src="{{$g->image}}" alt="image">--}}
{{--                                            <div class="item-img-overlay">--}}
{{--                                                <div class="overlay-info v-middle text-center">--}}
{{--                                                    <h6 class="sm-titl">{{$g->name}} | {{$g->name}}</h6>--}}
{{--                                                    <div class="icons">--}}
{{--                                            <span class="icon">--}}
{{--                                                <a href="#0">--}}
{{--                                                    <i class="fa fa-chain-broken" aria-hidden="true"></i>--}}
{{--                                                </a>--}}
{{--                                            </span>--}}
{{--                                                        <span class="icon link">--}}
{{--                                                <a href="{{$g->image}}">--}}
{{--                                                    <i class="fa fa-search-plus" aria-hidden="true"></i>--}}
{{--                                                </a>--}}
{{--                                            </span>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                @endif--}}

{{--                            @endforeach--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="tab-pane" id="settings-1" role="tabpanel" aria-labelledby="settings-tab-1">--}}
{{--                        <br>--}}
{{--                        <div class="gallery text-center">--}}

{{--                            <!-- gallery item -->--}}
{{--                            @foreach($gallery as $g)--}}
{{--                                @if($g->year->date == '2020' && $g->year->type == 'gallery')--}}
{{--                                    <div class="col-md-4 col-sm-6 {{$g->name}}">--}}
{{--                                        <div class="item-img">--}}
{{--                                            <img src="{{$g->image}}" alt="image">--}}
{{--                                            <div class="item-img-overlay">--}}
{{--                                                <div class="overlay-info v-middle text-center">--}}
{{--                                                    <h6 class="sm-titl">{{$g->name}} | {{$g->name}}</h6>--}}
{{--                                                    <div class="icons">--}}
{{--                                            <span class="icon">--}}
{{--                                                <a href="#0">--}}
{{--                                                    <i class="fa fa-chain-broken" aria-hidden="true"></i>--}}
{{--                                                </a>--}}
{{--                                            </span>--}}
{{--                                                        <span class="icon link">--}}
{{--                                                <a href="{{$g->image}}">--}}
{{--                                                    <i class="fa fa-search-plus" aria-hidden="true"></i>--}}
{{--                                                </a>--}}
{{--                                            </span>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                @endif--}}

{{--                            @endforeach--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <script>--}}
{{--                    $(function () {--}}
{{--                        $('#myTab li:last-child a').tab('show')--}}
{{--                    })--}}
{{--                </script>--}}
{{--            </div><!-- /row -->--}}
{{--            <hr>--}}
{{--                <h5><i>Collaboration</i></h5>--}}
{{--        <br>--}}
{{--        <div class="gallery text-center">--}}

{{--            <!-- gallery item -->--}}
{{--            @foreach($gallery as $g)--}}
{{--                @if($g->year->type == 'collaboration')--}}
{{--                    <div class="col-md-4 col-sm-6 {{$g->name}}">--}}
{{--                        <div class="item-img">--}}
{{--                            <img src="{{$g->image}}" alt="image">--}}
{{--                            <div class="item-img-overlay">--}}
{{--                                <div class="overlay-info v-middle text-center">--}}
{{--                                    <h6 class="sm-titl">{{$g->name}} | {{$g->name}}</h6>--}}
{{--                                    <div class="icons">--}}
{{--                                            <span class="icon">--}}
{{--                                                <a href="#0">--}}
{{--                                                    <i class="fa fa-chain-broken" aria-hidden="true"></i>--}}
{{--                                                </a>--}}
{{--                                            </span>--}}
{{--                                        <span class="icon link">--}}
{{--                                                <a href="{{$g->image}}">--}}
{{--                                                    <i class="fa fa-search-plus" aria-hidden="true"></i>--}}
{{--                                                </a>--}}
{{--                                            </span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endif--}}

{{--            @endforeach--}}
{{--        </div>--}}
{{--        <hr>--}}
{{--        <br>--}}
{{--        <h5><i>Awards</i></h5>--}}
{{--        <br>--}}
{{--        <div class="gallery text-center">--}}

{{--            <!-- gallery item -->--}}
{{--            @foreach($gallery as $g)--}}
{{--                @if($g->year->type == 'award')--}}
{{--                    <div class="col-md-4 col-sm-6 {{$g->name}}">--}}
{{--                        <div class="item-img">--}}
{{--                            <img src="{{$g->image}}" alt="image">--}}
{{--                            <div class="item-img-overlay">--}}
{{--                                <div class="overlay-info v-middle text-center">--}}
{{--                                    <h6 class="sm-titl">{{$g->name}} | {{$g->name}}</h6>--}}
{{--                                    <div class="icons">--}}
{{--                                            <span class="icon">--}}
{{--                                                <a href="#0">--}}
{{--                                                    <i class="fa fa-chain-broken" aria-hidden="true"></i>--}}
{{--                                                </a>--}}
{{--                                            </span>--}}
{{--                                        <span class="icon link">--}}
{{--                                                <a href="{{$g->image}}">--}}
{{--                                                    <i class="fa fa-search-plus" aria-hidden="true"></i>--}}
{{--                                                </a>--}}
{{--                                        </span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    @endif--}}

{{--            @endforeach--}}
{{--        </div>--}}
{{--    --}}


{{--    </div><!-- /container -->--}}
{{--</section>--}}
<!--====== End Portfolio ======-->

@include('landing.footer');


<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.tz-gallery');
</script>
