
<!-- ====== Navgition ======  -->
<nav class="navbar navbar-default blog-nav navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-icon-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- logo -->
            <a class="logo" href="{{url('/')}}"> <img src="{{asset('img/logo.png')}}"></a>

        </div>

        <!-- Collect the nav links, and other content for toggling -->
        <div class="collapse navbar-collapse" id="nav-icon-collapse">
            <!-- links -->
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{url('/')}}" style="font-size: 17px;">Home</a></li>
                <li><a href="{{url('/get-my-book')}}" style="font-size: 17px">Get My Book</a></li>
                <li><a href="{{url('my-profile')}}" style="font-size: 17px">My Profile</a></li>
                <li><a href="{{url('work-with-me')}}" style="font-size: 17px">Work with Me</a></li>
                <li><a href="{{url('blog')}}" style="font-size: 17px">Blog</a></li>

                <li><a href="https://web.facebook.com/yemisi.adeyeye.5" target="_blank">
                        <span><i class="fa fa-facebook circle-icon" aria-hidden="true"></i></span>
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com/drroadeyeye?lang=en" target="_blank">
                        <span><i class="fa fa-twitter circle-icon" aria-hidden="true"></i></span>
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/yemisiadeyeye/?hl=en" target="_blank">
                        <span><i class="fa fa-instagram circle-icon" aria-hidden="true"></i></span>
                    </a>
                </li>
                <li>
                    <a href="https://ng.linkedin.com/in/yemisi-adeyeye-5b0b96120" target="_blank">
                        <span><i class="fa fa-linkedin circle-icon" aria-hidden="true"></i></span>
                    </a>
                </li>
                <li>
                    <a href="https://wa.me/2349094407502" target="_blank">
                        <span><i class="fa fa-whatsapp circle-icon" aria-hidden="true"></i></span>
                    </a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container -->
</nav>
