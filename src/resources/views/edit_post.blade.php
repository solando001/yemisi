<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title> Admin Page | Yemisi Adeyeye </title>
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="{{asset('res/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('res/assets/css/plugins.css')}}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link href="{{asset('res/assets/css/users/user-profile.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('res/plugins/apex/apexcharts.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('res/assets/css/dashboard/dash_2.css')}}" rel="stylesheet" type="text/css" />

    <!--  END CUSTOM STYLE FILE  -->

    <script>
        $(document).ready(function(){
            $('.launch-modal').click(function(){
                $('#exampleModal').modal({
                    backdrop: 'static'
                });
            });
        });
    </script>
    <style>
        .bs-example{
            margin: 20px;
        }
    </style>

    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <script>
        tinymce.init({
            selector: '#mytextarea'
        });
    </script>
</head>
<body>

<!--  BEGIN NAVBAR  -->
@include('layouts.head-nav')
<!--  END NAVBAR  -->

<!--  BEGIN NAVBAR  -->
<div class="sub-header-container">
    <header class="header navbar navbar-expand-sm">
        <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

        <ul class="navbar-nav flex-row">
            <li>
                <div class="page-header">

                    <nav class="breadcrumb-one" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Admin</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><span>Profile</span></li>
                        </ol>
                    </nav>

                </div>
            </li>
        </ul>
    </header>
</div>
<!--  END NAVBAR  -->

<!--  BEGIN MAIN CONTAINER  -->
<div class="main-container" id="container">

    <div class="overlay"></div>
    <div class="search-overlay"></div>

    <!--  BEGIN SIDEBAR  -->
@include('layouts.sidebar')
<!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT AREA  -->
    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="row layout-spacing">

                @include('home-side')

                <div class="col-xl-8 col-lg-6 col-md-7 col-sm-12 layout-top-spacing">
                    @include('home-head')
                    <br>


                    {{--                      <div class="skills layout-spacing ">--}}
                    {{--                        <div class="widget-content widget-content-area">--}}
                    {{--                            <h3 class="">Skills</h3>--}}
                    {{--                            <div class="progress br-30">--}}
                    {{--                                <div class="progress-bar bg-primary" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><div class="progress-title"><span>PHP</span> <span>25%</span> </div></div>--}}
                    {{--                            </div>--}}
                    {{--                            <div class="progress br-30">--}}
                    {{--                                <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><div class="progress-title"><span>Wordpress</span> <span>50%</span> </div></div>--}}
                    {{--                            </div>--}}
                    {{--                            <div class="progress br-30">--}}
                    {{--                                <div class="progress-bar bg-primary" role="progressbar" style="width: 70%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><div class="progress-title"><span>Javascript</span> <span>70%</span> </div></div>--}}
                    {{--                            </div>--}}
                    {{--                            <div class="progress br-30">--}}
                    {{--                                <div class="progress-bar bg-primary" role="progressbar" style="width: 60%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><div class="progress-title"><span>jQuery</span> <span>60%</span> </div></div>--}}
                    {{--                            </div>--}}

                    {{--                        </div>--}}
                    {{--                    </div>--}}

                    <div class="bio layout-spacing ">
                        <div class="widget-content widget-content-area">
                            <div class="col-md-12 text-right mb-2">
                                <button id="add-work-exp" class="btn btn-success" data-toggle="modal" data-target="#exampleModal2">Create Category</button>
                                <button id="add-work-exp" class="btn btn-success" data-toggle="modal" data-target="#exampleModal1">Create Coupon</button>
                                <button id="add-work-exp" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Add Book</button>
                            </div>

                            <h3 class="">Edit Blog Post</h3>
                            @if(session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif

                            @if(session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif

                            <div class="bio-skill-box">

                                <form action="{{url('edit/blog')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Title</label>
                                        <input type="text" name="title" class="form-control" id="exampleFormControlInput1"  value="{{$post->title}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Categories</label>
                                        <select class="form-control" name="cat_id" id="exampleFormControlSelect1">
                                            <option value="1">Life</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Content</label>
                                        <textarea class="form-control" name="content" id="mytextarea" rows="10">{{$post->content}}</textarea>
                                    </div>

                                    <input type="hidden" name="slug" value="{{$post->slug}}">

                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Post Image</label>
                                        <input type="file" class="form-control" name="image">
                                    </div>

                                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                    <button  class="btn btn-success" type="submit">Create Post</button>
                                </form>

                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
        <div class="footer-wrapper">
            <div class="footer-section f-section-1">
                <p class="">Copyright © <?php echo date('Y') ?> <a target="_blank" href="https://designreset.com">Yemisi Adeyeye</a>, All rights reserved.</p>
            </div>
            <div class="footer-section f-section-2">
                {{--                <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg></p>--}}
            </div>
        </div>
    </div>
    <!--  END CONTENT AREA  -->
</div>
<!-- END MAIN CONTAINER -->

<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="{{asset('res/assets/js/libs/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('res/bootstrap/js/popper.min.js')}}"></script>
<script src="{{asset('res/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('res/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{asset('res/assets/js/app.js')}}"></script>

<script>
    $(document).ready(function() {
        App.init();
    });
</script>

<script src="{{asset('res/plugins/apex/apexcharts.min.js')}}"></script>
<script src="{{asset('res/assets/js/dashboard/dash_2.js')}}"></script>
<script src="res/assets/js/custom.js"></script>
<!-- END GLOBAL MANDATORY SCRIPTS -->
</body>
</html>

{{-- Upload Books--}}
<div class="modal fade" data-keyboard="false" data-backdrop="static" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Book</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form method="POST" action="{{ url('upload/book') }}" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group row">

                        <div class="col-md-12">
                            <label for="email">Name</label>
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autofocus>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label for="email">Price</label>
                            <input id="price" type="number" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ old('price') }}">

                            @error('price')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label for="email">Description</label>
                            {{--                                 <input id="descriptio" type="number" class="form-control @error('price') is-invalid @enderror" name="price"  required>--}}
                            <textarea id="description" class="form-control @error('description') is-invalid @enderror" name="description" >{{ old('description') }}</textarea>
                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label for="email">Image</label>
                            <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image">

                            @error('image')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                    </div>

                    <button type="submit" class="btn btn-primary">Save Book</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

{{-- Create Coupon --}}
<div class="modal fade" data-keyboard="false" data-backdrop="static" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create Coupon</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form method="POST" action="{{ url('create/coupon') }}" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group row">

                        <div class="col-md-12">
                            <label for="email">Enter Coupon Percentage</label>
                            <input id="percentage" type="text" class="form-control @error('percentage') is-invalid @enderror" name="percentage" value="{{ old('percentage') }}" autofocus required>

                            @error('percentage')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $percentage }}</strong>
                                    </span>
                            @enderror
                        </div>

                    </div>

                    <button type="submit" class="btn btn-primary">Generate Code</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
