{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
{{--    <div class="row justify-content-center">--}}
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Login') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    <form method="POST" action="{{ route('login') }}">--}}
{{--                        @csrf--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>--}}

{{--                                @error('email')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">--}}

{{--                                @error('password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <div class="form-check">--}}
{{--                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

{{--                                    <label class="form-check-label" for="remember">--}}
{{--                                        {{ __('Remember Me') }}--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row mb-0">--}}
{{--                            <div class="col-md-8 offset-md-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    {{ __('Login') }}--}}
{{--                                </button>--}}

{{--                                @if (Route::has('password.request'))--}}
{{--                                    <a class="btn btn-link" href="{{ route('password.request') }}">--}}
{{--                                        {{ __('Forgot Your Password?') }}--}}
{{--                                    </a>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--@endsection--}}


{{--<!DOCTYPE html>--}}
{{--<html lang="en">--}}
{{--<head>--}}
{{--    <meta charset="UTF-8">--}}
{{--    <meta http-equiv="X-UA-Compatible" content="IE=edge">--}}
{{--    <meta name="viewport" content="width=device-width, initial-scale=1">--}}
{{--    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->--}}

{{--    <title>VoiceOut | Login</title>--}}

{{--    <!-- Favicon -->--}}
{{--    <link rel="icon" href="images/favicon.png">--}}
{{--    <!-- Bootstrap CSS File -->--}}
{{--    <link rel="stylesheet" href="css/bootstrap.min.css">--}}
{{--    <!-- Font Awesome -->--}}
{{--    <link rel="stylesheet" href="css/themify-icons.css">--}}
{{--    <!-- Animate CSS File -->--}}
{{--    <link rel="stylesheet" href="css/animate.min.css">--}}
{{--    <!-- Cube Portfolio CSS File -->--}}
{{--    <link rel="stylesheet" href="css/cubeportfolio.min.css">--}}
{{--    <!-- Fancy Box CSS File -->--}}
{{--    <link rel="stylesheet" href="css/jquery.fancybox.min.css">--}}
{{--    <!-- Revolution Slider CSS Files -->--}}
{{--    <link rel="stylesheet" href="css/navigation.css">--}}
{{--    <link rel="stylesheet" href="css/settings.css">--}}
{{--    <!-- Swiper CSS File -->--}}
{{--    <link rel="stylesheet" href="css/swiper.min.css">--}}
{{--    <!-- Owl Carousel CSS Files -->--}}
{{--    <link rel="stylesheet" href="css/owl.carousel.min.css">--}}
{{--    <link rel="stylesheet" href="css/owl.theme.default.min.css">--}}
{{--    <!-- Slick CSS Files -->--}}
{{--    <link rel="stylesheet" href="css/slick.css">--}}
{{--    <link rel="stylesheet" href="css/slick-theme.css">--}}
{{--    <!-- Style CSS File -->--}}
{{--    <link rel="stylesheet" href="css/style.css">--}}
{{--    <!-- Custom Style CSS File -->--}}
{{--    <link rel="stylesheet" href="css/custom.css">--}}

{{--</head>--}}
{{--<body data-spy="scroll" data-target=".navbar" data-offset="90">--}}

{{--<!-- Loader -->--}}
{{--<div class="loader" id="loader-fade">--}}
{{--    <div class="loader-container center-block">--}}
{{--        <div class="grid-row">--}}
{{--            <div class="col center-block">--}}
{{--                <ul class="loading reversed">--}}
{{--                    <li></li>--}}
{{--                    <li></li>--}}
{{--                    <li></li>--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--<!-- Loader ends -->--}}

{{--<!-- Login -->--}}
{{--<section class="p-lg-0 login-sec">--}}
{{--    <div class="container-fluid">--}}

{{--        <div class="row align-items-center">--}}
{{--            <div class="col-lg-6 order-lg-2">--}}
{{--                <div class="login-content">--}}
{{--                    <div class="main-title d-inline-block mb-4 text-md-left">--}}
{{--                        <h5 class="mb-3"> <a href="{{url('/')}}"> <img src="images/logo-black.png" alt="logo" title="Logo"> </a></h5>--}}
{{--                        <h3 class="mb-3 color-black">Get more things, done with Login platform.</h3>--}}
{{--                        <p>Access to the most powerful tool in the entire design and web industry.</p>--}}
{{--                    </div>--}}
{{--                    <!--Alert-->--}}
{{--                    <div class="alert alert-warning alert-dismissible fade show with-icon" role="alert">--}}
{{--                        Please fill the following form with your information--}}
{{--                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">--}}
{{--                            <span aria-hidden="true">×</span>--}}
{{--                        </button>--}}
{{--                    </div>--}}

{{--                    <!--form-->--}}
{{--                    <form method="POST" action="{{ route('login') }}">--}}
{{--                        @csrf--}}
{{--                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email Address" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>--}}
{{--                        @error('email')--}}
{{--                        <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                        @enderror--}}

{{--                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" required autocomplete="current-password">--}}
{{--                        @error('password')--}}
{{--                        <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                        @enderror--}}
{{--                        <div class="form-button mt-40px">--}}
{{--                            <button type="submit" class="btn-setting btn-hvr-setting-main btn-yellow btn-hvr text-uppercase" id="submit_btn">Login--}}
{{--                                <span class="btn-hvr-setting btn-hvr-pink">--}}
{{--                                     <span class="btn-hvr-setting-inner">--}}
{{--                                     <span class="btn-hvr-effect"></span>--}}
{{--                                     <span class="btn-hvr-effect"></span>--}}
{{--                                     <span class="btn-hvr-effect"></span>--}}
{{--                                     <span class="btn-hvr-effect"></span>--}}
{{--                                     </span>--}}
{{--                                    </span>--}}
{{--                            </button>--}}
{{--                            <a href="{{url('/register')}}">Not yet Registered?</a>--}}
{{--                        </div>--}}
{{--                    </form>--}}

{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-lg-6 d-none d-lg-block p-0">--}}
{{--                <!--Feature Image Half-->--}}
{{--                <img src="images/login.jpg" class="about-img" alt="image">--}}
{{--            </div>--}}
{{--        </div>--}}


{{--    </div>--}}
{{--</section>--}}
{{--<!-- Login ends -->--}}


{{--<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->--}}
{{--<script src="js/jquery-3.3.1.min.js"></script>--}}
{{--<!-- Bootstrap JS File -->--}}
{{--<script src="js/popper.min.js"></script>--}}
{{--<script src="js/bootstrap.min.js"></script>--}}
{{--<!-- Appear JS File -->--}}
{{--<script src="js/jquery.appear.js"></script>--}}
{{--<!-- Isotop gallery -->--}}
{{--<script src="js/isotope.pkgd.min.js"></script>--}}
{{--<!-- Morphtext JS File -->--}}
{{--<script src="js/morphext.min.js"></script>--}}
{{--<!-- Cube Portfolio JS File -->--}}
{{--<script src="js/jquery.cubeportfolio.min.js"></script>--}}
{{--<!-- Equal Height JS File -->--}}
{{--<script src="js/jquery.matchHeight-min.js"></script>--}}
{{--<!--Parallax Background-->--}}
{{--<script src="js/parallaxie.min.js"></script>--}}
{{--<!-- Fancy Box JS File -->--}}
{{--<script src="js/jquery.fancybox.min.js"></script>--}}
{{--<!-- Slick JS File -->--}}
{{--<script src="js/slick.min.js"></script>--}}
{{--<!-- Swiper JS File -->--}}
{{--<script src="js/swiper.min.js"></script>--}}
{{--<!-- Owl Carousel JS File -->--}}
{{--<script src="js/owl.carousel.js"></script>--}}
{{--<!-- Wow JS File -->--}}
{{--<script src="js/wow.js"></script>--}}


{{--<!--Revolution Slider-->--}}
{{--<script src="js/revolution/jquery.themepunch.tools.min.js"></script>--}}
{{--<script src="js/revolution/jquery.themepunch.revolution.min.js"></script>--}}
{{--<script src="js/revolution/jquery.themepunch.revolution.contdown.min.js"></script>--}}

{{--<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->--}}
{{--<script src="js/revolution/extensions/revolution.extension.actions.min.js"></script>--}}
{{--<script src="js/revolution/extensions/revolution.extension.carousel.min.js"></script>--}}
{{--<script src="js/revolution/extensions/revolution.extension.kenburn.min.js"></script>--}}
{{--<script src="js/revolution/extensions/revolution.extension.layeranimation.min.js"></script>--}}
{{--<script src="js/revolution/extensions/revolution.extension.migration.min.js"></script>--}}
{{--<script src="js/revolution/extensions/revolution.extension.navigation.min.js"></script>--}}
{{--<script src="js/revolution/extensions/revolution.extension.parallax.min.js"></script>--}}
{{--<script src="js/revolution/extensions/revolution.extension.slideanims.min.js"></script>--}}
{{--<script src="js/revolution/extensions/revolution.extension.video.min.js"></script>--}}

{{--<!-- Custom JS File -->--}}
{{--<script src="js/functions.js"></script>--}}
{{--</body>--}}
{{--</html>--}}



{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
{{--    <div class="row justify-content-center">--}}
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Login') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    <form method="POST" action="{{ route('login') }}">--}}
{{--                        @csrf--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>--}}

{{--                                @error('email')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">--}}

{{--                                @error('password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <div class="form-check">--}}
{{--                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

{{--                                    <label class="form-check-label" for="remember">--}}
{{--                                        {{ __('Remember Me') }}--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row mb-0">--}}
{{--                            <div class="col-md-8 offset-md-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    {{ __('Login') }}--}}
{{--                                </button>--}}

{{--                                @if (Route::has('password.request'))--}}
{{--                                    <a class="btn btn-link" href="{{ route('password.request') }}">--}}
{{--                                        {{ __('Forgot Your Password?') }}--}}
{{--                                    </a>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--@endsection--}}


    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>Login | Yemisi Adeyeye </title>
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="res/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="res/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="res/assets/css/authentication/form-1.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="res/assets/css/forms/theme-checkbox-radio.css">
    <link rel="stylesheet" type="text/css" href="res/assets/css/forms/switches.css">
</head>
<body class="form">


<div class="form-container">
    <div class="form-form">
        <div class="form-form-wrap">
            <div class="form-container">
                <div class="form-content">

                    <h1 class="">Log In <a href="{{url('/')}}"><span class="brand-name"></span></a></h1>
                    <p class="signup-link">New Here? <a href="{{url('register')}}">Create an account</a></p>
                    <form class="text-left" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form">

                            <div id="username-field" class="field-wrapper input">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                <input id="email" name="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email Address">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div id="password-field" class="field-wrapper input mb-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock"><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 10 0v4"></path></svg>
                                <input id="password" name="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="d-sm-flex justify-content-between">
                                <div class="field-wrapper toggle-pass">
                                    <p class="d-inline-block">Show Password</p>
                                    <label class="switch s-primary">
                                        <input type="checkbox" id="toggle-password" class="d-none">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="field-wrapper">
                                    <button type="submit" class="btn btn-primary" value="">Log In</button>
                                </div>

                            </div>

                            <div class="field-wrapper text-center keep-logged-in">
                                <div class="n-chk new-checkbox checkbox-outline-primary">
                                    <label class="new-control new-checkbox checkbox-outline-primary">
                                        <input type="checkbox" class="new-control-input" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <span class="new-control-indicator"></span>Keep me logged in
                                    </label>
                                </div>
                            </div>

                            {{--                            <div class="field-wrapper">--}}
                            {{--                                <a href="auth_pass_recovery.html" class="forgot-pass-link">Forgot Password?</a>--}}
                            {{--                            </div>--}}

                        </div>
                    </form>
                    <p class="terms-conditions">© <?php echo date('Y') ?> All Rights Reserved. <a href="{{url('/')}}">Yemisi Adeyeye</a>
                </div>
            </div>
        </div>
    </div>
    <div class="form-image">
        <div class="l-image">
        </div>
    </div>
</div>


<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="res/assets/js/libs/jquery-3.1.1.min.js"></script>
<script src="res/bootstrap/js/popper.min.js"></script>
<script src="res/bootstrap/js/bootstrap.min.js"></script>

<!-- END GLOBAL MANDATORY SCRIPTS -->
<script src="res/assets/js/authentication/form-1.js"></script>

</body>
</html>




