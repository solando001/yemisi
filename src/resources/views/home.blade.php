 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title> Admin Page | Yemisi Adeyeye </title>
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="res/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="res/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link href="res/assets/css/users/user-profile.css" rel="stylesheet" type="text/css" />
    <link href="res/plugins/apex/apexcharts.css" rel="stylesheet" type="text/css">
    <link href="res/assets/css/dashboard/dash_2.css" rel="stylesheet" type="text/css" />

    <!--  END CUSTOM STYLE FILE  -->

    <script>
        $(document).ready(function(){
            $('.launch-modal').click(function(){
                $('#exampleModal').modal({
                    backdrop: 'static'
                });
            });
        });
    </script>
    <style>
        .bs-example{
            margin: 20px;
        }
    </style>
</head>
<body>

<!--  BEGIN NAVBAR  -->
@include('layouts.head-nav')
<!--  END NAVBAR  -->

<!--  BEGIN NAVBAR  -->
<div class="sub-header-container">
    <header class="header navbar navbar-expand-sm">
        <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

        <ul class="navbar-nav flex-row">
            <li>
                <div class="page-header">

                    <nav class="breadcrumb-one" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Admin</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><span>Profile</span></li>
                        </ol>
                    </nav>

                </div>
            </li>
        </ul>
    </header>
</div>
<!--  END NAVBAR  -->

<!--  BEGIN MAIN CONTAINER  -->
<div class="main-container" id="container">

    <div class="overlay"></div>
    <div class="search-overlay"></div>

    <!--  BEGIN SIDEBAR  -->
    @include('layouts.sidebar')
    <!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT AREA  -->
    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="row layout-spacing">

             @include('home-side')

                <div class="col-xl-8 col-lg-6 col-md-7 col-sm-12 layout-top-spacing">
                   @include('home-head')
                    <br>
                    <div class="bio layout-spacing ">
                        <div class="widget-content widget-content-area">

                            <div class="col-md-12 text-right mb-2">
                                <button id="add-work-exp" class="btn btn-success" data-toggle="modal" data-target="#exampleModal2">Create Category</button>
                                <button id="add-work-exp" class="btn btn-success" data-toggle="modal" data-target="#exampleModal1">Create Coupon</button>
                                <button id="add-work-exp" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Add Book</button>
                            </div>


                            <h3 class="">Manage Books</h3>
                            @if(session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif

                            @if(session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif

                            <div class="bio-skill-box">

                                <div class="row">
                                @foreach($books as $book)
                                    <div class="col-12 col-xl-12 col-lg-12 col-md-12 mb-xl-5 mb-5 ">

                                        <div class="d-flex b-skills">
                                            <div>
                                                <img src="{{$book->image}}" height="150" width="150" class="img-responsive" align="Book-image">
                                            </div>
                                            <div class="">
                                                <h5>{{$book->name}}</h5> | &#8358; {{number_format($book->price)}}
                                                <p> {{$book->description}} </p>
                                                <hr>
{{--                                                <p><b>Sales:</b> 1,000</p>--}}
                                            </div>
                                        </div>

                                    </div>
                                @endforeach

{{--                                    <div class="col-12 col-xl-6 col-lg-12 mb-xl-5 mb-5 ">--}}

{{--                                        <div class="d-flex b-skills">--}}
{{--                                            <div>--}}
{{--                                            </div>--}}
{{--                                            <div class="">--}}
{{--                                                <h5>Github Countributer</h5>--}}
{{--                                                <p>Ut enim ad minim veniam, quis nostrud exercitation aliquip ex ea commodo consequat.</p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                    </div>--}}

{{--                                    <div class="col-12 col-xl-6 col-lg-12 mb-xl-0 mb-5 ">--}}

{{--                                        <div class="d-flex b-skills">--}}
{{--                                            <div>--}}
{{--                                            </div>--}}
{{--                                            <div class="">--}}
{{--                                                <h5>Photograhpy</h5>--}}
{{--                                                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia anim id est laborum.</p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                    </div>--}}

{{--                                    <div class="col-12 col-xl-6 col-lg-12 mb-xl-0 mb-0 ">--}}

{{--                                        <div class="d-flex b-skills">--}}
{{--                                            <div>--}}
{{--                                            </div>--}}
{{--                                            <div class="">--}}
{{--                                                <h5>Mobile Apps</h5>--}}
{{--                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do et dolore magna aliqua.</p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                    </div>--}}

                                </div>

                            </div>

                        </div>
                    </div>

                </div>

            </div>

        </div>
        <div class="footer-wrapper">
            <div class="footer-section f-section-1">
                <p class="">Copyright © <?php echo date('Y') ?> <a target="_blank" href="https://designreset.com">Yemisi Adeyeye</a>, All rights reserved.</p>
            </div>
            <div class="footer-section f-section-2">
{{--                <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg></p>--}}
            </div>
        </div>
    </div>
    <!--  END CONTENT AREA  -->
</div>
<!-- END MAIN CONTAINER -->

<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="res/assets/js/libs/jquery-3.1.1.min.js"></script>
<script src="res/bootstrap/js/popper.min.js"></script>
<script src="res/bootstrap/js/bootstrap.min.js"></script>
<script src="res/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="res/assets/js/app.js"></script>

<script>
    $(document).ready(function() {
        App.init();
    });
</script>

<script src="res/plugins/apex/apexcharts.min.js"></script>
<script src="res/assets/js/dashboard/dash_2.js"></script>
<script src="res/assets/js/custom.js"></script>
<!-- END GLOBAL MANDATORY SCRIPTS -->
</body>
</html>


@include('modals')
