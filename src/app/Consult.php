<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consult extends Model
{
    protected $table = "consults";

    protected $fillable = [ 'ref', 'name',  'email', 'phone', 'location', 'type', 'hours', 'count', 'about', 'amount', 'type_id'];
}
