<?php

namespace App\Http\Controllers;

use App\Room;
use App\Voice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    public function create_room(Request $request)
    {
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name = time() . $image->getClientOriginalName();
            $image_extension_name = $image->getClientOriginalExtension();

            if ($image_extension_name == 'jpeg' || $image_extension_name == 'png' || $image_extension_name == 'jpg') {
                $imagefilePath = 'voiceout/' . $image_name;
                Storage::disk('s3')->put($imagefilePath, file_get_contents($image), 'public');
                $url = Storage::disk('s3')->url($imagefilePath);
                //$ref = \Ramsey\Uuid\Uuid::uuid4();

                $user = Auth::user();
                $data['user_id'] = "$user->id";
                $data['url'] = "$url";
                $data['name'] = "$request->name";
                $data['description'] = "$request->description";
                Room::create($data);
                return back()->with(['success' => 'Room Created Successfully']);
            }else{
                return back()->with(['error' => 'Please upload an image of either png, jpg, jpeg!']);
            }
        }else{
            return back()->with(['error' => 'Please upload at least one banner!']);
        }
    }

    public function create_voiceout(Request $request)
    {
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name = time() . $image->getClientOriginalName();
            $image_extension_name = $image->getClientOriginalExtension();

            if ($image_extension_name == 'jpeg' || $image_extension_name == 'png' || $image_extension_name == 'jpg') {
                $imagefilePath = 'voiceout/' . $image_name;
                Storage::disk('s3')->put($imagefilePath, file_get_contents($image), 'public');
                $url = Storage::disk('s3')->url($imagefilePath);
                $user = Auth::user();
                $ref = \Ramsey\Uuid\Uuid::uuid4();
                $data['user_id'] = "$user->id";
                $data['hash'] = "$ref";
                $data['url'] = "$url";
                $data['room_id'] = "$request->room_id";
                $data['title'] = "$request->title";
                $data['description'] = "$request->description";
                $data['status'] = true;
                Voice::create($data);
                return back()->with(['success' => 'VoiceOut Created Successfully']);
            }else{
                return back()->with(['error' => 'Please upload an image of either png, jpg, jpeg!']);
            }
        }else{
            return back()->with(['error' => 'Please upload at least one banner!']);
        }

    }
}
