<?php

namespace App\Http\Controllers;

use App\Book;
use App\Comment;
use App\Consult;
use App\Coupon;
use App\Customer;
use App\Event;
use App\EventDate;
use App\Gallery;
use App\Info;
use App\Newsletter;
use App\Post;
use App\Price;
use App\Room;
use App\Stat;
use App\Voice;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class GeneralController extends Controller
{

    public function landing()
    {
        $this->statistic();
        $books = Book::all();
        $sale = Book::where('id', '1')->first();
        $post = Post::where('status', true)->take('6')->get();
        //return view('landing.homepage', ['books' => $books, 'sale' => $sale]);
        return view('landing.new', ['books' => $books, 'sale' => $sale, 'posts' => $post]);
    }

    public function buyMyBook()
    {
        $sale = Book::where('id', '1')->first();
       return view('landing.buy-my-book', ['sale' => $sale]);
       //return view('landing.get_my_book', ['sale' => $sale]);
    }

    public function myProfile()
    {
        return view('landing.my-profile');
    }

    public function workWithMe()
    {
        return view('landing.work-with-me');
    }

    public function blog()
    {
        $post = Post::where('status', true)->get();
        return view('landing.blog', ['posts' => $post]);
    }

    public function blog_details($slug)
    {
       $details = Post::where('slug', $slug)->firstOrFail();
       $count = $details->count;
       $details->update(['count' => $count+1]);
       return view('landing.blog_details', ['details' => $details]);
    }

    public function statistic()
    {
//        $dt = \Carbon\Carbon::now();
        $date = \Carbon\Carbon::today()->toDateString();

        $check = Stat::where('date', $date)->first();
        if($check == null)
        {
            Stat::create(['name' => 'visits', 'date' => $date, 'count' => '1']);
        }else{
            $oldCount = $check->count;
            $check->update(['count' => $oldCount+1]);
        }
    }


    public function customer(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required',
            'address' => 'required|max:255',
            'location' => 'required|max:2048',
            'gender' => 'required|max:2048',
            'phone' => 'required|numeric',
            'coupon' => 'nullable|string',
        ]);
        $coupon = $request->coupon;
        if($coupon == null){
            $customer = Customer::create($request->all());
            $customer->save();
            $book_ref = $request->book_ref;
        }else{
            $real_coupon = Coupon::where('status', true)->first();
            if($coupon != $real_coupon->code)
            {
                return back()->with(['error' => 'The Coupon Code entered is Incorrect']);
            }
            $customer = Customer::create($request->all());
            $customer->save();
            $book_ref = $request->book_ref;
        }
        return redirect('buy/'.$book_ref.'/'.$customer->ref);
    }

    public function hrpay(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required',
            'address' => 'required|max:255',
            'location' => 'required|max:2048',
            //'gender' => 'required|max:2048',
            'phone' => 'required|numeric',
            //'coupon' => 'nullable|string',
        ]);

        $customer = Customer::create($request->all());
        $customer->save();
        //$book_ref = $request->book_ref;
        return redirect('buy/'.$customer->ref);
    }


    public function buyhr($customer)
    {
        //dd($customer);
        $cus = Customer::where('ref', $customer)->first();
        if($cus == null){
            abort('404');
        }
        return view('landing.hrpay', ['cus' => $cus]);
    }

    public function buy($ref, $customer)
    {
        $book = Book::where('ref', $ref)->first();
        $cus = Customer::where('ref', $customer)->first();
        if($book == null)
        {
            abort('404');
        }

        $coupon = Coupon::where('status', true)->first();

        $dis = "0";
        if($coupon->code == $cus->coupon)
        {
            $dis = $coupon->percentage;
        }
        $percentage = $dis/100;
        $percentage_price = $book->price*$percentage;

        $price = $book->price - $percentage_price;

        return view('landing.pay', ['book' => $book, 'cus' => $cus, 'price' => $price]);

    }

    public function success($ref)
    {
        return view('landing.success');
    }

    public function newsletter(Request $request)
    {
        //dd($request->email);
        $check = Newsletter::where('email', $request->email)->first();
        //dd($check);
        if($check == null){

            $ref = Uuid::uuid4();
            $newsletter = Newsletter::create(['ref' => $ref, 'email' => $request->email]);
            if($newsletter)
            {
                return back()->with(['success' => 'You have Successfully Subscribe for News Letter']);
            }

        }else{
            return back()->with(['error' => 'You have already subscribe']);
        }
    }

    public function consult_yemisi()
    {
        $one = Price::where('type', 'one')->first();
        $group = Price::where('type', 'group')->first();
        $price_public_ilorin = Price::where('type', 'public')->where('location', 'ilorin')->first();
        $price_public_out = Price::where('type', 'public')->where('location', 'outside')->first();
        $price_intl = Price::where('type', 'public')->where('location', 'int')->first();
        return view('landing.consult_yemisi', ['public_ilorin' => $price_public_ilorin,
            'public_outside' => $price_public_out,'public_intl' => $price_intl, 'one' => $one, 'group' => $group]);
    }

    public function post_public(Request $request)
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        $code = "";
        for ($i = 0; $i < 25; $i++) {
            $code .= $chars[mt_rand(0, strlen($chars)-1)];
        }
        $type = Price::where('type', $request->type)->where('location', $request->location)->first();
        $post = Consult::create($request->all());
        $post->save();
        $post->update(['ref' => $code, 'amount' => $type->amount, 'type_id' => $type->id]);
        return redirect('/pay/consult/'.$post->ref);
    }


    public function post_one(Request $request)
    {
        //dd($request);
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        $code = "";
        for ($i = 0; $i < 25; $i++) {
            $code .= $chars[mt_rand(0, strlen($chars)-1)];
        }
        $type = Price::where('type', $request->type)->first();

        $post = Consult::create($request->all());
        $post->save();
        $post->update(['ref' => $code, 'amount' => $type->amount, 'type_id' => $type->id]);

        return redirect('/pay/consult/'.$post->ref);
    }


    public function post_group(Request $request)
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        $code = "";
        for ($i = 0; $i < 25; $i++) {
            $code .= $chars[mt_rand(0, strlen($chars)-1)];
        }
        $type = Price::where('type', $request->type)->first();
        $post = Consult::create($request->all());
        $post->save();
        $post->update(['ref' => $code, 'amount' => $type->amount, 'type_id' => $type->id]);
        return redirect('/pay/consult/'.$post->ref);
    }

    public function pay_consult($user)
    {
        $customer = Consult::where('ref', $user)->first();
        $price_type = Price::where('id', $customer->type_id)->first();
        //dd($price_type);
        return view('landing.pay_consult', ['type' => $price_type, 'user' => $customer]);
    }

    public function resources()
    {
        $books = Book::all();
        return view('landing.resources', ['books' => $books]);
    }

    public function events()
    {
        $gallery = Gallery::all()->load('year');
        $events_gal = Event::where('type', 'gallery')->get();
        //dd($gallery);
        //$year = EventDate::all();
        return view('landing.events', ['gallery' => $gallery]);
    }

    public function post_comment(Request $request)
    {
        // $comment = Comment::create($request->all());
        // $comment->save();
        return back()->with(['success' => 'Comment Successfully Posted']);
    }

    public function post_info(Request $request)
    {
        $info = Info::create($request->all());
        $info->save();

        //$ref = bcrypt($info->name);

        return redirect('good');

    }

    public function good()
    {
        return view('landing.good');
    }

    public function bookLaunch()
    {
        return redirect('https://booklaunch.subscribemenow.com');
    }

    public function hrtemplate()
    {
        return view('landing.hr');
        //return redirect('https://hrtemplate.subscribemenow.com');
    }
}
