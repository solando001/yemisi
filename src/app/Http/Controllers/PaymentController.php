<?php

namespace App\Http\Controllers;


use App\ComsultTransaction;
use App\Transaction;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Paystack;

class PaymentController extends Controller
{

    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway()
    {
        return Paystack::getAuthorizationUrl()->redirectNow();

//        try{
//            return Paystack::getAuthorizationUrl()->redirectNow();
//        }catch(\Exception $e) {
//            return Redirect::back()->withMessage(['msg'=>'The paystack token has expired. Please refresh the page and try again.', 'type'=>'error']);
//        }
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
        $paymentDetails = Paystack::getPaymentData();
        if($paymentDetails['data']['metadata']['type'] == 'book'){
            $this->logUserPayment($paymentDetails);
        }else{
            $this->logConsultPayment($paymentDetails);
        }
        $ref = $paymentDetails['data']['reference'];
        return redirect('/success/'.$ref);
        //return view('landing.success');
        //dd($paymentDetails);
        // Now you have the payment details,
        // you can store the authorization_code in your db to allow for recurrent subscriptions
        // you can then redirect or do whatever you want
    }


    public function logUserPayment($paymentDetails)
    {
        Transaction::create([
            'amount'=>$paymentDetails['data']['amount'],
            'reference'=>$paymentDetails['data']['reference'],
            'book_id'=>$paymentDetails['data']['metadata']['book_id'],
            'customer_id'=>$paymentDetails['data']['metadata']['customer_id'],
            'successful'=>true,
//            'status'=>$paymentDetails['data']['gateway_response'],
//            'start_time'=>$paymentDetails['data']['created_at'],
//            'end_time'=>$paymentDetails['data']['paid_at'],
//            'email'=>$paymentDetails['data']['customer']['email'],
//            'gateway'=>'paystack',
//            'narration'=>$paymentDetails['data']['log'],

        ]);
    }

    public function logConsultPayment($paymentDetails)
    {
        ComsultTransaction::create([
            'amount'=>$paymentDetails['data']['amount'],
            'reference'=>$paymentDetails['data']['reference'],
            'price_id'=>$paymentDetails['data']['metadata']['price_id'],
            'consult_id'=>$paymentDetails['data']['metadata']['consult_id'],
            'successful'=>true,
//            'status'=>$paymentDetails['data']['gateway_response'],
//            'start_time'=>$paymentDetails['data']['created_at'],
//            'end_time'=>$paymentDetails['data']['paid_at'],
//            'email'=>$paymentDetails['data']['customer']['email'],
//            'gateway'=>'paystack',
//            'narration'=>$paymentDetails['data']['log'],

        ]);
    }
}
