<?php

namespace App\Http\Controllers;

use App\Book;
use App\Category;
use App\ComsultTransaction;
use App\Coupon;
use App\Event;
use App\EventDate;
use App\Gallery;
use App\Info;
use App\Post;
use App\Price;
use App\Room;
use App\Stat;
use App\Transaction;
use App\Voice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $role = $user->role()->first()->name;
        if($role == "user")
        {
            $this->statistic();
            $books = Book::all();
            $sale = Book::where('id', '1')->first();
            $post = Post::where('status', true)->take('6')->get();
            //return view('landing.homepage', ['books' => $books, 'sale' => $sale]);
            return view('landing.new', ['books' => $books, 'sale' => $sale, 'posts' => $post]);

            //$this->statistic();
//            $books = Book::all();
//            $sale = Book::where('id', '1')->first();
//            return view('landing.homepage', ['books' => $books, 'sale' => $sale]);
        }else{
            $visit = Stat::where('name', 'visits')->sum('count');
            $books = Book::all();
            $coupon = Coupon::where('status', true)->first();
            return view('home', ['visit' => $visit, 'books' => $books, 'coupon' => $coupon ]);
        }
    }

    public function statistic()
    {
//        $dt = \Carbon\Carbon::now();
        $date = \Carbon\Carbon::today()->toDateString();

        $check = Stat::where('date', $date)->first();
        if($check == null)
        {
            Stat::create(['name' => 'visits', 'date' => $date, 'count' => '1']);
        }else{
            $oldCount = $check->count;
            $check->update(['count' => $oldCount+1]);
        }
    }

    public function settings()
    {
        return view('settings');
    }

    public function upload_book(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'description' => 'required',
            'price' => 'required|numeric',
            'image' => 'required|mimes:jpg,jpeg,png|max:2048',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name = time() . $image->getClientOriginalName();
            $image_extension_name = $image->getClientOriginalExtension();

            if ($image_extension_name == 'jpeg' || $image_extension_name == 'png' || $image_extension_name == 'jpg') {
                $imagefilePath = 'images/' . $image_name;
                Storage::disk('s3')->put($imagefilePath, file_get_contents($image), 'public');
                $url = Storage::disk('s3')->url($imagefilePath);
                $ref = Uuid::uuid4();
                Book::create([
                    'user_id' => Auth::user()->id,
                    'ref' => $ref, 'name' => $request->name,
                    'price' => $request->price,
                    'description' => $request->description,
                    'image' => $url]);
                return back()->with(['success' => 'Book Uploaded Successfully']);
            } else {
                return back()->with(['error' => 'Error Uploading Book']);
            }
        }
    }

//        if ($request->file('image')) {
//            $imagePath = $request->file('image');
//            $imageName = $imagePath->getClientOriginalName();
//            $path = $request->file('image')->storeAs('books', $imageName, 'public');
//            $ref = Uuid::uuid4();
//            Book::create([
//                'user_id' => Auth::user()->id,
//                'ref' => $ref, 'name' => $request->name,
//                'price' => $request->price,
//                'description' => $request->description,
//                'image' => $path]);
//            return back()->with(['success' => 'Book Uploaded Successfully']);



    public function create_coupon(Request $request)
    {
        $request->validate([
            'percentage' => 'required|numeric'
        ]);
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $code = "";
        for ($i = 0; $i < 7; $i++) {
            $code .= $chars[mt_rand(0, strlen($chars)-1)];
        }
        $count = Coupon::all();
        if(count($count) > 0)
        {
            return back()->with(['error' => 'Coupon Code Already Exist']);
        }
        $coupon = Coupon::create(['user_id' => Auth::user()->id, 'code' => $code, 'percentage' => $request->percentage]);
        if($coupon){
            return back()->with(['success' => $code.' Coupon Code Generated']);
        }
    }

    public function delete_coupon($code)
    {
        $del = Coupon::where('code', $code)->first()->delete();
        if($del){
            return back()->with(['success' => 'Coupon Code Revoked']);
        }
    }

    public function add_post()
    {
        $visit = Stat::where('name', 'visits')->sum('count');
        $books = Book::all();
        $coupon = Coupon::where('status', true)->first();
        $category = Category::all();
        return view('addpost', ['visit' => $visit, 'books' => $books, 'coupon' => $coupon, 'category' => $category]);
    }

    public function post_blog(Request $request)
    {
        $request->validate([
            'title' => 'required|string',
            'content' => 'required'
        ]);

        if($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name = time() . $image->getClientOriginalName();
            $image_extension_name = $image->getClientOriginalExtension();

            if ($image_extension_name == 'jpeg' || $image_extension_name == 'png' || $image_extension_name == 'jpg') {
                $imagefilePath = 'images/' . $image_name;
                Storage::disk('s3')->put($imagefilePath, file_get_contents($image), 'public');
                $url = Storage::disk('s3')->url($imagefilePath);
                $post = Post::create($request->all());
                $post->save();
                $str = Str::slug($request->title, '-');
                $slug = $str.-$post->id;
                $post->update(['slug'=>$slug, 'image' => $url]);
                return back()->with(['success' => 'Post Created Successfully']);
            }else {
                return back()->with(['error' => 'Please upload an image of either png, jpg, jpeg!']);
            }

            }else{
                return back()->with(['error' => 'Please upload at least one banner!']);
            }




    }

    public function view_post()
    {
        $posts = Post::orderBy('id', 'desc')->get();
        $coupon = Coupon::where('status', true)->first();
        $visit = Stat::where('name', 'visits')->sum('count');
        return view('view_post', ['posts' => $posts, 'coupon' =>$coupon, 'visit' => $visit]);
    }

    public function edit_post($ref)
    {
        $post = Post::where('slug', $ref)->firstOrFail();
        $posts = Post::orderBy('id', 'desc')->get();
        $coupon = Coupon::where('status', true)->first();
        $visit = Stat::where('name', 'visits')->sum('count');
        return view('edit_post', ['posts' => $posts, 'coupon' =>$coupon, 'visit' => $visit, 'post' => $post]);
    }

    public function delete_post($ref)
    {
        $del = Post::where('slug', $ref)->delete();
        return back()->with(['success' => 'Post Deleted Successfully']);
    }

    public function post_blog_edit(Request $request)
    {
        $find = Post::where('slug', $request->slug)->firstOrFail();
        $find->update(['title' => $request->title, 'content' => $request->content, 'user_id' => Auth::user()->id ]);
        return back()->with(['success' => 'Post Edited Successfully']);
    }

    public function create_category(Request $request)
    {
        $category = Category::create($request->all());
        $category->save();
        return back()->with(['success' => 'Category Created Successfully']);
    }

    public function view_work()
    {
//        $post = Post::where('slug', $ref)->firstOrFail();
        $posts = Post::orderBy('id', 'desc')->get();
        $coupon = Coupon::where('status', true)->first();
        $visit = Stat::where('name', 'visits')->sum('count');
        $prices = Price::all();
        return view('view_work', ['prices' => $prices, 'posts' => $posts, 'coupon' =>$coupon, 'visit' => $visit]);
    }

    public function edit_consult(Request $request)
    {
       $post = Price::where('id', $request->id)->first();
        $post->update(['amount' => $request->amount]);
        return back()->with(['success' => 'Amount Changed Successfully']);
    }

    public function consult_list()
    {
        $posts = Post::orderBy('id', 'desc')->get();
        $coupon = Coupon::where('status', true)->first();
        $visit = Stat::where('name', 'visits')->sum('count');
        $consult_transaction = ComsultTransaction::all();
        return view('consult_list', ['consultations' => $consult_transaction, 'posts' => $posts, 'coupon' =>$coupon, 'visit' => $visit]);
    }

    public function add_event_date()
    {
        $posts = Post::orderBy('id', 'desc')->get();
        $coupon = Coupon::where('status', true)->first();
        $visit = Stat::where('name', 'visits')->sum('count');
        $dates = EventDate::all();
        return view('add_event', [ 'dates' => $dates, 'posts' => $posts, 'coupon' =>$coupon, 'visit' => $visit]);
    }

    public function add_year(Request $request)
    {
        $check = EventDate::where('date', $request->date)->first();
        if($check == null){
            $date = EventDate::create($request->all());
            $date->save();
            return back()->with(['success' => 'Date Added Successfully']);
        }else{
            return back()->with(['error' => 'Date already Exist']);
        }
    }

    public function add_event()
    {
        $posts = Post::orderBy('id', 'desc')->get();
        $coupon = Coupon::where('status', true)->first();
        $visit = Stat::where('name', 'visits')->sum('count');
        $dates = EventDate::all();
        $event = Event::all();
       return view('events', [ 'dates' => $dates, 'posts' => $posts, 'coupon' =>$coupon, 'visit' => $visit, 'events' => $event]);
    }

    public function create_event(Request $request)
    {
        $event = Event::create($request->all());
        $event->save();
        return back()->with(['success' => 'Event Created Successfully']);
    }

    public function upload_event_inage(Request $request)
    {
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name = time() . $image->getClientOriginalName();
            $image_extension_name = $image->getClientOriginalExtension();

            if ($image_extension_name == 'jpeg' || $image_extension_name == 'png' || $image_extension_name == 'jpg') {
                $imagefilePath = 'images/' . $image_name;
                Storage::disk('s3')->put($imagefilePath, file_get_contents($image), 'public');
                $url = Storage::disk('s3')->url($imagefilePath);
                Gallery::create([
                    'event_id' => $request->event_id,
                    'image' => $url
                ]);
                return back()->with(['success' => 'Image Uploaded Successfully']);
            }else {
                return back()->with(['error' => 'Please upload an image of either png, jpg, jpeg!']);
            }

        }else{
            return back()->with(['error' => 'Please upload at least one banner!']);
        }

    }

    public function info()
    {
        $posts = Post::orderBy('id', 'desc')->get();
        $coupon = Coupon::where('status', true)->first();
        $visit = Stat::where('name', 'visits')->sum('count');
        $infos = Info::all();
        return view('view_info', ['infos' => $infos, 'posts' => $posts, 'coupon' =>$coupon, 'visit' => $visit]);
    }

    public function delete_info($id)
    {
        $delete = Info::where('id', $id)->firstOrFail()->delete();
        return back()->with(['success' => 'Info Deleted Successfully']);
    }


}
