<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected  $table = "customers";
    protected $fillable = ['ref', 'name', 'email', 'phone', 'address', 'location', 'gender', 'coupon', 'quantity'];
}
