<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComsultTransaction extends Model
{
    protected $table = "comsult_transactions";

    protected $fillable = ['amount', 'reference', 'successful', 'consult_id', 'price_id'];

    public function customer()
    {
        return $this->belongsTo(Consult::class, 'consult_id');
    }

    public function price()
    {
        return $this->belongsTo(Price::class, 'price_id');
    }
}
