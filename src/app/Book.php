<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = "books";

    protected $fillable = ['user_id', 'ref', 'name', 'price', 'description', 'image', 'live'];
}
