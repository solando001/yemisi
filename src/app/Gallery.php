<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected  $table = "galleries";

    protected $fillable = ['event_id', 'image', 'type', 'description'];

    public function year()
    {
        return $this->belongsTo(Event::class, 'event_id');
    }
}
