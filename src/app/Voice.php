<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voice extends Model
{
    protected $table = 'voices';

    protected $fillable = ['user_id', 'room_id', 'hash', 'title', 'url', 'description', 'status'];

    public function room()
    {
        return $this->belongsTo('App\Room', 'room_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
