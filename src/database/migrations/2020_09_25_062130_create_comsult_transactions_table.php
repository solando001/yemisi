<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComsultTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comsult_transactions', function (Blueprint $table) {
            $table->id();
            $table->string('amount');
            $table->string('reference');
            $table->boolean('successful');
            $table->unsignedBigInteger('consult_id');
            $table->unsignedBigInteger('price_id');
            $table->timestamps();

            $table->foreign('consult_id')->references('id')->on('consults')
                ->cascadeOnDelete()->cascadeOnUpdate();

            $table->foreign('price_id')->references('id')->on('prices')
                ->cascadeOnUpdate()->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comsult_transactions');
    }
}
