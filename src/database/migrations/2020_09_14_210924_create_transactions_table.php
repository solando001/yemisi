<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('amount');
            $table->string('reference');
            $table->boolean('successful');
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('book_id');
            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('customers')
                ->cascadeOnDelete()->cascadeOnUpdate();

            $table->foreign('book_id')->references('id')->on('books')
                ->cascadeOnUpdate()->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
