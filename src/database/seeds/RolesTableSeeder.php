<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class RolesTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     *
     */
    public $roles;

    public function __construct()
    {
        $this->roles = [
            'admin' =>
                [
                    'name' => 'admin',
                    'description' => 'This is an administrator. He has access and can alter/delete anything on the system'
                ],
            'user' =>
                [
                    'name' => 'user',
                    'description' => 'This is a user role.'
                ],
        ];
    }

    public function run()
    {
        ///create all roles
        foreach ($this->roles as $k => $v) {
            DB::table('roles')->insert($v);
        }


    }
}
