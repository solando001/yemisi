<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $admin_role;

    public function __construct()
    {
        $this->admin_role = App\Role::where('name', 'admin')->first();
    }

    public function run()
    {
        $user = \App\User::create([
            'name' => 'System',
            'email' => 'femz12@yahoo.com',
            'phone' => '0819288373',
            'address' => 'No 8',
            'location' => 'Lagos',
            'gender' => 'Male',
            'password' => bcrypt('N*-aPh6U@*%AbZHQgeA+L7V'),
        ]);

        DB::table('user_roles')->insert([
            'user_id' => $user->id,
            'role_id' => $this->admin_role->id,
        ]);

        $user = \App\User::create([
            'name' => 'Oluwatobi',
            'email' => 'solotobby@gmail.com',
            'phone' => '0819288373',
            'address' => 'No 8',
            'location' => 'Lagos',
            'gender' => 'Male',
            'password' => bcrypt('solomon001'),
        ]);

        DB::table('user_roles')->insert([
            'user_id' => $user->id,
            'role_id' => $this->admin_role->id,
        ]);

        $user = \App\User::create([
            'name' => 'Farohunbi Samuel Tobi',
            'email' => 'myhotjobz@gmail.com',
            'phone' => '0819288373',
            'address' => 'No 8',
            'location' => 'Lagos',
             'gender' => 'Male',
            'password' => bcrypt('testimonies90'),
        ]);

        DB::table('user_roles')->insert([
            'user_id' => $user->id,
            'role_id' => $this->admin_role->id,
        ]);

        $user = \App\User::create([
            'name' => 'Yemisi Adeyeye',
            'email' => 'yemisi.adeyeye@yahoo.com',
            'phone' => '0819288373',
            'address' => 'No 8',
            'location' => 'Ilorin',
            'gender' => 'Female',
            'password' => bcrypt('Adeyeye2020'),
        ]);

        DB::table('user_roles')->insert([
            'user_id' => $user->id,
            'role_id' => $this->admin_role->id,
        ]);
    }
}
