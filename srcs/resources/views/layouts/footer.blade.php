<!-- Footer area start -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer-content">
                    <p class="pull-left">&copy; <?php echo date('Y') ?> Yemisi Adeyeye <i class="fa fa-heart" aria-hidden="true"></i> by
                        <a href="#" target="_blank"> GoodNews Info. Technology</a></p>
                    <div class="footer-icon pull-right">
                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer><!-- Footer area end -->
<!-- scrolltotop start -->
<div>
    <a href="#" class="scrollToTop text-center" >
        <i class="scroll-fa fa fa-angle-up" aria-hidden="true"></i>
    </a>
</div><!-- scrolltotop end -->
