<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Stat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $visit = Stat::where('name', 'visits')->sum('count');
        $books = Book::all();
        return view('home', ['visit' => $visit, 'books' => $books]);
    }

    public function settings()
    {
        return view('settings');
    }

    public function upload_book(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'description' => 'required',
            'price' => 'required|numeric',
            'image' => 'required|mimes:jpg,jpeg,png|max:2048',
        ]);


        $fileName = time().'.'.$request->image->extension();
        $location = $request->image->move(public_path('books/'), $fileName);

        if($location)
        {
            $ref = Uuid::uuid4();
            Book::create(['user_id' => Auth::user()->id, 'ref' => $ref, 'name' => $request->name, 'price' => $request->price, 'description' => $request->description, 'image' => $location]);
            return back()->with(['success' => 'Book Uploaded Successfully']);
        }else{
            return back()->with(['error' => 'Error Uploading Book']);
        }


    }
}
