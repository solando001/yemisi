<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Stat;
use Illuminate\Http\Request;

class GeneralController extends Controller
{
    public function index()
    {
        $this->statistic();
        $books = Book::all();
        return view('general.index', ['books' => $books]);
    }

    public function blog()
    {
        return view('general.blog');
    }

    public function statistic()
    {
//        $dt = \Carbon\Carbon::now();
        $date = \Carbon\Carbon::today()->toDateString();
        $check = Stat::where('date', $date)->first();
        if($check == null)
        {
            Stat::create(['name' => 'visits', 'date' => $date, 'count' => '1']);
        }else{
            $oldCount = $check->count;
            $check->update(['count' => $oldCount+1]);
        }
    }
}
